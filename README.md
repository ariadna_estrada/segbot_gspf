# README #

An ROS - Matlab implementation of the Gradient Sampling Particle Filter for unicycle dynamics. We take the GSPF algorithm developed by Neil Traft and Ian Mitchell demonstrated on a 
holonomic robot as a starting point and extend it to a non-holonomic robot. 

### What is this repository for? ###

* This package uses the GSPF algorithm on the Segbot RMP 100.  
* An initial version of the Value Sampling Particle Filter is also included.
* We include the Toolbox of Level Set Methods (Toolbox LS)
* This algorithms are described in detail in the following publication: 
	Estrada, Ariadna, and Ian M. Mitchell. "Control Synthesis and Classification for Unicycle Dynamics using the Gradient and Value Sampling Particle Filters." IFAC-PapersOnLine 51.16 (2018): 290-295.https://doi.org/10.1016/j.ifacol.2018.08.049

### What do I need to start? ###
####This work has been tested on:####

* ROS Indigo
* Matlab R2016b
* Matlab's Robotic System Toolbox Version 1.3

### How do I get set up? ###

* Create a catkin workspace
* Clone this repository into the src directory of the workspace. Run catkin_make on the terminal. 
* On the terminal type: roslaunch segbot_navigation navigation_sgpp_doorway.launch
* Open Matlab and run test_office_segbot.m. A GUI will start, click "Initialize" to connect to the ROS Master. Select a goal and hit "Start" to begin. 
* Included only two pre-computed value functions in the repository due to space constrains. To compute a value function for a new goal, see prepare_3d_valuefcn.m. 

### Who do I talk to? ###

* Ariadna Estrada aestra42@cs.ubc.ca
* Ian Mitchell mitchell@cs.ubc.ca
