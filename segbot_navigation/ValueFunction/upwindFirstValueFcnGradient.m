function gradient = upwindFirstValueFcnGradient(value_fcn, dx, obstacle_fcn)
% upwindFirstValueFcnGradient: Approximate the gradient of a value function.
%
%   gradient = upwindFirstValueFcnGradient(value_fcn, obstacle_fcn, grid)
%
% Computes an upwinded first order accurate finite difference approximation
% of the gradient at each node in a grid.  The value function's
% characteristics (the trajectories of the underlying dynamics) are assumed
% to flow from low values to high values, so "upwinding" seeks to take the
% finite differences in the direction of lower values.
%
% It is also assumed that the target is within the computational domain,
% which implies that all characteristics flow outward through the boundary
% and hence there is no need for boundary conditions to perform upwinded
% finite differences.  A similar assumption is made about nodes near
% obstacles.  Note that the combination of these assumptions means that the
% code will break if there are narrow passages (eg: a single node) between
% obstacles or between an obstacle and the computational boundary.  (Such a
% situation indicates insufficient resolution in the computational grid
% anyway.)
%
% Input parameters:
%
%   value_fcn: Array.  The function whose gradient is required.  Note: if
%   the value function is one dimensional, it must be a column vector.
%
%   dx: Scalar or vector of length ndims(value_fcn) containing positive
%   doubles.  The spacing of the nodes in each dimension.  Optional,
%   default = 1.
%
%   obstacle_fcn: logical array compatible with value_fcn or false.  A
%   value of NaN will be returned for nodes within the obstacle (where this
%   function is true) and it is assumed that all characteristics flow
%   toward the obstacle (so upwinding always points away from the obstacle).
%   A value of false indicates that there are no obstacles (as would a value
%   which is false everywhere).  If this array is lower dimensional than
%   value_fcn, it is replicated in the missing dimensions.  Optional, 
%   default = false.
%
% Output parameters:
%
%   gradient: Cell vector of arrays.  The vector is of length
%   ndims(value_fcn) and each array is the same size as value_fcn.  Element
%   i of the cell vector contains coordinate i of the gradient.

  % What does the value function array look like?
  value_size = size(value_fcn);
  value_dim = ndims(value_fcn);
  % Stupid Matlab has no one dimensional arrays, so we have to handle the
  % special case.
  if(value_dim == 2) 
    if(value_size(2) == 1)
      % We got a column vector.  Set dimension to one, but otherwise no
      % special code is needed.
      value_dim = 1;
    elseif(value_size(1) == 1)
      error('One dimensional value functions must be column vectors.');
    end
  end

  %---------------------------------------------------------------------------
  %% Optional input parameters.
  if(nargin < 2)
    dx = ones(value_dim, 1);
  elseif(length(dx) == 1)
    dx = dx * ones(value_dim, 1);
  else
    assert(length(dx) == value_dim);
  end
  if(nargin < 3)
    obstacle_fcn = false;
  end

  % If there is an obstacle function, create a logical array which
  % identifies which nodes are inside the obstacle.
  if(numel(obstacle_fcn) > 1)
    % Check that the obstacle array is compatible with the value function
    % array.
    assert(all(size(obstacle_fcn) == value_size(1:ndims(obstacle_fcn))))
    % If necessary, expand the obstacle array in missing dimensions.
    mask_obs = repmat(obstacle_fcn, [ ones(1, ndims(obstacle_fcn)), value_size(ndims(obstacle_fcn)+1:end) ]);
  else
    mask_obs = false(value_size);
  end
  
  %---------------------------------------------------------------------------
  %% Compute the upwinded gradient.
    
  % Output argument
  gradient = cell(value_dim, 1);
  
  % Each component of the gradient is computed independently.
  for d = 1 : value_dim
    
    %---------------------------------------------------------------------------
    %% Compute a finite difference in this dimension.
    
    % Create an index array which can be used to pick out rectangular
    % subsets of the nodes.
    index_l = cell(value_dim, 1);
    for i = 1 : value_dim
      if(i == d)
        index_l{i} = 1 : value_size(i) - 1;
      else
        index_l{i} = 1 : value_size(i);
      end
    end
    index_r = index_l;
    index_r{d} = 2 : value_size(d);

    % Compute the finite difference array.   Note that this array is
    % narrower by one in the current dimension.
    fd = (value_fcn(index_r{:}) - value_fcn(index_l{:})) / dx(d);
    
    %---------------------------------------------------------------------------
    %% Handle the boundary nodes.
    
    % Nodes on the boundary must look inward.  Set up the index arrays to
    % find these nodes.
    index_l{d} = 1;
    index_r{d} = value_size(d);
    
    % Nodes on the far right must look left and nodes on the far left must
    % look right.  Note that some of these nodes may be within obstacles;
    % these will be masked out later.
    mask_l_bdry = false(value_size);
    mask_r_bdry = false(value_size);
    mask_l_bdry(index_r{:}) = true;
    mask_r_bdry(index_l{:}) = true;
    
    %---------------------------------------------------------------------------
    %% Handle the nodes next to obstacles.

    % Set up the index arrays so that they pick out left and right subsets
    % of the indexes in the current dimension, ignoring the nodes on the
    % boundary (which already have known direction).
    index_r{d} = 2 : value_size(d);
    index_l{d} = 1 : value_size(d) - 1;

    % Nodes to the left of an obstacle must look left and vice-versa for
    % nodes which are to the right of an obstacle.  Note that these masks
    % will include nodes which are within obstacles, but we will ignore
    % those nodes later.
    mask_l_obs = false(value_size);
    mask_r_obs = false(value_size);
    mask_l_obs(index_l{:}) = mask_obs(index_r{:});
    mask_r_obs(index_r{:}) = mask_obs(index_l{:});
    
    %---------------------------------------------------------------------------
    %% Handle the remaining nodes.
        
    % Pad the FD array on the left / right to get it back to the correct
    % size.  We will pad with 0 because these FD values will not be used.
    % (It would be better to pad with NaN to ensure that they are not used,
    % but Matlab's logical operators cannot propagate NaN but rather
    % generate an error).
    slice_size = value_size;
    slice_size(d) = 1;
    slice_zeros = zeros(slice_size);
    fd_l = cat(d, slice_zeros, fd);
    fd_r = cat(d, fd, slice_zeros);
    
    % Properties of the finite differences.
    sign_l = sign(fd_l);
    sign_r = sign(fd_r);
    mask_sign_mixed = (sign_l .* sign_r ~= +1);
    mask_bigger_l = (abs(fd_l) > abs(fd_r));
    
    % Upwind rules: Look left if both left and right FD are positive or
    % left and right FD disagree and left magnitude is larger.  Look right
    % if both left and right FD are negative or left and right FD disagree
    % and right magnitude is larger.
    mask_l_upwind = ((sign_l == +1) & (sign_r == +1) | (mask_sign_mixed & mask_bigger_l));
    mask_r_upwind = ((sign_l == -1) & (sign_r == -1) | (mask_sign_mixed & ~mask_bigger_l));

    % Upwinding should not lead to any nodes looking left and right.
    assert(~any(mask_l_upwind(:) & mask_r_upwind(:)));
    
    %---------------------------------------------------------------------------
    %% Combine the various masks (and check for inconsistency).

    % Identify those nodes which must look left or right because of an
    % obstacle or the computational boundary.  Nodes within the obstacles
    % should be set to false.
    mask_l_forced = ((mask_l_bdry | mask_l_obs) & ~mask_obs);
    mask_r_forced = ((mask_r_bdry | mask_r_obs) & ~mask_obs);
    
    % There should be no nodes which are forced to look left and right.
    assert(~any(mask_l_forced(:) & mask_r_forced(:)));
    
    % Build the final masks.
    mask_l = mask_l_forced | (mask_l_upwind & ~mask_r_forced & ~mask_obs);
    mask_r = mask_r_forced | (mask_r_upwind & ~mask_l_forced & ~mask_obs);

    % Check that the masks partition the array.
    assert(all(mask_l(:) | mask_r(:) | mask_obs(:)));
    assert(~any(mask_l(:) & mask_r(:)));
    assert(~any(mask_l(:) & mask_obs(:)));
    assert(~any(mask_r(:) & mask_obs(:)));
    
    %---------------------------------------------------------------------------
    %% Extract the upwinded finite difference.

    % Pad the FD array on the left / right to get it back to the correct
    % size.  This time we will pad with NaN to ensure that these values are
    % not accidentally used.
    slice_size = value_size;
    slice_size(d) = 1;
    slice_nan = zeros(slice_size);
    fd_l = cat(d, slice_nan, fd);
    fd_r = cat(d, fd, slice_nan);

    % We also intialize the gradient array with NaN.  Nodes within
    % obstacles will remain NaN, but everything else should get a value.
    gradient{d} = nan * ones(value_size);
    gradient{d}(mask_l) = fd_l(mask_l);
    gradient{d}(mask_r) = fd_r(mask_r);
    
  end % Loop over dimensions.
  
end