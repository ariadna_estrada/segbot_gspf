function [ mttr, gridOut, obstacle_indicator, obstacle_lsf ] = ttr(visualize, obstacle, target, dim, t_max, dryRun)
% ttr: time to reach for infinite wall/finite wall
%
%   [ mttr, gridOut, obstacle_indicator, obstacle_lsf ] = ttr(visualize, obstacle, target, dim, t_max, dryRun)
%  
% In this example we calculate the minimum time to reach (MTTR) a small ball
% around the target location without passing through obstacles.
%
% Depending on the requested dimension, this code will compute the MTTR for
% either a holonomic planar robot (2D) or a unicycle (3D).  In both cases
% the robot is assumed to be pointlike, and in the latter case it is
% assumed that the heading is irrelevant with respect to obstacles or
% achieving the goal.
%
% This function was originally designed as a script file, so most of the
% options can only be modified in the file.  For example, edit the file to
% change the grid dimension, boundary conditions, motion parameters, etc.
%
% Parameters:
%
%   visualize: Boolean.  If true, then a figure window is created to
%   visualize the reach set as it moves.  Optional, default = false.
%
%   obstacle: String, function_handle or struct.  Signed distance or cost
%   function for the obstacle(s).  Optional, default = 'finite_wall'.
%   If a string, the options are:
%      'none': There are no obstacles.
%      'infinite_wall: call lsf_infinite_wall(grid, [ 0; 0 ], 0.5);
%      'finite_wall': call lsf_finite_wall(grid, [ 0; 1 ], 0.5);
%   If a function_handle, call the function with the grid.xs{:} as its
%   input arguments, and use the first output argument as the signed
%   distance function.
%   If a struct, then it describes a 2D costmap and must have the following
%   fields:
%      resolution: Scalar; width of one grid cell in meters (cells are
%          always square).
%      width: Scalar; number of grid cells in the first dimension.
%      height: Scalar; number of grid cells in the second dimension.
%      origin: 2-vector; the lower-left corner of the map.
%      cost: An array matching the given width and height. The cost should be
%          normalized to the range [0, 100], where 100 = obstacle and 0 =
%          free space. 
%   For structs, the grid will be constructed to match the map. For all
%   formats, an appropriate obstacle_indicator and obstacle_lsf will be generated.
%
%   target: Vector of length 2.  Coordinates of the target point.
%   Optional, default = [ -2.0; 0.0 ].   Note that for the infinite wall
%   example you probably want to use something further down; for example, [
%   -4.0; -4.0 ].
%
%   dim: Either 2 or 3.  Specifies the robot's state space dimension: 2 for
%   a holonomic robot (which can move equally fast in any direction at any
%   time) or 3 for a unicycle robot (which has a heading and a maximum
%   angular velocity).  Optional.  Default = 2.
%   
%   t_max: Scalar. The number of seconds over which to compute the value
%   function (i.e. the maximum TTR in the TTR function).
%   
%   dryRun: Boolean. If true, everything except mttr will be computed (grid,
%   obstacles, and speed). Optional, default = false.
%
% Output parameters:
%
%   mttr: Array. Minimum time to reach function at t_max for each grid
%   node.
%
%   gridOut: Structure. Computational grid used to compute mttr.  See
%   processGrid() for details.
%
%   obstacle_indicator: Boolean array same size as mttr.
%
%   obstacle_lsf: Array same size as mttr.  Signed distance function for the obstacle.

% Copyright 2014-2015 Ian M. Mitchell (mitchell@cs.ubc.ca).
% This software is used, copied and distributed under the licensing 
%   agreement contained in the file LICENSE in the top directory of 
%   the distribution.
%
% Ian Mitchell, 2014/07/07.

  %---------------------------------------------------------------------------
  % You will see many executable lines that are commented out. These are
  % included to show some of the options available; modify the commenting
  % to modify the behavior.

  % This flag suppresses the m-lint warning that an m-lint warning was once
  % being suppressed but is no longer being generated.  Without it, m-lint
  % complains about unreachable code too much (or the fact that formerly
  % unreachable code can now be reached so why are you suppressing that
  % message still).
  %#ok<*MSNU>

  %---------------------------------------------------------------------------
  % Make sure we can see the kernel m-files.
  %run addPathToKernel;

  %---------------------------------------------------------------------------
  %% Initialize various variables.
  
  %---------------------------------------------------------------------------
  % Input parameters.
  if(nargin < 1)
    visualize = false;
  end
  if(nargin < 2)
    obstacle = 'finite_wall';
  end
  if(nargin < 3)
    target = [ -2.0; 0.0 ];
  end
  if(nargin < 4)
    dim = 2;
  end
  if(nargin < 5)
    t_max = 12;
  end
  if(nargin < 6)
    dryRun = false;
  end
  
  %---------------------------------------------------------------------------
  % Integration parameters.
  t0 = 0;                      % Start time.
  
  % How close (relative) do we need to get to tMax to be considered finished?
  small = 100 * eps;

  %---------------------------------------------------------------------------
  % Visualization parameters.
  
  % How many intermediate plots to produce?
  plot_steps = 9;
  
  % Period at which intermediate plots should be produced.
  t_plot = (t_max - t0) / (plot_steps - 1);

  % What level set should we view?
  level = 0;

  % Visualize the reachable set.
  switch(dim)
    case 2
      display_type = 'contour';
    case 3
      display_type = 'surface';
  end
  
  % Plot at each timestep (overrides tPlot).
  single_step = false;
  
  % Pause after each plot?
  pause_after_plot = false;

  % Delete previous plot before showing next?
  delete_last_plot = true;

  % Plot in separate subplots (set deleteLastPlot = 0 in this case)?
  use_subplots = false;

  %---------------------------------------------------------------------------
  % Integrator parameters.
  
  % Accuracy of the numerical approximation schemes.  A 5/3 scheme makes a
  % noticeable difference, but takes about three times longer than 2/2.
  % Spatial approximation scheme.
  derivFunc = @upwindFirstENO2;
  % Temporal approximation scheme.
  integratorFunc = @odeCFL2;

  % Some integration parameters apply to any integrator.
  integratorOptions = odeCFLset('factorCFL', 0.9, 'stats', 'on');
  if(single_step)
    integratorOptions = odeCFLset(integratorOptions, 'singleStep', 'on'); %#ok<UNRCH>
  end

  %---------------------------------------------------------------------------
  % Problem Parameters.

  % How wide is the boundary layer around the obstacles where the robot
  % speed slows down? (Not applicable for costmaps.)
  boundary_layer = 0.5;
  
  % Radius of the target set. A value of zero will be translated into a
  % value of (smallTarget * g.dx).
  target_radius = 0.4;

  % What is considered a small target?
  small_target = 1.0;

  %---------------------------------------------------------------------------
  %% Create the grid.
  g.dim = 2;
  g.bdry = { @addGhostExtrapolate; @addGhostExtrapolate };
  
  % Start by creating the 2D grid.
  if isstruct(obstacle)
      g.min = obstacle.origin(:);
      g.max = obstacle.origin(:);
      g.max(1) = g.max(1) + (obstacle.width-1)*obstacle.resolution;
      g.max(2) = g.max(2) + (obstacle.height-1)*obstacle.resolution;
      g.N = [obstacle.width; obstacle.height];
      %g.dx = obstacle.resolution * ones(2, 1);

      % Store a 2D version of the grid for when we process the obstacle
      % cost map later.
      g2 = processGrid(g);
  else
      g.min = -5 * ones(2, 1);
      g.max = +5 * ones(2, 1);
      g.N = 51 * ones(2, 1);
  end
  
  % Store the 2D grid for processing the obstacle map later.

  % If necessary, expand the grid in the third (orientation) dimension.
  if(dim == 3)
    % Choose a number of grid nodes in the orientation dimension equal to
    % the minimum of the number chosen in any other dimension, but no more
    % than 90 (4 degree resolution).
    g.dim = 3;
    g.N = [ g.N; min(90, min(g.N) - 1) ];
    g.min = [ g.min; 0 ];
    % Do not include upper grid node for periodic BC.
    g.max = [ g.max; 2 * pi * (g.N(3) - 1) / g.N(3) ];
    g.bdry = cat(1, g.bdry, { @addGhostPeriodic });
  end
  
  g = processGrid(g);

  % Set output argument in case the caller requests it.
  gridOut = g;

  %---------------------------------------------------------------------------
  %% Create the robot model.
  switch(dim)
    case 2
      model = RobotModel2D();
    case 3
      model = RobotModel3D(g);
    otherwise
      error('Invalid dimension: %d', dim);
  end
  
  %---------------------------------------------------------------------------
  %% Create initial conditions.
  % Check the target radius
  if(target_radius == 0)
    % Precisely zero target radius will not work.  
    %   Make it small relative to the size of the grid.
    target_radius = small_target * max(g.dx);
  else
    % Make sure that the nonzero target size is not too small.
    assert(target_radius >= small_target * max(g.dx))
  end

  switch(dim)
    case 2
      data = shapeSphere(g, target, target_radius);
    case 3
      % Cylinder aligned with third dimension provides a target which
      % is a circle for any heading of the robot.
      data = shapeCylinder(g, 3, [ target; 0 ], target_radius);
  end

  %---------------------------------------------------------------------------
  %% Deal with obstacle.
  % For the 3D case, we assume that the model can accept a 2D map and
  % expand it in an orientation agnostic manner into 3D.
  
  % Create obstacle array if necessary. 
  if isstruct(obstacle)
    % Set up an implicit surface function which roughly corresponds to the obstacle.
    
    % We'll use second order accurate approximations in the reinit.  Note
    % that the zero level set capture code is only first order accurate, so
    % there seems little reason to go to much higher order accuracy in the
    % rest of the calculation.
    accuracy_reinit = 'medium';
    
    % Saturation for the distance from the obstacle in the signed distance
    % function.
    t_reinit = 2;
    
    % Set up an initial function whose zero level set is the obstacle
    % boundary.  This will be an implicit surface function, but not signed
    % distance.  We are assuming that the input cost map has values in [0,
    % 100] and that 50 is the boundary of the obstacle.
    data0_reinit = 2 * t_reinit * ((obstacle.cost >= 50) - 0.5);
    fprintf('Computing obstacle signed distance function.\n');
    obstacle_lsf = -signedDistanceIterative(g2, data0_reinit, accuracy_reinit, t_reinit);

    % I tried a "smoother" mapping from cost map to initial implicit
    % surface function, but for some reason this initial condition caused
    % signedDistanceIterative() to blow up at one point along the boundary.
    %
    %data0_reinit = 2 * t_reinit * ((50 - obstacle.cost) / 100);

    
  elseif ischar(obstacle)
    switch(lower(obstacle))
      case 'none'
        % Create a level set function whose value is much larger than zero
        % to indicate that there are no obstacles nearby.
        obstacle_lsf = +inf;
      case 'infinite_wall'
        % Choice of parameters specified in help comments.
        obstacle_lsf = lsf_infinite_wall(g, [ 0, 0 ], 0.5);
      case 'finite_wall'
        % Choice of parameters specified in help comments.
        obstacle_lsf = lsf_finite_wall(g, [ 0, 2 ], 0.5);
      otherwise
        error('Unknown obstacle string: %s');
    end
  elseif isa(obstacle, 'function_handle')
    % Make a call to generate the implicit surface function, as specified
    % in the help comments.
    obstacle_lsf = obstacle(g.xs{:});
  else
    error('Bad format for obstacle: %s', class(obstacle));
  end

  % Tell the model about the obstacle function.
  model = model.setObstacle(obstacle_lsf, boundary_layer);
  obstacle_indicator = (obstacle_lsf <= 0);
  
  % We could also set up a postTimestep masking to ensure that the reach
  % set does not enter the obstacle; however, hopefully it will be
  % sufficient for the speed to go gradually to zero.
  
  %---------------------------------------------------------------------------
  % Exit prematurely if requested.
  if dryRun
      mttr = [];
      return;
  end
  
  %---------------------------------------------------------------------------
  % Set up spatial approximation scheme.
  schemeFunc = @termLaxFriedrichs;
  schemeData.hamFunc = @(t, data, deriv, schemeData) model.hamFunc(t, data, deriv, schemeData);
  schemeData.partialFunc = @(t, data, derivMin, derivMax, schemeData, dim) model.partialFunc(t, data, derivMin, derivMax, schemeData, dim);
  schemeData.grid = g;
  schemeData.dissFunc = @artificialDissipationGLF;
  schemeData.derivFunc = derivFunc;

  %---------------------------------------------------------------------------
  % Restrict the Hamiltonian so that reachable set only grows.
  %   The Lax-Friedrichs approximation scheme MUST already be completely set up.
  innerFunc = schemeFunc;
  innerData = schemeData;
  clear schemeFunc schemeData;

  % Wrap the true Hamiltonian inside the term approximation restriction routine.
  schemeFunc = @termRestrictUpdate;
  schemeData.innerFunc = innerFunc;
  schemeData.innerData = innerData;
  schemeData.positive = 0;

  %---------------------------------------------------------------------------
  % Set up minimum time to reach recording using postTimestepFunc.
  integratorOptions = odeCFLset(integratorOptions, 'postTimestep', @postTimestepTTR);

  % Initialize the minimum time to reach function by calling the
  % postTimestepFunc once with initial data.
  y = data(:);
  [ y, schemeData ] = feval(@postTimestepTTR, t0, y, schemeData);
  data = reshape(y, g.shape);

  %---------------------------------------------------------------------------
  % Initialize Display
  if visualize
    f = figure;

    % Set up subplot parameters if necessary.
    if use_subplots
      rows = ceil(sqrt(plot_steps)); %#ok<UNRCH>
      cols = ceil(plot_steps / rows);
      plotNum = 1;
      subplot(rows, cols, plotNum);
    end

    h = visualizeLevelSet(g, data, display_type, level, [ 't = ' num2str(t0) ]);
    hold on;

  end

  %---------------------------------------------------------------------------
  % Loop until tMax (subject to a little roundoff).
  t_now = t0;
  start_time = cputime;
  while(t_max - t_now > small * t_max)

    % Reshape data array into column vector for ode solver call.
    y0 = data(:);

    % How far to step?
    t_span = [ t_now, min(t_max, t_now + t_plot) ];

    % Take a timestep.
    [ t, y, schemeData ] = feval(integratorFunc, schemeFunc, t_span, y0,...
                                 integratorOptions, schemeData);
    t_now = t(end);

    % Get back the correctly shaped data array
    data = reshape(y, g.shape);

    if visualize
      if pause_after_plot
        % Wait for last plot to be digested.
        pause; %#ok<UNRCH>
      end

      % Get correct figure, and remember its current view.
      figure(f);

      % Delete last visualization if necessary.
      if(delete_last_plot && ~use_subplots)
        delete(h);
      end

      % Move to next subplot if necessary.
      if use_subplots
        plotNum = plotNum + 1; %#ok<UNRCH>
        subplot(rows, cols, plotNum);
      end

      % Create new visualization.
      h = visualizeLevelSet(g, data, display_type, level, [ 't = ' num2str(t_now) ]);
    end

  end

  end_time = cputime;
  fprintf('Total execution time %g seconds\n', end_time - start_time);

  % Extract the minimum time to reach function from the schemeData
  % structure. Reshape it into an array, and replace the +inf entries with
  % NaN to make the visualization more pleasant.
  mttr = reshape(schemeData.ttr, g.shape);
  mttr(isinf(mttr)) = NaN;

end
