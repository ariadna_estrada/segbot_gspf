classdef RobotModel2D
  % RobotModel2D: Class containing a holonomic robot model.
  %
  % This class implements a model of a planar robot which can move in any
  % direction at full speed.  The dynamics are:
  %
  %   \dot x    = b_1
  %	  \dot y    = b_2
  %
  % where input ||b||_2 \leq 1 is trying to hit the target.
  
  % Created by Ian Mitchell, 2014/07/07

  % Copyright 2014 Ian M. Mitchell (mitchell@cs.ubc.ca).
  % This software is used, copied and distributed under the licensing 
  % agreement contained in the file LICENSE in the top directory of 
  % the distribution.
  
  %----------------------------------------------------------------------
  %----------------------------------------------------------------------
  properties
    
    % - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    %% Model parameters.

    % Maximum speed in any direction (when far from any obstacles).
    input_bound

    % Obstacle signed distance function (negative inside the obstacle,
    % positive outside).  The robot's speed decreases linearly as it
    % approaches the obstacle, and speed drops to zero at the obstacle.
    % Use a value of +inf to indicate that there is no obstacle.
    lsf_obstacle
    
    % Width of the boundary layer around the obstacle where the robot's
    % speed decreases.
    bdry_width_obstacle

    % Computed from the obstacle data.  As long as the obstacle data does
    % not change, there is no need to recompute this array.  The speed
    % should take into account the input_bound.
    speed
    
  end % Properties.
  
  %----------------------------------------------------------------------
  %----------------------------------------------------------------------
  methods
    
    %----------------------------------------------------------------------
    function obj = RobotModel2D(input_bound, lsf_obstacle, bdry_width_obstacle)
      % RobotModel2D: Constructor for the RobotModel2D class.
      %
      %   obj = RobotModel2D(input_bound, lsf_obstacle, bdry_width_obstacle)
      %
      % Input Parameters:
      %
      %   input_bound: Maximum speed of the robot.  Optional, default = 1.
      %
      %   lsf_obstacle: Array of size compatible with the arrays that will
      %   be passed to hamFunc().  Obstacle signed distance function
      %   (negative inside the obstacle, positive outside).  The robot's
      %   speed decreases linearly as it approaches the obstacle, and speed
      %   drops to zero at the obstacle.  Optional, default = +inf (no
      %   obstacle).
      %
      %   bdry_width_obstacle: Positive scalar.  Width of the boundary
      %   layer around the obstacle where the robot's speed decreases.
      %   Optional, default = 0 (no boundary layer).
      %
      % Output Parameters:
      %
      %   obj: BicycleVPhiModel object.  You can access the grid that is
      %   constructed through obj.getGrid().

      if(nargin < 1)
        obj.input_bound = 1;
      else
        obj.input_bound = input_bound;
      end
      
      if(nargin < 2)
        obj = obj.setObstacle();
      elseif(nargin < 3)
        obj = obj.setObstacle(lsf_obstacle);
      else
        obj = obj.setObstacle(lsf_obstacle, bdry_width_obstacle);
      end
      
    end % RobotModel2D() constructor.
    
    %----------------------------------------------------------------------
    function obj = setObstacle(obj, lsf_obstacle, bdry_width_obstacle)
      % setObstacle: setter function for the obstacle attributes.
      %
      %   obj = setObstacle(obj, lsf_obstacle, bdry_width_obstacle)
      %
      % Input parameters:
      %
      %   lsf_obstacle: Array of size compatible with the arrays that will
      %   be passed to hamFunc().  Obstacle signed distance function
      %   (negative inside the obstacle, positive outside).  The robot's
      %   speed decreases linearly as it approaches the obstacle, and speed
      %   drops to zero at the obstacle.  Optional, default = +inf (no
      %   obstacle).
      %
      %   bdry_width_obstacle: Positive scalar.  Width of the boundary
      %   layer around the obstacle where the robot's speed decreases.
      %   Optional, default = 0 (no boundary layer).
      %
      % Output parameters:
      %
      %   obj: The modified Robot2D object.
      
      if(nargin < 2)
        obj.lsf_obstacle = +inf;
      else
        obj.lsf_obstacle = lsf_obstacle;
      end
      if(nargin < 3)
        obj.bdry_width_obstacle = 0;
      else
        obj.bdry_width_obstacle = bdry_width_obstacle;
      end
      
      obj = obj.computeSpeed();
    end
   
    %----------------------------------------------------------------------
    function obj = setCostmap(obj, costmap)
      % setCostmap: alternative method for defining obstacles
      %
      %   obj = setCostmap(obj, costmap)
      %
      % Input parameters:
      %
      %   costmap: Array of size compatible with the arrays that will
      %   be passed to hamFunc(). A function which expresses a "cost" of
      %   occupancy, normalized to the range [0,100], where cost=100
      %   denotes an obstacle and cost=0 denotes free space far from any
      %   obstacle.
      %   After setting this costmap, speed will be set to the inverse of
      %   the cost and renormalized to [0,1]. lsf_obstacle will be +inf.
      %
      % Output parameters:
      %
      %   obj: The modified Robot2D object.
      
      obj.lsf_obstacle = +inf;
      obj.speed = 1 - costmap/100;
      
      % Actual speed is scaled by input_bound.
      obj.speed = obj.speed * obj.input_bound;
    end
      
    %----------------------------------------------------------------------
    function obj = computeSpeed(obj)
      % computeSpeed: Determines the speed adjustment due to an obstacle.
      %
      %   obj = computeSpeed(obj)
      %
      % This method is intended to be used as a private method.  It should
      % be called whenever the obstacle function is modified.
      %
      % Input parameters:
      %
      %   None.
      %
      % Output parameters:
      %
      %   obj: The modified Robot2D object.

      assert(obj.input_bound > 0);
      assert(obj.bdry_width_obstacle >= 0);
      
      if(obj.lsf_obstacle == +inf)
        obj.speed = +1;
      else
        if(obj.bdry_width_obstacle == 0)
          % There is no boundary layer; the speed drops from one to zero
          % discontinuously at the obstacle boundary.
          obj.speed = ones(size(obj.lsf_obstacle));
        else
          % Speed starts at zero at the obstacle, rises to one at the edge
          % of the boundary layer.
          obj.speed = obj.lsf_obstacle / obj.bdry_width_obstacle;
          % Speed can never be more than one.
          obj.speed = min(obj.speed, 1);
        end
        % Inside the obstacle, the speed is zero.
        obj.speed(obj.lsf_obstacle <= 0) = 0;
      end
      
      % Actual speed is scaled by input_bound.
      obj.speed = obj.speed * obj.input_bound;
    end
    
    %----------------------------------------------------------------------
    function [ ham_value, schemeData ] = hamFunc(obj, ~, ~, deriv, schemeData)
      % hamFunc: analytic Hamiltonian.
      %
      %   [ ham_value, schemeData ] = hamFunc(t, data, deriv, schemeData)
      %
      % This function implements the hamFunc prototype for the 2D robot
      % which can move in any direction at the same speed.
      %
      % Input parameters:
      %
      %   t: Time at beginning of timestep.
      %
      %   data: Data array.
      %
      %   deriv: Cell vector of the costate (\grad \phi).
      %
      %   schemeData: A structure (see below).
      %
      % Output parameters:
      %
      %   ham_value: The analytic hamiltonian.
      %
      %   schemeData: The schemeData structure (not modified).
      %
      % schemeData is a structure containing data specific to this Hamiltonian
      % For this function it does not contain anything useful.

      ham_value = -(-obj.speed .* sqrt(deriv{1} .^2 + deriv{2} .^2));
      
    end % function hamFunc().



    %----------------------------------------------------------------------
    function alpha = partialFunc(obj, ~, ~, ~, ~, ~, ~)
    % partialFunc: Hamiltonian partial function.
    %
    %   alpha = partialFunc(t, data, derivMin, derivMax, schemeData, dim)
    %
    % This function implements the partialFunc prototype for the 2D robot
    % which can move in any direction at the same speed.
    %
    % It calculates the extrema of the absolute value of the partials of the 
    % analytic Hamiltonian with respect to the costate (gradient).
    %
    % Parameters:
    %
    %   t: Time at beginning of timestep.
    %
    %   data: Data array.
    %
    %   derivMin: Cell vector of minimum values of the costate (\grad \phi).
    %
    %   derivMax: Cell vector of maximum values of the costate (\grad \phi).
    %
    %   schemeData: A structure (see below).
    %
    %   dim: Dimension in which the partial derivatives is taken.
    %
    % Output Parameters:
    %
    %   alpha: Maximum absolute value of the partial of the Hamiltonian with
    %   respect to the costate in dimension dim for the specified range of
    %   costate values (O&F equation 5.12). Note that alpha can (and should) be
    %   evaluated separately at each node of the grid.
    %
    % schemeData is a structure containing data specific to this Hamiltonian
    % For this function it does not contain anything useful.

    alpha = obj.speed;
  
    end % function partialFunc().
    
  end % methods.
  
end % classdef.
