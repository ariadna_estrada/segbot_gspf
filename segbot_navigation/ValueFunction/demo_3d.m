% Script to demonstrate the use of ttr and upwindFirstValueFcnGradient
% for the unicycle (3D) robot.
%
% Ian Mitchell, 2015/03/18

% Compute a minimum time to reach approximation for the finite wall
% scenario.  Conveniently, the code also instantiates a suitable grid and
% obstacle function.  If you would like to watch the reachable set grow,
% change the first argument to true.
[ mttr, g, obs ] = ttr(false, 'finite_wall', [ -2.0; 0.0 ], 3);

% Compute an upwinded approximation of the gradient.
grad = upwindFirstValueFcnGradient(mttr, g.dx, obs);

% The resulting gradient includes some rather large magnitudes near the
% obstacle.  If you would like to clean them up, you can use max() and
% min() to limit them; however, be careful since for some reason max() and
% min() do not propagate NaN (eg: max(1, nan) = 1).  If necessary, you can
% also flip the direction of the gradient so that it points toward the
% goal.
action = cell(g.dim, 1);
for d = 1 : g.dim
  action{d} = max(min(grad{d}, +1), -1);
  % Put back the NaN values.
  action{d}(isnan(grad{d})) = nan;
  
  % Flip the sign if desired.
  %action{d} = -action{d};
end


%% Show the result as slices in the heading direction.
show_gradients(g, action, 50);
