function [] = show_gradients( g, action, num_slices )
%SHOW_GRADIENTS A nice way to visualize 3D gradients.

  fig = gcf();
  clf(fig);

  S.current_slice = 0;
  S.num_slices = num_slices;
  S.g = g;
  S.action = action;
  
  slice = get_index(g, S.current_slice, num_slices);
  [dx, dy, vx, vy] = get_data_for_slice(g, action, slice);

  hold on;
  % The gradient itself.
  S.gplot = quiver(g.xs{1}(:,:,slice), g.xs{2}(:,:,slice), dx, dy);
  % The projection of the gradient onto the current motion vector.
  S.vplot = quiver(g.xs{1}(:,:,slice), g.xs{2}(:,:,slice), vx, vy);

  % Show which slice.
  S.title = title(get_title_for_slice(g, slice));
  % Adjust to use square aspect ratio.
  axis image;
  
  guidata(fig, S);
  set(fig, 'KeyPressFcn', @keypress_fn);

end

function [] = keypress_fn(H,E)
  S = guidata(H);
  switch E.Key
    case 'rightarrow'
      S.current_slice = mod(S.current_slice - 1, S.num_slices);
    case 'leftarrow'
      S.current_slice = mod(S.current_slice + 1, S.num_slices);
    otherwise
      return;
  end
  
  slice = get_index(S.g, S.current_slice, S.num_slices);
  [dx, dy, vx, vy] = get_data_for_slice(S.g, S.action, slice);

  set(S.gplot, 'udata', dx, 'vdata', dy);
  set(S.vplot, 'udata', vx, 'vdata', vy);
  set(S.title, 'string', get_title_for_slice(S.g, slice));

  guidata(H, S);
end

function [index] = get_index(g, i, num_slices)
  index = round(i * g.N(3) / (num_slices)) + 1;
end

function [dx, dy, vx, vy] = get_data_for_slice(g, action, slice)
  dx = -action{1}(:,:,slice);
  dy = -action{2}(:,:,slice);
  theta = g.vs{3}(slice);
  v = dx.*cos(theta) + dy.*sin(theta);
  vx = v.*cos(theta);
  vy = v.*sin(theta);
end

function [title] = get_title_for_slice(g, slice)
  title = {sprintf('\\theta = %4.2f', g.vs{3}(slice));...
    'Use the arrow keys to rotate the view'};
end
