function lsf_wall = lsf_infinite_wall(grid, center, half_width)
% infinite_wall: Implicit surface function for the (semi-)infinite wall
%
%   lsf_wall = infinite_wall(grid, center, width)
%
% Constructs an implicit surface (or level set) function for the
% (semi-)infinite wall.  The wall extends infinitely far in the negative y
% direction. It is capped by a semicircle whose center is provided.  The
% radius of the semicircle is the half-width of the wall. The implicit
% surface function is negative inside the wall, zero on its boundary and
% positive outside.
%
% Input parameters:
%
%   grid: A grid structure from ToolboxLS.  Assumed to have at least two
%   dimensions.  Dimensions beyond two are ignored so the implicit surface
%   function is constant in all other dimensions; in other words, it is as
%   if the wall extended infinitely in both directions in all other
%   dimensions.
%
%   center: Column vector with at least two elements.  The center of the
%   semicircle which caps the wall.  The first coordinate implicitly
%   defines the center of the wall as well.   Only the first two
%   coordinates are used.  Optional.  Default is the origin.
%
%   half_width: Positive scalar.  The width of the wall is double this
%   value.  This value is also the radius of the semicircle which caps the
%   wall.  Optional. Default is 5 * max(grid.dx).
%
% Output parameters:
%
%   lsf_wall: Array of size grid.shape.  Implicit surface function for the
%   wall.

% Created by Ian Mitchell, 2014/07/03

  % Make sure we have access to the ToolboxLS functions.
  run addPathToKernel;

  %-----------------------------------------------------------------------
  %% Handle the default input parameter values.
  if(nargin < 2)
    center = zeros(grid.dim, 1);
  end
  if(nargin < 3)
    half_width = 5 * max(grid.dx);
  end
  
  %-----------------------------------------------------------------------
  %% Construct the components of the wall.
  % The wall is a slab up to the center point, and then a semicircular cap
  % above that.
  %-----------------------------------------------------------------------
  % For the cap, use a cylinder to handle the case where the grid has more
  % than two dimensions.
  if(grid.dim > 2)
    ignore_dims = 2 + (1:grid.dim - 2)';
    lsf_cap = shapeCylinder(grid, ignore_dims, center, half_width);
  else
    lsf_cap = shapeSphere(grid, center, half_width);
  end
  
  %-----------------------------------------------------------------------
  % The slab extends infinitely far in every dimension except x.  Note that
  % we don't need to worry about the upper limit of the slab in the
  % y-direction because the implicit surface function for the slab is
  % ignored above the center point (that would not be true if we used a
  % union of shapes).
  lower = -inf * ones(grid.dim);
  upper = +inf * ones(grid.dim);
  lower(1) = center(1) - half_width;
  upper(1) = center(1) + half_width;
  lsf_slab = shapeRectangleByCorners(grid, lower, upper);
  
  %-----------------------------------------------------------------------
  %% Construct the wall.
  % We could just take the union (eg: maximum) of the two implicit surface
  % functions, but that does not generate a proper signed distance
  % function.  Instead we will extract appropriate parts of the two
  % components.
  below_center = grid.xs{2} < center(2);
  lsf_wall = below_center .* lsf_slab + ~below_center .* lsf_cap;
  
end
  