% Script to demonstrate the use of ttr and upwindFirstValueFcnGradient
% for the holonomic (2D) robot.
%
% Ian Mitchell, 2014/07/10

% Compute a minimum time to reach approximation for the finite wall
% scenario.  Conveniently, the code also instantiates a suitable grid and
% obstacle function.  If you would like to watch the reachable set grow,
% change the first argument to true.
[ mttr, g, obs ] = ttr(false);

% Compute an upwinded approximation of the gradient.
grad = upwindFirstValueFcnGradient(mttr, g.dx, obs);

% The resulting gradient includes some rather large magnitudes near the
% obstacle.  If you would like to clean them up, you can use max() and
% min() to limit them; however, be careful since for some reason max() and
% min() do not propagate NaN (eg: max(1, nan) = 1).  If necessary, you can
% also flip the direction of the gradient so that it points toward the
% goal.
action = cell(g.dim, 1);
for d = 1 : g.dim
  action{d} = max(min(grad{d}, +1), -1);
  % Put back the NaN values.
  action{d}(isnan(grad{d})) = nan;
  
  % Flip the sign if desired.
  action{d} = -action{d};
end

% Show the result.  The "{:}" notation generates a "comma separated list"
% of the elements of the cell vector, which is just perfect for passing as
% the arguments of functions like quiver, ndgrid, interpn, etc.
quiver(g.xs{:}, action{:});
% Adjust to use square aspect ratio.
axis image;
