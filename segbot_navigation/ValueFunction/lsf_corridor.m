function lsf_wall = lsf_corridor(grid, half_width)
% narrow_corridor: A narrow corridor between two walls.
%
%   lsf_wall = lsf_corridor(grid, half_width)
%
% Constructs an implicit surface (or level set) function for a narrow
% corridor scenario.  The narrow corridor runs parallel and centered on the
% positive x axis.  The walls then turn and run along the y axis (so there
% are no walls for negative values of x).  The implicit surface
% function is negative inside the wall, zero on its boundary and positive
% outside.
%
% Input parameters:
%
%   grid: A grid structure from ToolboxLS.  Assumed to have at least two
%   dimensions.  Dimensions beyond two are ignored so the implicit surface
%   function is constant in all other dimensions; in other words, it is as
%   if the wall extended infinitely in both directions in all other
%   dimensions.
%
%   half_width: Positive scalar.  The width of the corridor is double this
%   value.  Optional. Default is 5 * max(grid.dx).  Default is also used if
%   half_width isempty().
%
% Output parameters:
%
%   lsf_wall: Array of size grid.shape.  Implicit surface function for the
%   wall.

% Created by Ian Mitchell, 2015/09/26

  %-----------------------------------------------------------------------
  %% Handle the default input parameter values.
  if((nargin < 2) || isempty(half_width))
    half_width = 5 * max(grid.dx);
  end
  
  %-----------------------------------------------------------------------
  %% Construct the components of the corridor.

  % Construct the upper wall.
  lower = -inf * ones(grid.dim);
  upper = +inf * ones(grid.dim);
  % The corridor ends at the y axis.
  lower(1) = 0;
  % The corridor extends half_width above the x axis.
  lower(2) = +half_width;
  % Construct the corridor as the complement (the negative) of a wall in 
  % the same spot.
  lsf_upper = shapeRectangleByCorners(grid, lower, upper);
  
  % Now flip the array in the y dimension to get the lower wall.
  lsf_lower = fliplr(lsf_upper);
  
  % Corridor map is the union of those two walls.
  lsf_wall = shapeUnion(lsf_lower, lsf_upper);  
end
