function [ action ] = toAction( grad )
%TOACTION Clamp partial derivatives to [-1,+1].
% 
% Input:
%   
%   grad: gradient in the form returned by upwindFirstValueFcnGradient.

action = cell(size(grad));
for d = 1 : size(grad,1)
    action{d} = max(min(grad{d}, +1), -1);
    % Put back the NaN values.
    action{d}(isnan(grad{d})) = nan;
end

end

