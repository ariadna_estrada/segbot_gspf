classdef RobotModel3D
  % RobotModel3D: Class containing a non-holonomic robot model.
  %
  % This class implements a model of a planar robot with a 3D pose (x, y,
  % heading) with linear and angular velocity inputs.  In control circles
  % this model is often called the (kinematic) unicycle.  In robotics it is
  % similar to Dubin's car, except that the linear velocity can be altered.
  % The dynamics are:
  %
  %   \dot x     = v cos theta
  %	  \dot y     = v sin theta
  %   \dot theta = omega
  %
  % where v and omega are the inputs.  This model is equivalent to a
  % differential drive robot where v is the average of the left and right
  % wheel velocities and omega is the difference.
  %
  % Obstacles are accounted for by setting linear velocity to zero inside
  % the obstacle (and possibly reducing it gradually toward zero in a
  % boundary layer near the obstacle).  Angular velocity is unaffected by
  % obstacles (I cannot think of any reason to set it to zero in this
  % case).
  
  % Created by Ian Mitchell, 2015/02/18

  % Copyright 2015 Ian M. Mitchell (mitchell@cs.ubc.ca).
  % This software is used, copied and distributed under the licensing 
  % agreement contained in the file LICENSE in the top directory of 
  % the distribution.
  
  %----------------------------------------------------------------------
  %----------------------------------------------------------------------
  properties
    
    % - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    %% Model parameters.

    % Bounds on the inputs.  We will allow the linear velocity bounds to be
    % assymetric (eg: maybe the vehicle cannot drive backwards) but the
    % angular velocity bounds will be symmetric.
    v_max
    v_min
    omega_bound

    % Bound on the magnitude of the velocity.
    v_bound

    % States over which this robot object will be evaluated.
    grid
    
    % Obstacle signed distance function (negative inside the obstacle,
    % positive outside).  The robot's speed decreases linearly as it
    % approaches the obstacle, and speed drops to zero at the obstacle.
    % Use a value of +inf to indicate that there is no obstacle.
    lsf_obstacle
    
    % Width of the boundary layer around the obstacle where the robot's
    % speed decreases.
    bdry_width_obstacle

    % Computed from the obstacle data.  As long as the obstacle data does
    % not change, there is no need to recompute this array.  Note that this
    % array will be the same dimension as lsf_obstacle.
    v_scale

    % Precompute some components of the dynamics for the grid.
    cos_theta
    sin_theta
    
  end % Properties.
  
  %----------------------------------------------------------------------
  %----------------------------------------------------------------------
  methods
    
    %----------------------------------------------------------------------
    function obj = RobotModel3D(grid, v_max, omega_bound, lsf_obstacle, bdry_width_obstacle)
      % RobotModel3D: Constructor for the RobotModel3D class.
      %
      %   obj = RobotModel3D(v_max, omega_bound, grid, lsf_obstacle, bdry_width_obstacle)
      %
      % Input Parameters:
      %
      %   grid: Grid structure describing the domain on which the robot
      %   will operate.
      %
      %   v_max: Positive double.  Maximum linear velocity.  Optional,
      %   default = +1.
      %
      %   omega_bound: Positive double.  Maximum magnitude of angular
      %   velocity. Optional, default = +1.
      %
      %   lsf_obstacle: Array of size compatible with grid.  Obstacle
      %   signed distance function (negative inside the obstacle, positive
      %   outside).  The robot's speed decreases linearly as it approaches
      %   the obstacle, and speed drops to zero at the obstacle.  Optional,
      %   default = +inf (no obstacle).
      %
      %   bdry_width_obstacle: Positive scalar.  Width of the boundary
      %   layer around the obstacle where the robot's speed decreases.
      %   Optional, default = 0 (no boundary layer).
      %
      % Output Parameters:
      %
      %   obj: BicycleVPhiModel object.  You can access the grid that is
      %   constructed through obj.getGrid().

      obj = obj.setGrid(grid);
      
      if(nargin < 2)
        obj.v_max = +1;
      else
        assert(v_max > 0)
        obj.v_max = v_max;
      end
      % For now, assume that the vehicle can travel backward only half as
      % fast as forwards (to break symmetry).  We could set minimum speed
      % to zero (forwards motion only) but then the value function
      % definitely becomes discontinuous.
      obj.v_min = -0.5 * obj.v_max;

      % Set up a bound on the magnitude of the velocity.
      obj.v_bound = max(abs(obj.v_max), abs(obj.v_min));
      
      if(nargin < 3)
        obj.omega_bound = +1;
      else
        assert(omega_bound > 0)
        obj.omega_bound = omega_bound;
      end
      
      if(nargin < 4)
        obj = obj.setObstacle();
      elseif(nargin < 5)
        obj = obj.setObstacle(lsf_obstacle);
      else
        obj = obj.setObstacle(lsf_obstacle, bdry_width_obstacle);
      end
      
    end % RobotModel3D() constructor.

    
    %----------------------------------------------------------------------
    function obj = setObstacle(obj, lsf_obstacle, bdry_width_obstacle)
      % setObstacle: setter function for the obstacle attributes.
      %
      %   obj = setObstacle(obj, lsf_obstacle, bdry_width_obstacle)
      %
      % Input parameters:
      %
      %   lsf_obstacle: Array of size compatible with the arrays that will
      %   be passed to hamFunc() in one of two ways: either it is 3D and
      %   the same size, or it is 2D and the dimensions match the first two
      %   dimensions of the hamFunc() arrays.  In the latter case, it is
      %   assumed that the obstacle is the same for all headings (eg: the
      %   robot is circular).  This array is a signed distance function for
      %   the obstacle (negative inside the obstacle, positive outside).
      %   The robot's speed decreases linearly as it approaches the
      %   obstacle, and speed drops to zero at the obstacle.  Optional,
      %   default = +inf (no obstacle).
      %
      %   bdry_width_obstacle: Positive scalar.  Width of the boundary
      %   layer around the obstacle where the robot's speed decreases.
      %   Optional, default = 0 (no boundary layer).
      %
      % Output parameters:
      %
      %   obj: The modified Robot3D object.
      
      if(nargin < 2)
        obj.lsf_obstacle = +inf;
      else
        % Check that we got a compatible grid and obstacle.  Either the
        % obstacle is a scalar or it matches the grid's size in those
        % dimensions that it has.
        assert(isscalar(lsf_obstacle) || ...
               all(size(lsf_obstacle) == obj.grid.shape(1:ndims(lsf_obstacle))))
        obj.lsf_obstacle = lsf_obstacle;
      end
      if(nargin < 3)
        obj.bdry_width_obstacle = 0;
      else
        obj.bdry_width_obstacle = bdry_width_obstacle;
      end
      
      obj = obj.computeVScale();
    end 
   
    %----------------------------------------------------------------------
    function obj = setCostmap(obj, costmap)
      % setCostmap: alternative method for defining obstacles
      %
      %   obj = setCostmap(obj, costmap)
      %
      % Input parameters:
      %
      %   costmap: Array of size compatible with the arrays that will
      %   be passed to hamFunc(). A function which expresses a "cost" of
      %   occupancy, normalized to the range [0,100], where cost=100
      %   denotes an obstacle and cost=0 denotes free space far from any
      %   obstacle.
      %   After setting this costmap, speed will be set to the inverse of
      %   the cost and renormalized to [0,1]. lsf_obstacle will be +inf.
      %
      % Output parameters:
      %
      %   obj: The modified Robot2D object.
      
      
      % Check that we got a compatible grid and map. It should match the grid
      % size in the first two dimensions.
      assert(all(size(costmap) == obj.grid.shape(1:ndims(costmap))))
      % Now we can expand the 2D costmap into 3D.
      costmap_3D = repmat(costmap, 1, 1, obj.grid.N(3));
      
      obj.lsf_obstacle = +inf;
      obj.v_scale = 1 - costmap_3D/100;
      
    end


    %----------------------------------------------------------------------
    function obj = setGrid(obj, grid)
      % setGrid: Precomputes some elements of the dynamics for a particular grid.
      %
      %   obj = setGrid(obj, grid)
      %
      % Input parameters:
      %
      %   grid: Grid structure.
      %
      % Output parameters:
      %
      % Output parameters:
      %
      %   obj: The modified Robot3D object.

      obj.grid = grid;
      % If we have an obstacle defined, check for compatible sizes.
      % Either the obstacle is a scalar or it matches the grid's size in
      % those dimensions that it has.
      assert(isempty(obj.lsf_obstacle) || isscalar(obj.lsf_obstacle) || ...
             all(size(obj.lsf_obstacle) == obj.grid.shape(1:ndims(obj.lsf_obstacle))))
      % Precompute dynamics.
      obj.cos_theta = cos(obj.grid.xs{3});
      obj.sin_theta = sin(obj.grid.xs{3});

    end

    
    %----------------------------------------------------------------------
    function obj = computeVScale(obj)
      % computeVScale: Determines the linear velocity adjustment due to an obstacle.
      %
      %   obj = computeVScale(obj)
      %
      % This method is intended to be used as a private method.  It should
      % be called whenever the obstacle function is modified.
      %
      % Input parameters:
      %
      %   None.
      %
      % Output parameters:
      %
      %   obj: The modified Robot3D object.

      assert(obj.bdry_width_obstacle >= 0);

      if(obj.lsf_obstacle == +inf)
        obj.v_scale = +1;
      else
        if(ndims(obj.lsf_obstacle) == 2) %#ok<ISMAT>
          % We need to expand the 2D obstacle map into 3D for this
          % calculation.
          lsf_obstacle_3D = repmat(obj.lsf_obstacle, 1, 1, obj.grid.N(3));
        else
          lsf_obstacle_3D = obj.lsf_obstacle;
        end
        
        if(obj.bdry_width_obstacle == 0)
          % There is no boundary layer; the speed drops from one to zero
          % discontinuously at the obstacle boundary.
          obj.v_scale = ones(size(lsf_obstacle_3D));
        else
          % Speed starts at zero at the obstacle, rises to one at the edge
          % of the boundary layer.
          obj.v_scale = lsf_obstacle_3D / obj.bdry_width_obstacle;
          % Speed can never be more than one.
          obj.v_scale = min(obj.v_scale, 1);
        end
        % Inside the obstacle, the speed is zero.
        obj.v_scale(lsf_obstacle_3D <= 0) = 0;
      end
      
    end


    %----------------------------------------------------------------------
    function [ ham_value, schemeData ] = hamFunc(obj, ~, ~, deriv, schemeData)
      % hamFunc: analytic Hamiltonian.
      %
      %   [ ham_value, schemeData ] = hamFunc(t, data, deriv, schemeData)
      %
      % This function implements the hamFunc prototype for the 3D robot
      % with linear and angular velocity inputs.
      %
      % Input parameters:
      %
      %   t: Time at beginning of timestep.
      %
      %   data: Data array.
      %
      %   deriv: Cell vector of the costate (\grad \phi).
      %
      %   schemeData: A structure (see below).
      %
      % Output parameters:
      %
      %   ham_value: The analytic hamiltonian.
      %
      %   schemeData: The schemeData structure (not modified).
      %
      % schemeData is a structure containing data specific to this
      % Hamiltonian.  For this particular Hamiltonian, it is not needed.

      % Check that our input bounds make sense.
      assert(obj.omega_bound > 0)
      assert(obj.v_max > 0)
      assert(obj.v_min < obj.v_max)
      
      % Choose the optimal inputs.  Angular velocity is easy because it
      % enters only into heading derivative.
      omega = -obj.omega_bound * sign(deriv{3});
      % Linear velocity enters into both position derivatives, it may not
      % be symmetric, and it is affected by the presence of obstacles.
      % Figure out what v is multiplied by in the Hamiltonian first.
      mult_v = deriv{1} .* obj.cos_theta + deriv{2} .* obj.sin_theta;
      % Now choose optimal linear velocity.  When mult_v is zero, we
      % arbitrarily choose linear velocity zero (it should not matter).
      v = obj.v_max .* (mult_v < 0) + obj.v_min .* (mult_v > 0);
      % Finally, scale to take the obstacle into account.
      v = v .* obj.v_scale;
      
      % Compute the Hamiltonian.  Remember to multiply by -1 to reverse the
      % time.
      ham_value = -(v .* mult_v + omega .* deriv{3});

      % Holonomic robot motion.
      %ham_value = -(-obj.v_max .* obj.v_scale .* sqrt(deriv{1} .^2 + deriv{2} .^2));

    end % function hamFunc().


    %----------------------------------------------------------------------
    function alpha = partialFunc(obj, ~, ~, ~, ~, ~, dim)
    % partialFunc: Hamiltonian partial function.
    %
    %   alpha = partialFunc(t, data, derivMin, derivMax, schemeData, dim)
    %
    % This function implements the partialFunc prototype for the 3D robot
    % which can move in any direction at the same speed.
    %
    % It calculates the extrema of the absolute value of the partials of the 
    % analytic Hamiltonian with respect to the costate (gradient).
    %
    % Parameters:
    %
    %   t: Time at beginning of timestep.
    %
    %   data: Data array.
    %
    %   derivMin: Cell vector of minimum values of the costate (\grad \phi).
    %
    %   derivMax: Cell vector of maximum values of the costate (\grad \phi).
    %
    %   schemeData: A structure (see below).
    %
    %   dim: Dimension in which the partial derivatives is taken.
    %
    % Output Parameters:
    %
    %   alpha: Maximum absolute value of the partial of the Hamiltonian with
    %   respect to the costate in dimension dim for the specified range of
    %   costate values (O&F equation 5.12). Note that alpha can (and should) be
    %   evaluated separately at each node of the grid.
    %
    % schemeData is a structure containing data specific to this Hamiltonian
    % For this function it does not contain anything useful.

    switch(dim)
      case { 1, 2 }
        % Very simplistic overapproximation: Assume maximum magnitude
        % linear velocity everywhere.
        alpha = obj.v_bound;
      case 3
        alpha = obj.omega_bound;
      otherwise
        error('Invalid dim: %f', dim);
    end
    
    end % function partialFunc().
    
  end % methods.
  
end % classdef.
