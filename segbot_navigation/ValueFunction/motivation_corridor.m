% Script to demonstrate the problems with having a v-shaped valley in 
% the value function.
%
% Ian Mitchell, 2015/09/27

%------------------------------------------------------------------------                   
%% Get the data.
data_filename = 'motivation_corridor';
try

  % Try to load the data for this example rather than recomputing every time.
  load(data_filename);
  % Reconstruct the xs field, which should not be stored (it is very big).
  g = processGrid(g);
  
catch me

  % Something went wrong.  We will naively assume it was a missing data
  % file, and recompute.  It would be better to check the type of the
  % exception and rethrow if necessary.
  
  % Compute a minimum time to reach approximation for the finite wall
  % scenario.  Conveniently, the code also instantiates a suitable grid and
  % obstacle function.  If you would like to watch the reachable set grow,
  % change the first argument to true.
  [ mttr, g, obstacle_lsf, speed ] = ttr_corridor(false, 0.6);

  % Save the results so we have it next time.
  g = rmfield(g, 'xs');
  save(data_filename, 'mttr', 'g', 'obstacle_lsf', 'speed');
  g = processGrid(g);
end

%------------------------------------------------------------------------
%% Compute an upwinded approximation of the gradient.
grad = upwindFirstValueFcnGradient(mttr, g.dx, (obstacle_lsf <= 0));

% The resulting gradient includes some rather large magnitudes near the
% obstacle.  If you would like to clean them up, you can use max() and
% min() to limit them; however, be careful since for some reason max() and
% min() do not propagate NaN (eg: max(1, nan) = 1).  If necessary, you can
% also flip the direction of the gradient so that it points toward the
% goal.
action = cell(g.dim, 1);
for d = 1 : g.dim
  action{d} = max(min(grad{d}, +1), -1);
  % Put back the NaN values.
  action{d}(isnan(grad{d})) = nan;
  
  % Flip the sign if desired.
  action{d} = -action{d};
end

%------------------------------------------------------------------------                   
%% Plot the scenario, contours and vector field.
figure

% First the scenario.
subplot(3,1,1);
% We'd like the walls to be gray, not black
gray_colormap = gray(21);
gray_colormap = gray_colormap(16:end, :);
colormap(gray_colormap);

contourf(g.xs{:}, obstacle_lsf, [ -10, 0 ]);
hold on;
axis image;

% Include the goal location.  The marker is not quite the same size as the
% actual goal, but if we used contour() to plot the actual goal then the
% contourf() patches start to misbehave.  Stupid matlab.
plot(9.0, 0, 'bo', 'LineWidth', 2);

% Now the contours.
subplot(3,1,2);
contour(g.xs{:}, mttr, 0:1.0:16, 'k-');
axis image;

% Finally the vector field.
subplot(3,1,3);

% We need to downsample the grid considerably to see anything useful.
downsample = cell(g.dim, 1);
for d = 1 : g.dim
  downsample{d} = 1 : 5 : g.N(d);
end
quiver(g.xs{1}(downsample{:}), g.xs{2}(downsample{:}), action{1}(downsample{:}), action{2}(downsample{:}), 0.3);
% Adjust to use square aspect ratio.
axis image;

%------------------------------------------------------------------------
%% Show the problem with state uncertainty.

% Need some sample states in 2D.  Scale and shift so that they are to the
% left of the corridor opening.
state_samples_count = 50;
state_samples = cell(g.dim, 1);
state_samples{1} = 2 * rand(1, state_samples_count) - 2;
state_samples{2} = 2.5 * rand(1, state_samples_count) - 1.25;

% Evaluate the action at each sample.
action_samples = cell(g.dim, 1);
for d = 1 : g.dim
  action_samples{d} = interpn(g.xs{:}, action{d}, state_samples{:});
end

% Visualize the result.
figure;
contour(g.xs{:}, mttr, 0:1.0:16, 'c-');
hold on;
axis image;
plot(state_samples{:}, 'k.', 'MarkerSize', 15);
quiver(state_samples{:}, action_samples{:}, 0.5, 'k-', 'LineWidth', 2);
% Focus in on the relevant part of the plot.
axis([ -3, +1, -1.5, +1.5 ]);

%------------------------------------------------------------------------
%% Generate some sample paths using traditional methods.

% The ODE integrators do not take well to NaN values.  Modify the action array
% to create gradients which point back toward the corridor when we are in
% the walls.
wall_grad = upwindFirstValueFcnGradient(obstacle_lsf, g.dx, false);
ode_action = cell(g.dim, 1);
for d = 1 : g.dim
  ode_action{d} = action{d};
  ode_action{d}(isnan(action{d})) = 0;
  %ode_action{d}(obstacle = action{d} .* (obstacle_lsf > 0) + wall_grad{d} .* (obstacle_lsf <= 0);
end

% Construct an ODE for this vector field.  We'll specialize to 2D to avoid
% writing a loop to interpolate each dimension.
ode_func = @(t, y) [ interpn(g.xs{:}, ode_action{1}, y(1,:), y(2,:));
                     interpn(g.xs{:}, ode_action{2}, y(1,:), y(2,:)); ];
                   
%% Run a forward Euler integrator through the vector field.

% From what initial condition should we start?  Choose a point just inside
% the corridor but well away from the center of the corridor.
y0_fe = [ 0.0; +0.5 ];
% Step size of FE?  This is supposed to simulate the cycle time of a robot,
% so it should not be too small.
t_step_fe = 0.5;
% How many steps?  Choose enough to demonstrate the mess made at the end.
max_steps_fe = 25;
y_fe = zeros(max_steps_fe, g.dim);
t_fe = t_step_fe * (1 : max_steps_fe)';
y_fe(1,:) = y0_fe';
for i = 2 : max_steps_fe
  y_fe(i,:) = y_fe(i-1,:) + t_step_fe * ode_func(t_fe(i-1), y_fe(i-1,:)')';
end

%% Run an adaptive timestep integrator through the vector field.

% From what initial condition?  If separate subplots are being generated,
% then use the same IC as FE.
y0_adapt = [ y0_fe(1); y0_fe(2) ];

% Maximum time for adaptive scheme?  Chosen empirically.

[ t_adapt, y_adapt ] = ode15s(ode_func, [ 0 15 ], y0_adapt);

%% Run random ball gradient sampling through the vector field.
% Just ignores NaN gradients.

% From what initial condition?  If separate subplots are being generated,
% then use the same IC as FE.
y0_gs = [ y0_fe(1), y0_fe(2) ];
y_gs = y0_gs;
                     % Variables in the BLO paper
m_gs = 20;           % m
step_gs = 0.5;       % t

k = 2; % iteration
while true
	particle_set = sample_set(y_gs(k-1,:), m_gs);
	[gk, ~] = basic_gradsamp_2d(particle_set, g, action);
	if isnan(gk)
        break
	else
		% no line search, just take the gradient magnitude
		y_gs(k,:) = y_gs(k-1,:) - step_gs*gk';
		k = k + 1;
	end
end

%------------------------------------------------------------------------
%% Visualize the trajectories on top of a contour plot of the value function.
figure

subplot(3,1,1);
hold on;
contour(g.xs{:}, mttr, 0 : 1.0 : 16, 'c-');
axis image;
plot(y_fe(:,1), y_fe(:,2), 'r-o', 'LineWidth', 2);

subplot(3,1,2);
hold on;
contour(g.xs{:}, mttr, 0 : 1.0 : 16, 'c-');
axis image;
plot(y_adapt(:,1), y_adapt(:,2), 'b-*', 'LineWidth', 2);

subplot(3,1,3);
hold on;
contour(g.xs{:}, mttr, 0 : 1.0 : 16, 'c-');
axis image;
plot(y_gs(:,1), y_gs(:,2), 'k-x', 'LineWidth', 2);
