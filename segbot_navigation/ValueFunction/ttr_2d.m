function [ mttr, gridOut, obstacle_func, speedOut ] = ttr_2d(visualize, obstacle, target, dryRun)
% ttr_2d: time to reach for infinite wall/finite wall in holonomic 2D robot
%
%   [ mttr, gridOut, obstacle_func, speedOut ] = ttr_2d(visualize, obstacle, target, dryRun)
%  
% In this example we calculate the minimum time to reach a small ball
% around the target location without passing through obstacles.
%
% This function was originally designed as a script file, so most of the
% options can only be modified in the file.  For example, edit the file to
% change the grid dimension, boundary conditions, motion parameters, etc.
%
% Parameters:
%
%   visualize: Boolean.  If true, then a figure window is created to
%   visualize the reach set as it moves.  Optional, default = false.
%
%   obstacle: String, function_handle or struct.  Signed distance or cost
%   function for the obstacle(s).  Optional, default = 'finite_wall'.
%   If a string, the options are:
%      'none': There are no obstacles.
%      'infinite_wall: call lsf_infinite_wall(grid, [ 0; 0 ], 0.5);
%      'finite_wall': call lsf_finite_wall(grid, [ 0; 1 ], 0.5);
%   If a function_handle, call the function with the grid.xs{:} as its
%   input arguments, and use the first output argument as the signed
%   distance function.
%   If a struct, then it describes a 2D costmap and must have the following
%   fields:
%      resolution: Scalar; width of one grid cell in meters (cells are
%          always square).
%      width: Scalar; number of grid cells in the first dimension.
%      height: Scalar; number of grid cells in the second dimension.
%      origin: 2-vector; the lower-left corner of the map.
%      cost: An array matching the given width and height. The cost should be
%          normalized to the range [0, 100], where 100 = obstacle and 0 =
%          free space. 
%   For structs, the grid will be constructed to match the map. For all
%   formats, an appropriate obstacle_func will be generated.
%
%   target: Vector of length 2.  Coordinates of the target point.
%   Optional, default = [ -2.0; 0.0 ].   Note that for the infinite wall
%   example you probably want to use something further down; for example, [
%   -4.0; -4.0 ].
%   
%   dryRun: Boolean. If true, everything except mttr will be computed (grid,
%   obstacles, and speed). Optional, default = false.
%
% Output parameters:
%
%   mttr: Array. Minimum time to reach function at t_max for each grid
%   node.
%
%   gridOut: Structure. Computational grid used to compute mttr.  See
%   processGrid() for details.
%
%   obstacle_func: Logical array, same size as mttr, which is true inside
%   obstacles and false everywhere else.
%   
%   speedOut: Array, same size as mttr, giving the maximum speed at each
%   location in the grid.

% Copyright 2014 Ian M. Mitchell (mitchell@cs.ubc.ca).
% This software is used, copied and distributed under the licensing 
%   agreement contained in the file LICENSE in the top directory of 
%   the distribution.
%
% Ian Mitchell, 2014/07/07.

  %---------------------------------------------------------------------------
  % You will see many executable lines that are commented out. These are
  % included to show some of the options available; modify the commenting
  % to modify the behavior.

  % This flag suppresses the m-lint warning that an m-lint warning was once
  % being suppressed but is no longer being generated.  Without it, m-lint
  % complains about unreachable code too much (or the fact that formerly
  % unreachable code can now be reached so why are you suppressing that
  % message still).
  %#ok<*MSNU>

  %---------------------------------------------------------------------------
  % Make sure we can see the kernel m-files.
  run addPathToKernel;

  %---------------------------------------------------------------------------
  %% Initialize various variables.
  
  %---------------------------------------------------------------------------
  % Input parameters.
  if(nargin < 1)
    visualize = false;
  end
  if(nargin < 2)
    obstacle = 'finite_wall';
  end
  if(nargin < 3)
    target = [ -2.0; 0.0 ];
  end
  if(nargin < 4)
    dryRun = false;
  end
  
  %---------------------------------------------------------------------------
  % Integration parameters.
  t0 = 0;                      % Start time.
  t_max = 12;                  % End time.

  % How close (relative) do we need to get to tMax to be considered finished?
  small = 100 * eps;

  %---------------------------------------------------------------------------
  % Visualization parameters.
  
  % How many intermediate plots to produce?
  plot_steps = 9;
  
  % Period at which intermediate plots should be produced.
  t_plot = (t_max - t0) / (plot_steps - 1);

  % What level set should we view?
  level = 0;

  % Visualize the reachable set.
  display_type = 'contour';
  
  % Plot at each timestep (overrides tPlot).
  single_step = false;
  
  % Pause after each plot?
  pause_after_plot = false;

  % Delete previous plot before showing next?
  delete_last_plot = true;

  % Plot in separate subplots (set deleteLastPlot = 0 in this case)?
  use_subplots = false;

  %---------------------------------------------------------------------------
  % Integrator parameters.
  
  % Accuracy of the numerical approximation schemes.  A 5/3 scheme makes a
  % noticeable difference, but takes about three times longer than 2/2.
  % Spatial approximation scheme.
  derivFunc = @upwindFirstENO2;
  % Temporal approximation scheme.
  integratorFunc = @odeCFL2;

  % Some integration parameters apply to any integrator.
  integratorOptions = odeCFLset('factorCFL', 0.9, 'stats', 'on');
  if(single_step)
    integratorOptions = odeCFLset(integratorOptions, 'singleStep', 'on'); %#ok<UNRCH>
  end

  %---------------------------------------------------------------------------
  % Problem Parameters.

  % Create the robot model.
  model = RobotModel2D();
  
  % How wide is the boundary layer around the obstacles where the robot
  % speed slows down? (Not applicable for costmaps.)
  boundary_layer = 0.5;
  
  % Radius of the target set. A value of zero will be translated into a
  % value of (smallTarget * g.dx).
  target_radius = 0.2;

  % What is considered a small target?
  small_target = 1.0;

  %---------------------------------------------------------------------------
  %% Create the grid.
  if isstruct(obstacle)
      g.dim = 2;
      g.min = obstacle.origin(:);
      g.max = obstacle.origin(:);
      g.max(1) = g.max(1) + (obstacle.width-1)*obstacle.resolution;
      g.max(2) = g.max(2) + (obstacle.height-1)*obstacle.resolution;
      g.N = [obstacle.width; obstacle.height];
      g.dx = obstacle.resolution;
  else
      g.dim = 2;
      g.min = -5;
      g.max = +5;
      g.N = 101;
  end
  g.bdry = @addGhostExtrapolate;
  g = processGrid(g);

  % Set output argument in case the caller requests it.
  gridOut = g;

  % Check the target radius
  if(target_radius == 0)
    % Precisely zero target radius will not work.  
    %   Make it small relative to the size of the grid.
    target_radius = small_target * max(g.dx);
  end

  %---------------------------------------------------------------------------
  %% Create initial conditions.
  data = shapeSphere(g, target, target_radius);

  %---------------------------------------------------------------------------
  %% Deal with obstacle.
  
  % Create obstacle array if necessary. 
  if isstruct(obstacle)
      % Tell the robot about the obstacles.
      model = model.setCostmap(obstacle.cost);
      obstacle_func = obstacle.cost>=99;
  else
      if ischar(obstacle)
          switch(lower(obstacle))
          case 'none'
              % Create a level set function whose value is much larger than zero
              % to indicate that there are no obstacles nearby.
              obstacles = +inf;
          case 'infinite_wall'
              % Choice of parameters specified in help comments.
              obstacles = lsf_infinite_wall(g, [ 0, 0 ], 0.5);
          case 'finite_wall'
              % Choice of parameters specified in help comments.
              obstacles = lsf_finite_wall(g, [ 0, 2 ], 0.5);
          otherwise
              error('Unknown obstacle string: %s');
          end
      elseif isa(obstacle, 'function_handle')
          % Make a call to generate the implicit surface function, as specified
          % in the help comments.
          obstacles = obstacle(g.xs{:});
      else
          error('Bad format for obstacle: %s', class(obstacle));
      end
      
      % Tell the robot about the obstacles.
      model = model.setObstacle(obstacles, boundary_layer);
      obstacle_func = obstacles<=0;
  end
  
  speedOut = model.speed;
  
  % We could also set up a postTimestep masking to ensure that the reach
  % set does not enter the obstacle; however, hopefully it will be
  % sufficient for the speed to go gradually to zero.
  
  %---------------------------------------------------------------------------
  % Exit prematurely if requested.
  if dryRun
      mttr = [];
      return;
  end
  
  %---------------------------------------------------------------------------
  % Set up spatial approximation scheme.
  schemeFunc = @termLaxFriedrichs;
  schemeData.hamFunc = @(t, data, deriv, schemeData) model.hamFunc(t, data, deriv, schemeData);
  schemeData.partialFunc = @(t, data, derivMin, derivMax, schemeData, dim) model.partialFunc(t, data, derivMin, derivMax, schemeData, dim);
  schemeData.grid = g;
  schemeData.dissFunc = @artificialDissipationGLF;
  schemeData.derivFunc = derivFunc;

  %---------------------------------------------------------------------------
  % Restrict the Hamiltonian so that reachable set only grows.
  %   The Lax-Friedrichs approximation scheme MUST already be completely set up.
  innerFunc = schemeFunc;
  innerData = schemeData;
  clear schemeFunc schemeData;

  % Wrap the true Hamiltonian inside the term approximation restriction routine.
  schemeFunc = @termRestrictUpdate;
  schemeData.innerFunc = innerFunc;
  schemeData.innerData = innerData;
  schemeData.positive = 0;

  %---------------------------------------------------------------------------
  % Set up minimum time to reach recording using postTimestepFunc.
  integratorOptions = odeCFLset(integratorOptions, 'postTimestep', @postTimestepTTR);

  % Initialize the minimum time to reach function by calling the
  % postTimestepFunc once with initial data.
  y = data(:);
  [ y, schemeData ] = feval(@postTimestepTTR, t0, y, schemeData);
  data = reshape(y, g.shape);

  %---------------------------------------------------------------------------
  % Initialize Display
  if visualize
    f = figure;

    % Set up subplot parameters if necessary.
    if use_subplots
      rows = ceil(sqrt(plot_steps)); %#ok<UNRCH>
      cols = ceil(plot_steps / rows);
      plotNum = 1;
      subplot(rows, cols, plotNum);
    end

    h = visualizeLevelSet(g, data, display_type, level, [ 't = ' num2str(t0) ]);
    hold on;

  end

  %---------------------------------------------------------------------------
  % Loop until tMax (subject to a little roundoff).
  t_now = t0;
  start_time = cputime;
  while(t_max - t_now > small * t_max)

    % Reshape data array into column vector for ode solver call.
    y0 = data(:);

    % How far to step?
    t_span = [ t_now, min(t_max, t_now + t_plot) ];

    % Take a timestep.
    [ t, y, schemeData ] = feval(integratorFunc, schemeFunc, t_span, y0,...
                                 integratorOptions, schemeData);
    t_now = t(end);

    % Get back the correctly shaped data array
    data = reshape(y, g.shape);

    if visualize
      if pause_after_plot
        % Wait for last plot to be digested.
        pause; %#ok<UNRCH>
      end

      % Get correct figure, and remember its current view.
      figure(f);

      % Delete last visualization if necessary.
      if(delete_last_plot && ~use_subplots)
        delete(h);
      end

      % Move to next subplot if necessary.
      if use_subplots
        plotNum = plotNum + 1; %#ok<UNRCH>
        subplot(rows, cols, plotNum);
      end

      % Create new visualization.
      h = visualizeLevelSet(g, data, display_type, level, [ 't = ' num2str(t_now) ]);
    end

  end

  end_time = cputime;
  fprintf('Total execution time %g seconds\n', end_time - start_time);

  % Extract the minimum time to reach function from the schemeData
  % structure. Reshape it into an array, and replace the +inf entries with
  % NaN to make the visualization more pleasant.
  mttr = reshape(schemeData.ttr, g.shape);
  mttr(isinf(mttr)) = NaN;

end
