function lsf_wall = lsf_finite_wall(grid, center1, half_width, center2)
% finite_wall: Implicit surface function for the finite wall
%
%   lsf_wall = finite_wall(grid, center1, half_width, center2)
%
% Constructs an implicit surface (or level set) function for the finite
% wall.  The wall runs parallel to the y axis and lies between two
% semi-circular caps centered at the specified locations.  The radius of
% the semicircles is the half-width of the wall. The implicit surface
% function is negative inside the wall, zero on its boundary and positive
% outside.
%
% Input parameters:
%
%   grid: A grid structure from ToolboxLS.  Assumed to have at least two
%   dimensions.  Dimensions beyond two are ignored so the implicit surface
%   function is constant in all other dimensions; in other words, it is as
%   if the wall extended infinitely in both directions in all other
%   dimensions.
%
%   center1: Column vector with at least two elements.  The center of one
%   semicircle which caps the wall.  The first coordinate implicitly
%   defines the center of the wall as well.   Only the first two
%   coordinates are used.  Optional.  Default is the origin.  Note that
%   using the default will result in a very short wall (it will only
%   contain its caps).
%
%   half_width: Positive scalar.  The width of the wall is double this
%   value.  This value is also the radius of the semicircle which caps the
%   wall.  Optional. Default is 5 * max(grid.dx).
%
%   center2: Column vector with at least two elements.  The center of the
%   other semicircle which caps the wall.  The first coordinate must be the
%   same as the first coordinate in center1.  Only the first two
%   coordinates are used.  Optional.  Default is the same as center1, but
%   with the second coordinate multiplied by -1 (so the wall will run just
%   as far above the x-axis as below it).
%
% Output parameters:
%
%   lsf_wall: Array of size grid.shape.  Implicit surface function for the
%   wall.

% Created by Ian Mitchell, 2014/07/03

  %-----------------------------------------------------------------------
  %% Handle the default input parameter values.
  if(nargin < 2)
    center1 = zeros(grid.dim, 1);
  end
  if(nargin < 3)
    half_width = 5 * max(grid.dx);
  end
  if(nargin < 4)
    center2 = center1;
    center2(2) = -center1(2);
  elseif(center1(1) ~= center2(1))
    error('Arguments center1 and center2 must have the same first coordinate.');
  end

  % Reorder the center points so that center1 is above center2.
  if(center1(2) < center2(2))
    center_temp = center1;
    center1 = center2;
    center2 = center_temp;
  end
  
  %-----------------------------------------------------------------------
  %% Construct the components of the wall.
  % The wall is a slab between the center points, and semicircular caps
  % outside that.
  %-----------------------------------------------------------------------
  % For the caps, use a cylinder to handle the case where the grid has more
  % than two dimensions.
  if(grid.dim > 2)
    ignore_dims = 2 + (1:grid.dim - 2)';
    lsf_cap1 = shapeCylinder(grid, ignore_dims, center1, half_width);
    lsf_cap2 = shapeCylinder(grid, ignore_dims, center2, half_width);
  else
    lsf_cap1 = shapeSphere(grid, center1, half_width);
    lsf_cap2 = shapeSphere(grid, center2, half_width);
  end
  
  %-----------------------------------------------------------------------
  % The slab extends infinitely far in every dimension except x.  Note that
  % we don't need to worry about the upper and lower limits of the slab in
  % the y-direction because the implicit surface function for the slab is
  % ignored outside the y internal between the center points (that would
  % not be true if we used a union of shapes).
  lower = -inf * ones(grid.dim);
  upper = +inf * ones(grid.dim);
  lower(1) = center1(1) - half_width;
  upper(1) = center1(1) + half_width;
  lsf_slab = shapeRectangleByCorners(grid, lower, upper);

  %-----------------------------------------------------------------------
  %% Construct the wall.
  % We need to be careful about the case where center1 == center2 (the wall
  % is actually just a circle) to avoid using both lsf_cap1 and lsf_cap2.
  below_center1 = grid.xs{2} < center1(2);
  above_center2 = grid.xs{2} > center2(2);
  lsf_wall = ((below_center1 & above_center2) .* lsf_slab ...
              + (~below_center1 & above_center2) .* lsf_cap1 ...
              + ~above_center2 .* lsf_cap2);
  
end
  