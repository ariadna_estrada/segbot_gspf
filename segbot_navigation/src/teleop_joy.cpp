#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/Joy.h>


class TeleopSegbot
{
public:
  TeleopSegbot();

private:
  void joyCallback(const sensor_msgs::Joy::ConstPtr& joy);

  ros::NodeHandle nh_;

  int linearx_, lineary_, angular_;
  double lx_scale_, ly_scale_, angular_scale_;
  ros::Publisher vel_pub_;
  ros::Subscriber joy_sub_;

};


TeleopSegbot::TeleopSegbot():
  linearx_(1),
  lineary_(3),
  angular_(3)
{

  nh_.param("axis_linearx", linearx_, linearx_);
  nh_.param("axis_lineary", lineary_, lineary_);
  nh_.param("axis_angular", angular_, angular_);
  nh_.param("scale_lineary", ly_scale_, 0.5);
  nh_.param("scale_linearx", lx_scale_, 0.5);
  nh_.param("scale_angular", angular_scale_, 0.5);

  vel_pub_ = nh_.advertise<geometry_msgs::Twist>("cmd_vel", 1);


  joy_sub_ = nh_.subscribe<sensor_msgs::Joy>("joy", 10, &TeleopSegbot::joyCallback, this);

}

void TeleopSegbot::joyCallback(const sensor_msgs::Joy::ConstPtr& joy)
{
  geometry_msgs::Twist twist;
  twist.linear.x = lx_scale_*joy->axes[linearx_];
  twist.linear.y = ly_scale_*joy->axes[lineary_];
  twist.angular.z = angular_scale_*joy->axes[angular_];
  vel_pub_.publish(twist);
}


int main(int argc, char** argv)
{
  ros::init(argc, argv, "teleop_segbot");
  TeleopSegbot teleop_segbot;

  ros::spin();
}
