/*
 * Generate and publish a cost function based on the static occupancy map.
 *
 *  Created on: Mar 14, 2015
 *      Author: Neil Traft
 */

#include <ros/ros.h>
#include <tf/transform_listener.h>
#include <costmap_2d/costmap_2d_ros.h>

int main(int argc, char **argv) {

	// Start ROS.
	ros::init(argc, argv, "costmap");
	ros::NodeHandle nh("~");

	// Create a costmap.
	tf::TransformListener tl(ros::Duration(10));
	costmap_2d::Costmap2DROS costmap("static_costmap", tl);

	ros::spin();

	return EXIT_SUCCESS;
}
