bag = rosbag('simple_bag.bag');
bagselect1 = select(bag, 'Topic', '/nav_kinect/rgb/image_raw');
% bagselect2 = select(bag, 'Topic', '/cmd_vel');
bagselect3 = select(bag, 'Topic', 'particlecloud');

kinect_msgs = readMessages(bagselect1);
amcl_pose_msgs = readMessages(bagselect1);
% cmd_vel_msgs = readMessages(bagselect2);
particle_cloud_msgs = readMessages(bagselect3);


%% Get pose estimates
% Compute robot's pose [x,y,yaw] from amcl message
n = length(amcl_pose_msgs); 
estimated_pose = zeros(n,3);
for i = 1:n
    amclpose = amcl_pose_msgs{i};
    
    amclQuat = [amclpose.Pose.Pose.Orientation.W, ...
                amclpose.Pose.Pose.Orientation.X, ...
                amclpose.Pose.Pose.Orientation.Y, ...
                amclpose.Pose.Pose.Orientation.Z];

    amclRotation = quat2eul(amclQuat);
    amclOrientation = mod(amclRotation(1), 2*pi);

    estimated_pose(i,:) = [amclpose.Pose.Pose.Position.X, ... 
                         amclpose.Pose.Pose.Position.Y, ...
                         amclOrientation];
end

msgList = bag.MessageList;
timestamps = msgList(:,1:2);