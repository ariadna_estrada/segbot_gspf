trialNo = 2;
images_directory = strcat('extracted_images/trial-20180316-', num2str(trialNo),'/');

face_data = csvread(strcat(images_directory, 'viz_data.csv'));
D1 = dir(strcat(images_directory, 'face_images/*.jpg'));
D2 = dir(strcat(images_directory, 'kinect_pov/*.jpg'));
NumImg.face = numel(D1)-1;
NumImg.kinect = numel(D2)-1;
h = gazebo_trials(NumImg, face_data, images_directory);