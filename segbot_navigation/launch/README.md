How to create a new map:

1) To create a new map, use the gmapping launch file:

roslaunch segbot_navigation robot_with_gmapping_v4.launch

This will bring up the necessary tools to create the map and visualize it as it's being computed. 

2) Saving the map

Once you are done with the map you can save it with the following command:
rosrun map_server map_saver -f path/name

NOTE!: This only works if gmapping (one of the tools launched by the first launch file) is still active!

How to run a simulated world:

1) Run gazebo launch file 

roslaunch segbot_navigation segbot_gazebo_eband.launch
