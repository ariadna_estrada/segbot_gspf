%Script to run teleop callback using a node
clear;close all;
rosshutdown;
rosinit('localhost');

% Create ROS publisher for sending out velocity commands
[vel_segbot.Pub, vel_segbot.Msg] = rospublisher('/cmd_vel_mux/input/teleop', 'geometry_msgs/Twist');

% Subscribe to ROS topics
joySub = rossubscriber('joy', {@joyTeleopNodeCallback, vel_segbot, true});
