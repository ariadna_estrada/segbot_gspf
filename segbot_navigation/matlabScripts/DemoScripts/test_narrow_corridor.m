clear;close all;
profile on;
tmpdir = '/home/ariadna/ros_matlab_ws/src/holonomic_robot';
cd(tmpdir);
setup_paths();
rosshutdown;
rosinit('localhost');

global particles
global descentType
global gradientSamples
global actionSamples
global pathHistory

%% Set-up variables

holonomic = false;
save_path = true;

%% Load value functions for all goals

world = 'narrow_passage';
goals = [11.75, 0.25];
goals_vf = load_valuefcn(goals, world);
pathHistory = [];

%% Load office map and create figures
load('narrowPassageMap.mat');

figMap = figure('Name', 'Map');
[viz_plot, toggleBtn] = map_plot_ui(figMap, map, goals);
figSaddle = figure('Name', 'Saddle point classification');
[saddle_plot] = saddle_classification_plots(figSaddle, holonomic);

 %% Create ROS publisher for sending out velocity commands
[vel.holonomic.pub, vel.holonomic.msg] =  rospublisher('/cmd_vel','geometry_msgs/Twist');
[vel.diff.teleop.pub,vel.diff.teleop.msg] = rospublisher('/cmd_vel_mux/input/teleop','geometry_msgs/Twist');
[vel.diff.navi.pub,vel.diff.navi.msg] = rospublisher('/cmd_vel_mux/input/navi','geometry_msgs/Twist');

%% AMCL nomotion_update service
amclSvc.client = rossvcclient('/request_nomotion_update');
amclSvc.req = rosmessage(amclSvc.client);

 %% Subscribe to ROS topics
particleSub = rossubscriber('particlecloud', {@particleTestCallbackFcn, holonomic, viz_plot});
amclSub = rossubscriber('amcl_pose', {@amclTestCallbackFcn, save_path, viz_plot});
joySub = rossubscriber('joy', {@JoyAutoCallbackFcn, holonomic, vel, goals_vf, toggleBtn, amclSvc, saddle_plot});
