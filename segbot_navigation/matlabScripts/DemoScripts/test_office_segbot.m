% close all;
profile on;
%Declare globals
global particles
global gradientSamples
global goal_select
global pathHistory
global classHistory
global classType
global actionSamples
global start
global saddleCount
global pointCount

%Initialize globals
goal_select = 1;
pathHistory = [];
classHistory = [0,0,0,0,0,0,0,0];
saddleCount= 0;
pointCount = 0;


%Simulation variables
model.holonomic = false;
% model.world = 'doorway/lab_map_smooth';
model.world = 'gazebo/office';

switch(model.world)
    case 'gazebo/office'
        load('myOfficeMap.mat');
        model.goals = [-4.3, 2;4.5, -2; 4.7, 2; -2.7, -0.8;-0.1,4.5];
    case 'doorway/lab_map_smooth'
        load('lab_map.mat');
        model.goals = [8.5,4.5; 2,5; 5.5,0.5; 9.5,0.5;4,9.5;0,0];
    otherwise
        warning('Unknown world')
end

model.map = map;

if ~exist('goals_vf', 'var')
    disp('Loading value functions...')
    tic
    goals_vf = load_valuefcn(model.goals, model.world);
    toc
end

% Start GUI
% hObject = mapGUInavigation(model, goals_vf);
hObject = mapGUItest(model,goals_vf);
