% profile on;
close all;
rosshutdown;
rosinit('localhost');

global particles
global gradientSamples
global actionSamples
global pathHistory
global classHistory

%TestRun configuration
holonomic = false;
save_path = true;
classifyJoy = false;

%% Load value functions for all goals

world = 'finitewall';
goals = [0,0;3,0];
goals_vf = load_valuefcn(goals, world);
pathHistory = [];
classHistory = [0;0];

%% Load office map 
load('finitewall.mat');

%Figures for visualization
figMap = figure('Name', 'Map');
[viz_plot, toggleBtn] = map_plot_ui(figMap, map, goals);
figSaddle = figure('Name', 'Saddle point classification');
[saddle_plot] = saddle_classification_plots(figSaddle, holonomic);


 %% Create ROS publisher for sending out velocity commands
[vel.pub, vel.msg] =  rospublisher('/cmd_vel','geometry_msgs/Twist');

%% AMCL nomotion_update service
amclSvc.client = rossvcclient('/request_nomotion_update');
amclSvc.req = rosmessage(amclSvc.client);

 %% Subscribe to ROS topics
particleSub = rossubscriber('particlecloud', {@particleSegbotCallbackFcn, viz_plot});
amclSub = rossubscriber('amcl_pose', {@amclSegbotCallbackFcn, viz_plot});

if classifyJoy
    figInputClass = figure('Name', 'Classification of joystick input');
    [class_plot] = joystick_classification_plot(figInputClass);
    joySub = rossubscriber('joy', {@JoySegbotClassifyCallbackFcn, vel, goals_vf, toggleBtn, saddle_plot, class_plot});
else
    joySub = rossubscriber('joy', {@JoySegbotCallbackFcn, vel, goals_vf, toggleBtn, amclSvc, saddle_plot});
end