%Script to run teleop callback during gmapping
clear;close all;
rosshutdown;
rosinit('localhost');

% Create ROS publisher for sending out velocity commands
[vel_segbot.pub, vel_segbot.msg] = rospublisher('/cmd_vel', 'geometry_msgs/Twist');

% Subscribe to ROS topics
joySub = rossubscriber('joy', {@joySegbotGmapping, vel_segbot});
