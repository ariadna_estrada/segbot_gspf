%Script to run teleop callback using a node
% clear;close all;
% rosshutdown;
% rosinit('localhost');
% 
% % Create ROS publisher for sending out velocity commands
% [vel_segbot.Pub, vel_segbot.Msg] = rospublisher('/cmd_vel', 'geometry_msgs/Twist');
% 
% % Subscribe to ROS topics
% joySub = rossubscriber('joy', {@joyTeleopNodeCallback, vel_segbot, false});
% 
global rand_goal 
%Predefined safe goal locations
%other possible start location 0,9.5;
goals = [1.5, 11.8; -0.5, 14; 0, 9.5; -1.5, 2; 4, -3.5];
n = length(goals);
rand_goal = goals(randi(n),:)

%Simulation variables
model.holonomic = false;
model.world = 'gazebo/two_rooms';
load('two_rooms.mat'); model.map = map;
model.goals = [1.5, 11.8; -0.5, 14; 0, 9.5; -1.5, 2; 4, -3.5];

%Start the GUI
hObject = goalFinderGUI(model);