profile on;
close all;
rosshutdown;
rosinit('localhost');

global particles
global gradientSamples
global actionSamples
global pathHistory
global classHistory
global ParticleSet

%TestRun configuration
holonomic = false;
save_path = true;
classifyJoy = true;

%% Load value functions for all goals

world = 'labmap_obstacles';
goals = [0,0; 3, 9; 4, 6.5; 3, 4.5];
% goals_vf = load_valuefcn(goals, world);
pathHistory = [];
classHistory = [];

%% Load office map 
load('lab_obstacles.mat');

%Figures for visualization
figMap = figure('Name', 'Map');
[viz_plot, toggleBtn] = map_plot_ui(figMap, map, goals);
figSaddle = figure('Name', 'Saddle point classification');
[saddle_plot] = saddle_classification_plots(figSaddle, holonomic);


 %% Create ROS publisher for sending out velocity commands
[vel.pub, vel.msg] =  rospublisher('/cmd_vel','geometry_msgs/Twist');

%% Create AMCL nomotion_update service 
amclSvc.client = rossvcclient('/request_nomotion_update');
amclSvc.req = rosmessage(amclSvc.client);

 %% Subscribe to ROS topics and setup callback functions
particleSub = rossubscriber('particlecloud', {@particleSegbotCallbackFcn, viz_plot});
amclSub = rossubscriber('amcl_pose', {@amclSegbotCallbackFcn, viz_plot});

if classifyJoy
    figInputClass = figure('Name', 'Classification of joystick input');
    [class_plot] = joystick_classification_plot(figInputClass);
    joySub = rossubscriber('joy', {@JoySegbotClassifyCallbackFcn, vel, goals_vf, toggleBtn,  amclSvc, saddle_plot, class_plot});
else
    joySub = rossubscriber('joy', {@JoySegbotCallbackFcn, vel, goals_vf, toggleBtn, amclSvc, saddle_plot});
end