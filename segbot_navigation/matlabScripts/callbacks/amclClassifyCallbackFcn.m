function amclClassifyCallbackFcn( ~, amclpose, plots )
%amclClassifyCallbackFcn retreive and plot estimated position from amcl
%   message: amcl message
%   plots: a vector of plot handles

    global estimatedPose
    global pathHistory
 
    % Compute robot's pose [x,y,yaw] from amcl message
    amclQuat = [amclpose.Pose.Pose.Orientation.W, ...
                amclpose.Pose.Pose.Orientation.X, ...
                amclpose.Pose.Pose.Orientation.Y, ...
                amclpose.Pose.Pose.Orientation.Z];
            
    amclRotation = quat2eul(amclQuat);
    amclOrientation = normalize_angle(amclRotation(1));
    
    estimatedPose = [amclpose.Pose.Pose.Position.X, ... 
                     amclpose.Pose.Pose.Position.Y, ...
                     amclOrientation];
                 
    if nargin > 2 && ~isempty(plots)
        set(plots(3), 'xdata', estimatedPose(1), 'ydata', estimatedPose(2));
    end
    
  
    pathHistory = [pathHistory;estimatedPose];

end
function na =  normalize_angle(angle)
    na = mod(angle, 2*pi);
end

