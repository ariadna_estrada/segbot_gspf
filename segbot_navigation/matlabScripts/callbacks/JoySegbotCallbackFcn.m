function JoySegbotCallbackFcn( ~, message, vel, value_fcn_all, toggleBtn, amclSvc, plot)
%JoyCallbackFcn
%Inputs:
%   -message: joystick message 
%   -vel: 2x1 struct containing /cmd_vel_mux/input/ publisher and message 
%   -value_fcn_all: Value function for all goals
%   -toggleBtn: UIControl for Start/Stop button. 
%   -amclSvc: 1x1 struct contains client and request fields for amcl
%   service request no motion update.


% global holonomic
global start
global particles 
global goal_select


value_fcn = value_fcn_all{goal_select, 2};
particle_set = particles;

estop = get_stop(message.Buttons, toggleBtn);


if start 
    action = computeNextAction(particle_set, value_fcn, toggleBtn, amclSvc, plot);
else
    action = get_teleop(message);
end

if isnan(action)
    warning('Cannot send NaN velocity, changing to zero');
    action = [0, 0];
end

if estop
    send_velocity(vel,[0,0]);
else
    send_velocity(vel, action);
end

end

function [action] = get_teleop(input)
maxX = 0.5;
maxY = 0.2;

% Extract joystick value from the ROS message
joy.X = double(input.Axes(2));
joy.Y = double(input.Axes(4));
joy.mag = sqrt(joy.X^2 + joy.Y^2);
if joy.mag ~= 0
    joy.normX = joy.X / joy.mag; %Normalizing joy input to unit circle 
    joy.normY = joy.Y / joy.mag;
else
    joy.normX = joy.X;
    joy.normY = joy.Y;
end

action = [joy.normX*maxX, joy.normY*maxY];

end
function stop = get_stop(input, btn)
global start
if input(2) == 1
    stop = true;
    disp('stopping...');
    start = 0;
    set(btn, 'String', 'Start', 'Value', 0);
else
    stop = false;
end
end

function send_velocity(vel, action)
vel.msg.Linear.X = action(1);
vel.msg.Angular.Z = action(2);
send(vel.pub,vel.msg);
end
