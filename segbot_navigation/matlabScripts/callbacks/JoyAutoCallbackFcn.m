function JoyAutoCallbackFcn( ~, message, holonomic, vel, value_fcn_all, toggleBtn, amclSvc, plot)
%JoyCallbackFcn
%Inputs:
%   -message: joystick message 
%   -holonomic: boolean. True for holonomic, false for unicycle robot
%   -vel: 2x1 struct containing /cmd_vel_mux/input/ publisher and message 
%   -value_fcn_all: Value function for all goals
%   -toggleBtn: UIControl for Start/Stop button. 
%   -amclSvc: 1x1 struct contains client and request fields for amcl
%   service request no motion update.


% global holonomic
global start
global particles 
global goal_select


if holonomic ndims = 2;  else ndims = 3; end
value_fcn = value_fcn_all{goal_select, ndims-1};


particle_set = particles;

if start 
    action = computeNextAction(particle_set, value_fcn, toggleBtn, amclSvc, plot);
else
    action = get_teleop(message, holonomic);
end

if isnan(action)
    warning('Cannot send NaN velocity, changing to zero');
    action = [0, 0];
end

send_velocity(holonomic, vel, action);
    
end

function [action] = get_teleop(input, holonomic)
maxX = 0.3;
maxY = 0.3;
if holonomic, y_ax=1; else, y_ax=4; end

% Extract joystick value from the ROS message
joy.X = double(input.Axes(2));
joy.Y = double(input.Axes(y_ax));
joy.mag = sqrt(joy.X^2 + joy.Y^2);
if joy.mag ~= 0
    joy.normX = joy.X / joy.mag; %Normalizing joy input to unit circle 
    joy.normY = joy.Y / joy.mag;
else
    joy.normX = joy.X;
    joy.normY = joy.Y;
end

if holonomic
    action(1) = joy.normX;
    action(2) = joy.normY;
else 
    action(1) = joy.normX*maxX;
    action(2) = joy.normY*maxY;
end
end

function send_velocity(holonomic, vel, action)
    v1 = action(1);
    v2 = action(2);
    if holonomic
        vel.holonomic.msg.Linear.X = v1;
        vel.holonomic.msg.Linear.Y = v2;
        
        send(vel.holonomic.pub,vel.holonomic.msg);
    else 
        vel.diff.teleop.msg.Linear.X = v1;
        vel.diff.teleop.msg.Angular.Z = v2;
        
        send(vel.diff.teleop.pub, vel.diff.teleop.msg);
    end

end
