function particleSegbotCallbackFcn( ~, particleCloud, plots )
%particleCallbackFcn retreive particles from msg and plot on map 
%   -message: particle cloud msg
%   -plots: a vector of plot handles

    % Global variables 
    global particles
  
    n = length(particleCloud.Poses);
    particles = zeros(n, 3);
    
    % Retreive particles positions store as global variables
    for p = 1:n
        particles(p,1) = particleCloud.Poses(p).Position.X;
        particles(p,2) = particleCloud.Poses(p).Position.Y;
        particleQuat = [particleCloud.Poses(p).Orientation.W, ...
                        particleCloud.Poses(p).Orientation.X, ...
                        particleCloud.Poses(p).Orientation.Y, ...
                        particleCloud.Poses(p).Orientation.Z];
        particleRotation = quat2eul(particleQuat);
        particles(p,3) = normalize_angle(particleRotation(1));
    end
    
%% Code for plotting particle cloud on map
if nargin > 2 && ~isempty(plots)
     set(plots(1), 'xdata', particles(:,1), 'ydata', particles(:,2));
end


end

function na =  normalize_angle(angle)
    na = mod(angle, 2*pi);
end
