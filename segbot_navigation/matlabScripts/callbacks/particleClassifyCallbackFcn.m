function particleClassifyCallbackFcn( ~, particleCloud, holonomic, plots )
%particleCallbackFcn retreive particles from msg and plot on map 
%   -message: particle cloud msg
%   -plots: a vector of plot handles

    % Global variables

    global particles;
    global particletime;
    
    particletime = particleCloud.Header.Stamp;
    
    n = length(particleCloud.Poses);
    
    if holonomic
        particles = zeros(n, 2);
    else
        particles = zeros(n, 3);
    end
    
    % Retreive particles positions store as global variables
    for p = 1:n
        position = particleCloud.Poses(p).Position;
        orientation = particleCloud.Poses(p).Orientation;
        particles(p,1) = position.X;
        particles(p,2) = position.Y;
        if ~holonomic
            particleQuat = [orientation.W, ...
                            0, ...
                            0, ...
                            orientation.Z];
            particleRotation = quat2eul(particleQuat);
            particles(p,3) = normalize_angle(particleRotation(1));
        end
    end
   
    
%% Code for plotting particle cloud on map
if nargin > 3 && ~isempty(plots)
     set(plots(1), 'xdata', particles(:,1), 'ydata', particles(:,2));
end
end
function na =  normalize_angle(angle)
    na = mod(angle, 2*pi);
end


