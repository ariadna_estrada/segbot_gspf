function joyTeleopNodeCallback( ~, message, twist, holonomic)
%joyTeleopNodeCallback
% Function retreive joystick message, normalize to unity, and publish it to
% cmd_vel topic.
%Inputs:
%   -message: joystick message 
%   -twist: 2x1 struct containing /cmd_vel publisher and message 
%   -holonomic: boolean. Indicates if robot has holonomic dynamics. 

[joyVel] = get_teleop(message);
send_velocity(twist, joyVel, holonomic);
    
end

function [joyVel] = get_teleop(input)
maxX=0.5;
maxY=0.5;

% Extract joystick value from the ROS message
joy.X = double(input.Axes(2));
joy.Y = double(input.Axes(1));
joy.Z = double(input.Axes(1));
% joy.mag = sqrt(joy.X^2 + joy.Y^2);
% 
% if joy.mag > 1
%     %Normalize joy input to unit circle 
%     joy.X = joy.X / joy.mag; 
%     joy.Y = joy.Y / joy.mag;
% end
% 

joyVel = [joy.X*maxX, joy.Y*maxY, joy.Z];

end

function send_velocity(twist, action, holonomic)
twist.Msg.Linear.X = action(1);

if holonomic
    twist.Msg.Linear.Y = action(2);
    twist.Msg.Angular.Z = action(3);
else
    twist.Msg.Angular.Z = action(2);
end

send(twist.Pub,twist.Msg);
end
