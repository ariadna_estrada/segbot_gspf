function joyNonHolonomicNodeClassify( ~, message, userData)
%JoyNonHolonomicClassifyCallbackFcn 
% Function retreive joystick message, normalize to unity, and evaluate
% whether the user's input is productive or not. No intervention will
% occur, the user's input will be sent to the robot without modification.
%Inputs:
%   -message: joystick message 
%   -userData: struct with fields vf, viz_plot, particleSub


global classHistory
global classType
global particles
global slider_value
global goal_select
global estimatedPose
global pointCount 

[joyVel, joyInput] = get_teleop(message);


%proceed with classification only if joystick input is different to 0 
if (joyInput(1) ~= 0 || joyInput(2) ~= 0)
    duration = message.Header.Stamp - userData.particleSub.LatestMessage.Header.Stamp;
    value_fcn = userData.vf{goal_select, 2};
        switch(classType)
            case 'CV'

            case 'Count'

            case 'Multiple'

            case 'VFSampling'
                [ps, vf_samples] = valueFunctionSamples3D(particles, value_fcn.g, value_fcn.mttr, value_fcn.XS, value_fcn.VS, 1);

                %Translate each particle by the user's input 
                translated_ps = sample_motion_nonholonomic(joyVel, ps);

                %Sample value function at the new particle location
                [~, new_vf_samples] = valueFunctionSamples3D(translated_ps, value_fcn.g, value_fcn.mttr, value_fcn.XS, value_fcn.VS, 0);

                %Evaluate what fraction of the particles is going downhill
                going_downhill = compare_vf_samples(vf_samples, new_vf_samples);
                
                %Plot update text on figure
                per_string = strcat(num2str( ceil( going_downhill*100 )), '%');
                set(userData.text, 'String', per_string );
                if going_downhill >= slider_value
                    classHistory = [classHistory;estimatedPose,joyVel, 0,1, going_downhill];
%                     save('good_near', 'vf_samples', 'new_vf_samples', 'particles', 'joyVel', 'estimatedPose');
                else 
                    classHistory = [classHistory;estimatedPose,joyVel, 0,3, going_downhill];
                    save('obstacle4', 'vf_samples', 'new_vf_samples', 'particles', 'joyVel', 'estimatedPose');
                end
                
                show_classification(classHistory, userData.plot);
                
            otherwise
                
                disp('Unknown classification method');          
        end
        
    send_duration(duration, userData.durationStamp);
end
    
end

function [joyVel, joyInput] = get_teleop(input)
maxX=0.5;
maxY=0.4;
% Extract joystick value from the ROS message
joy.X = double(input.Axes(2));
joy.Y = double(input.Axes(1));
joy.mag = sqrt(joy.X^2 + joy.Y^2);

if joy.mag > 1
    %Normalize joy input to unit circle 
    joy.X = joy.X / joy.mag; 
    joy.Y = joy.Y / joy.mag;
end


joyInput = [sign(joy.X), sign(joy.Y)];
joyVel = [joy.X*maxX, joy.Y*maxY];

end
function show_classification(classHistory, plots)
global classType
  if ~isempty(classHistory)
      switch(classType)
          case('CV')
         
          case('Count')
            ind_high = find(classHistory(:,5)==1);
            ind_med = find(classHistory(:,5) ==2);
            ind_low = find(classHistory(:,5) ==3);

            set(plots(5), 'xdata', classHistory(ind_high,1), 'ydata', classHistory(ind_high,2));
            set(plots(6), 'xdata', classHistory(ind_med,1), 'ydata', classHistory(ind_med,2));
            set(plots(7), 'xdata', classHistory(ind_low,1), 'ydata', classHistory(ind_low,2));
          case('Multiple')
              
          case 'VFSampling'
            ind_high = find(classHistory(:,7)==1);
            ind_med = find(classHistory(:,7) ==2);
            ind_low = find(classHistory(:,7) ==3);

            set(plots(5), 'xdata', classHistory(ind_high,1), 'ydata', classHistory(ind_high,2));
            set(plots(6), 'xdata', classHistory(ind_med,1), 'ydata', classHistory(ind_med,2));
            set(plots(7), 'xdata', classHistory(ind_low,1), 'ydata', classHistory(ind_low,2));    
          otherwise
              disp('Unknown classification method');
        end
  end
end
function send_duration(duration, stamp)
stamp.Msg.Data = duration;
send(stamp.Pub, stamp.Msg);
end
