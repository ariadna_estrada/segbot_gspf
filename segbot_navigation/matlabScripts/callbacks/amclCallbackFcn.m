function amclCallbackFcn( ~, amclpose, plots )
%amclCallbackFcn retreive and plot estimated position from amcl
%   message: amcl message
%   plots: a vector of plot handles

    global estimatedPose
    global pathHistory
    global showHistory
    global rand_goal
    
    persistent goal_achieved;

    % Compute robot's pose [x,y,yaw] from amcl message
    amclQuat = [amclpose.Pose.Pose.Orientation.W, ...
                amclpose.Pose.Pose.Orientation.X, ...
                amclpose.Pose.Pose.Orientation.Y, ...
                amclpose.Pose.Pose.Orientation.Z];
            
    amclRotation = quat2eul(amclQuat);
    amclOrientation = normalize_angle(amclRotation(1));
    
    estimatedPose = [amclpose.Pose.Pose.Position.X, ... 
                     amclpose.Pose.Pose.Position.Y, ...
                     amclOrientation];
                 
    if nargin > 2 && ~isempty(plots)
        set(plots(3), 'xdata', estimatedPose(1), 'ydata', estimatedPose(2));
    end
    
  
    pathHistory = [pathHistory;estimatedPose];
    if showHistory
        set(plots(4), 'Visible', 'on');
        set(plots(4), 'xdata', pathHistory(:,1), 'ydata', pathHistory(:,2));
    else
        set(plots(4), 'Visible', 'off');
    end
    
    
    if within_goal_region(estimatedPose(1:2), rand_goal)
        load splat.mat;
        sound(y);
        disp('goal_found');
    end

end
function na =  normalize_angle(angle)
    na = mod(angle, 2*pi);
end

function goal_reached = within_goal_region(estimatedPose, rand_goal)
    if ( abs( estimatedPose - rand_goal ) < 0.5 ) 
        goal_reached = true;
    else
        goal_reached = false; 
    end
end

