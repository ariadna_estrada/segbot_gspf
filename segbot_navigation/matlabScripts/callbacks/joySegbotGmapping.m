function joySegbotGmapping( ~, message, vel)
%joySegbotGmapping simple teleoperation code to be used with the segbot during
%mapping.
%Inputs:
%   -message: joystick message 
%   -vel: 2x1 struct containing /cmd_vel publisher and message 


maxLinear = 0.5;
maxAngular = 0.5;
linear_axes = 2;
angular_axes = 4; 

% Extract joystick value from the ROS message
joy.X = double(message.Axes(linear_axes));
joy.Y = double(message.Axes(angular_axes));
joy.mag = sqrt(joy.X^2 + joy.Y^2);

if joy.mag ~= 0
    joy.normX = joy.X / joy.mag; %Normalizing joy input to unit circle 
    joy.normY = joy.Y / joy.mag;
else
    joy.normX = joy.X;
    joy.normY = joy.Y;
end

vel.msg.Linear.X = joy.normX*maxLinear;
vel.msg.Angular.Z = joy.normY*maxAngular;


send(vel.pub,vel.msg);

end
