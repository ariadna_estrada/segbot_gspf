%% Example 1: from trial a13 (change to trial directory) point 19
close all;
figMap = figure('Name', 'Map');
load('lab_map.mat');
load('poses.mat'); estimated_pose = pathHistory;
% goal = [ 5.5,0.5];
show(map)
title(' ');
hold on;
n = length(estimated_pose);
plot(estimated_pose(1:n-2,1), estimated_pose(1:n-2,2), '-b', 'linewidth', 2);
plot(goal(1), goal(2), '-p', 'MarkerIndices', [1], 'MarkerFaceColor', 'red', 'MarkerEdgeColor', 'red', 'MarkerSize', 12)

%classPoint_19
load('classPoint_11.mat');
pose5 = estimatedPose;
plot(pose5(1), pose5(2), 'og', 'MarkerSize', 10, 'MarkerFaceColor', 'g');

theta = pose5(3);

% rot_ut = [cos(theta) -sin(theta); sin(theta) cos(theta)] * searchDir'; 
rot_ut = [cos(theta) -sin(theta); sin(theta) cos(theta)] * [0.5;0]; 

qg = quiver(pose5(1), pose5(2), rot_ut(1), rot_ut(2), 0, 'k', 'LineWidth', 2, ...
     'MaxHeadSize', 1);
 
%Classification plot
figSaddle = figure('Name', 'Saddle point classification');
set(figSaddle, 'Position', [1212 651 560 256]);
sp_plots = saddle_classification_plots(figSaddle, 0);
sp_heading(ps, gs, sp_plots);
sp_velocity_rev(ps, actionSamples, gs, sp_plots);
%% save all the images
save_all_images(figMap, 'cs_ex1', figSaddle, 'sp_classification_ex1')

%% Example 2: from trial a16 (change to trial directory) use point 63
close all;
figMap = figure('Name', 'Map');
load('lab_map.mat');
load('poses.mat'); estimated_pose = pathHistory;
% goal = [ 5.5,0.5];
show(map)
title(' ');
hold on;
n = length(estimated_pose);
plot(estimated_pose(1:n-2,1), estimated_pose(1:n-2,2), '-b', 'linewidth', 2);
plot(goal(1), goal(2), '-p', 'MarkerIndices', [1], 'MarkerFaceColor', 'red', 'MarkerEdgeColor', 'red', 'MarkerSize', 12)

%classPoint_19
load('classPoint_63.mat');
pose5 = estimatedPose;
plot(pose5(1), pose5(2), 'og', 'MarkerSize', 10, 'MarkerFaceColor', 'g');

theta = pose5(3);
rot_ut = [cos(theta) -sin(theta); sin(theta) cos(theta)] * searchDir'; 
% rot_ut = [cos(theta) -sin(theta); sin(theta) cos(theta)] * [0.5;0.5]; 

qg = quiver(pose5(1), pose5(2), rot_ut(1), rot_ut(2), 0, 'k', 'LineWidth', 2, ...
     'MaxHeadSize', 1);
 
%Classification plot
figSaddle = figure('Name', 'Saddle point classification');
set(figSaddle, 'Position', [1212 651 560 256]);
sp_plots = saddle_classification_plots(figSaddle, 0);
sp_heading(ps, gs, sp_plots);
sp_velocity_rev(ps, actionSamples, gs, sp_plots);

%% plot in a circle
% close all
n = length(ps);
figure
hold on;
plot(0,0,'*');
th = linspace(0,2*pi,100);
xy = [cos(th)', sin(th)'];
plot(xy(:,1), xy(:,2), '-', 'Linewidth', 0.1, 'color', [0.83, 0.82, 0.78]);
axis([-2 2 -2 2])
axis('equal'); 
% plot(cos(ps(:,3)), sin(ps(:,3)), '.b', 'MarkerSize', 10);
ind_neg = find(actionSamples(:,2) == -1); nneg = length(ind_neg);
ind_pos = find(actionSamples(:,2) == 1);  npos = length(ind_pos);
ind_zro = find(actionSamples(:,2) == 0);  nzer = length(ind_zro);


%first quadrant
% qh_pos = quiver(cos(ps(ind_pos,3)), sin(ps(ind_pos,3)), -0.1*ones(npos,1),0.1*actionSamples(ind_pos,2),0, 'Marker', '.', 'MarkerSize', 10, 'Color', 'r');
% qh_neg = quiver(cos(ps(ind_neg,3)), sin(ps(ind_neg,3)), 0.1*ones(nneg,1),0.1*actionSamples(ind_neg,2),0, 'Marker', '.', 'MarkerSize', 10, 'Color', 'b');

%second quadrant
% qh_pos = quiver(cos(ps(ind_pos,3)), sin(ps(ind_pos,3)), 0.1*ones(npos,1),-0.1*actionSamples(ind_pos,2),0, 'Marker', '.', 'MarkerSize', 10, 'Color', 'r');

% qh_neg = quiver(cos(ps(ind_neg,3)), sin(ps(ind_neg,3)), 0.1*ones(nneg,1),-0.1*actionSamples(ind_neg,2),0, 'Marker', '.', 'MarkerSize', 10, 'Color', 'b');

% top of circle
% qh_neg = quiver(cos(ps(ind_neg,3)), sin(ps(ind_neg,3)), -0.1*actionSamples(ind_neg,2), 0.1*ones(nneg,1),0, 'Marker', '.', 'MarkerSize', 10, 'Color', 'b');
% qh_pos = quiver(cos(ps(ind_pos,3)), sin(ps(ind_pos,3)), -0.1*actionSamples(ind_pos,2), -0.1*ones(npos,1),0, 'Marker', '.', 'MarkerSize', 10, 'Color', 'r');

%bottom of circle
qh_pos = quiver(cos(ps(ind_pos,3)), sin(ps(ind_pos,3)), 0.1*actionSamples(ind_pos,2), 0.1*ones(npos,1), 0, 'Marker', '.', 'MarkerSize', 10, 'Color', 'r');
qh_neg = quiver(cos(ps(ind_neg,3)), sin(ps(ind_neg,3)), 0.1*actionSamples(ind_neg,2), 0.1*ones(nneg,1),0, 'Marker', '.', 'MarkerSize', 10, 'Color', 'b');


plot(cos(ps(ind_zro,3)), sin(ps(ind_zro,3)), 'g.', 'markersize', 10);
%% save all the images
save_all_images(figMap, 'cs_ex2', figSaddle, 'sp_classification_ex2')

%% Example 3: from trial6 (change to trial6 directory)
clear;clc;
close all;
figure
load('lab_map.mat');
load('poses.mat'); 
show(map)
title(' ');
hold on;
n = length(estimated_pose);
plot(estimated_pose(1:n-2,1), estimated_pose(1:n-2,2), '-b', 'linewidth', 2);
plot(2,5 , '-p', 'MarkerIndices', [1], 'MarkerFaceColor', 'red', 'MarkerEdgeColor', 'red', 'MarkerSize', 12)

%classPoint_4
load('classPoint_4.mat');
pose4 = estimatedPose;
plot(pose4(1), pose4(2), 'og', 'MarkerSize', 10, 'MarkerFaceColor', 'g');

theta = pose4(3);
rot_ut = [cos(theta) -sin(theta); sin(theta) cos(theta)] * searchDir'; 

qg = quiver(pose4(1), pose4(2), rot_ut(1), rot_ut(2), 0, 'k', 'LineWidth', 2, ...
     'MaxHeadSize', 1);
 
%classPoint_10
load('classPoint_10.mat');
pose4 = estimatedPose;
plot(pose4(1), pose4(2), 'og', 'MarkerSize', 10, 'MarkerFaceColor', 'g');

theta = pose4(3);
rot_ut = [cos(theta) -sin(theta); sin(theta) cos(theta)] * searchDir'; 

qg = quiver(pose4(1), pose4(2), rot_ut(1), rot_ut(2), 0, 'k', 'LineWidth', 2, ...
     'MaxHeadSize', 1);

 
%Classification plot
figSaddle = figure('Name', 'Saddle point classification');
sp_plots = saddle_classification_plots(figSaddle, 0);
[wIsMinimum, omega, xc_t] = sp_heading(ps, gs, sp_plots);
[vIsMinimum, v, xc_v] = sp_velocity_rev(ps, actionSamples, sp_plots);

