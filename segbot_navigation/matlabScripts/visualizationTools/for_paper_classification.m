
% 
% 
% load('noturn.mat');
% ydiff3 = new_vf_samples - vf_samples;
% 
% load('direct.mat');
% ydiff4 = new_vf_samples - vf_samples;


%% Overall
load('classHistory');
close all;
figure
figure(1)
% subplot(1,2,1);
show(map);
hold on;
title(' ');
plot(2, 5, '-p', 'MarkerIndices', [1], 'MarkerFaceColor', 'b', 'MarkerEdgeColor', 'b', 'MarkerSize', 10)



ind_high = find(classHistory(:,8)>= 0.7);
ind_low = find(classHistory(:,8) < 0.7);


% plot( classHistory(ind_high,1), classHistory(ind_high,2), '.g', 'MarkerSize', 10);
% plot( classHistory(ind_low,1),  classHistory(ind_low,2), '.r', 'MarkerSize', 10);  


ut = classHistory(:,4:5);
positions = classHistory(:,1:2);
theta = classHistory(:,3);
n = length(classHistory);
rot_ut = zeros(n,2);
for i = 1:n
    rot_ut(i,:) = [cos(theta(i)) -sin(theta(i)); sin(theta(i)) cos(theta(i))] * ut(i,:)'; 
end
sub_low = ind_low(2:2:end);
sub_high = ind_high(2:4:end);
% qg = quiver(positions(ind_high,1), positions(ind_high,2), rot_ut(ind_high,1), rot_ut(ind_high,2), 0, 'g', 'LineWidth', 1, ...
%      'MaxHeadSize', 0.1);
% qr = quiver(positions(ind_low,1), positions(ind_low,2), rot_ut(ind_low,1), rot_ut(ind_low,2), 0, 'r', 'LineWidth', 1, ... 
%      'MaxHeadSize', 0.1);

qg = quiver(positions(sub_high,1), positions(sub_high,2), rot_ut(sub_high,1), rot_ut(sub_high,2), 0, 'g', 'LineWidth', 1, ...
     'MaxHeadSize', 0.2);
qr = quiver(positions(sub_low,1), positions(sub_low,2), rot_ut(sub_low,1), rot_ut(sub_low,2), 0, 'r', 'LineWidth', 1, ... 
     'MaxHeadSize', 0.1);
 %% Markers
load('away.mat');
theta = estimatedPose(3);
plot(estimatedPose(1), estimatedPose(2), 'ro', 'MarkerFaceColor', 'r' , 'MarkerEdgeColor', 'k', 'MarkerSize', 10)
ydiff1 = new_vf_samples - vf_samples;

load('good_far.mat');
theta = estimatedPose(3);
plot(estimatedPose(1), estimatedPose(2), 'ro', 'MarkerFaceColor', 'g', 'MarkerEdgeColor', 'k', 'MarkerSize', 10)
ydiff2 = new_vf_samples - vf_samples;


load('obstacle4.mat');
theta = estimatedPose(3);
plot(estimatedPose(1), estimatedPose(2), 'ro', 'MarkerFaceColor', 'r', 'MarkerEdgeColor', 'k', 'MarkerSize', 10)
ydiff3 = new_vf_samples - vf_samples;
% 
load('obstacle.mat');
theta = estimatedPose(3);
plot(estimatedPose(1), estimatedPose(2), 'ro', 'MarkerFaceColor', 'r', 'MarkerEdgeColor', 'k', 'MarkerSize', 10)
ydiff4 = new_vf_samples - vf_samples;

% load('obstacle3.mat');
% theta = estimatedPose(3);
% plot(estimatedPose(1), estimatedPose(2), 'ro', 'MarkerFaceColor', 'r', 'MarkerEdgeColor', 'k', 'MarkerSize', 8)
% ydiff3 = new_vf_samples - vf_samples;

load('door2.mat');
theta = estimatedPose(3);
plot(estimatedPose(1), estimatedPose(2), 'ro', 'MarkerFaceColor', 'r', 'MarkerEdgeColor', 'k', 'MarkerSize', 10)
ydiff5 = new_vf_samples - vf_samples;

load('good_near.mat');
theta = estimatedPose(3);
plot(estimatedPose(1), estimatedPose(2), 'ro', 'MarkerFaceColor', 'g', 'MarkerEdgeColor', 'k', 'MarkerSize', 10)
ydiff6 = new_vf_samples - vf_samples;

%% Second plot
xs1=ones(length(ydiff1),1);
xs2=ones(length(ydiff2),1);
xs3=ones(length(ydiff3),1);
xs4=ones(length(ydiff4),1);
xs5=ones(length(ydiff5),1);
xs6=ones(length(ydiff6),1);

figure
% subplot(1,2,2);
hold on;
xlabel('Section');
ylabel('\Delta \psi(x_t)');
grid on;
h = plot(xs1, ydiff1, 'o');
c1 = get(h, 'Color');

h = plot(2*xs2, ydiff2, 'o');
c2 = get(h, 'Color');

h = plot(3*xs3, ydiff3, 'o');
c3 = get(h, 'Color');

h = plot(4*xs4, ydiff4, 'o');
c4 = get(h, 'Color');

h = plot(5*xs5, ydiff5, 'o');
c5 = get(h, 'Color');

h = plot(6*xs6, ydiff6, 'o');
c6 = get(h, 'Color');

xticks([1 2 3 4 5 6 ])
axis([0.5 6.5 -0.5 1.5])
