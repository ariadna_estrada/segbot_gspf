function varargout = mapGUInavigation(varargin)
% MAPGUINAVIGATION MATLAB code for mapGUInavigation.fig
%      MAPGUINAVIGATION, by itself, creates a new MAPGUINAVIGATION or raises the existing
%      singleton*.
%
%      H = MAPGUINAVIGATION returns the handle to a new MAPGUINAVIGATION or the handle to
%      the existing singleton*.
%
%      MAPGUINAVIGATION('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MAPGUINAVIGATION.M with the given input arguments.
%
%      MAPGUINAVIGATION('Property','Value',...) creates a new MAPGUINAVIGATION or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before mapGUInavigation_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to mapGUInavigation_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help mapGUInavigation

% Last Modified by GUIDE v2.5 29-Nov-2017 22:19:44

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @mapGUInavigation_OpeningFcn, ...
                   'gui_OutputFcn',  @mapGUInavigation_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before mapGUInavigation is made visible.
function mapGUInavigation_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to mapGUInavigation (see VARARGIN)
% Create the data to plot.
handles.model = varargin{1};
handles.value_fcn = varargin{2};

ax = gca;
map = handles.model.map;
show(map);
title('Map');
hold(ax, 'on');
grid on;
plots(1) = plot(ax, [0], [0], 'b.'); %particles
plots(2) = plot(ax, [0], [0], 'rx', 'linewidth', 3); %true position
plots(3) = plot(ax, [0], [0], 'yo',  'linewidth', 3); %estimated position
plots(4) = plot(ax, [-15], [-5], '-m', 'linewidth', 2); %path history

goals = handles.model.goals;
[n,~] = size(goals);
if n > 1
    for i = 1:n
        text(goals(i,1), goals(i,2), num2str(i), 'Color', 'red') ;
    end
end 

% Choose default command line output for mapGUInavigation
handles.output = hObject;
handles.plots = plots;
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes mapGUInavigation wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = mapGUInavigation_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in initialize_pb.
function initialize_pb_Callback(hObject, eventdata, handles)
% hObject    handle to initialize_pb (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
rosshutdown;
try 
    rosinit('localhost');
catch 
    disp('Unable to connect to ROS master');
    return;
end
viz_plot = handles.plots;
goals_vf = handles.value_fcn;
startBtn = handles.startBtn;
% figSaddle = figure('Name', 'Saddle point classification');
% sp_plots = saddle_classification_plots(figSaddle, handles.model.holonomic);
sp_plots = [];
% Create ROS publisher for sending out velocity commands
[vel.pub, vel.msg] =  rospublisher('/cmd_vel','geometry_msgs/Twist');

%Create AMCL service for non-motion updates
amclSvc.client = rossvcclient('/request_nomotion_update');
amclSvc.req = rosmessage(amclSvc.client);

% Subscribe to ROS topics
try
    handles.particleSub = rossubscriber('particlecloud', {@particleSegbotCallbackFcn, viz_plot});
    handles.amclSub = rossubscriber('amcl_pose', {@amclCallbackFcn, viz_plot});
    handles.joySub = rossubscriber('joy', {@JoySegbotCallbackFcn, vel, goals_vf, startBtn, amclSvc, sp_plots});
catch me
    disp('The ROS master is not reachable. Launch ROS simulation in terminal first.'); 
    return;
end

guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function axes1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate axes1
handles.axes = hObject;
guidata(hObject,handles);


% --- Executes on button press in clear.
function clear_Callback(hObject, eventdata, handles)
% hObject    handle to clear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global pathHistory;
pathHistory = [];
plots = handles.plots;
set(plots(4), 'xdata', -15, 'ydata', -5);


% --- Executes on button press in goal1.
function goal1_Callback(hObject, eventdata, handles)
% hObject    handle to goal1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of goal1
global goal_select
goal_select = 1;

% --- Executes on button press in goal2.
function goal2_Callback(hObject, eventdata, handles)
% hObject    handle to goal2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of goal2
global goal_select
goal_select = 2;

% --- Executes on button press in goal3.
function goal3_Callback(hObject, eventdata, handles)
% hObject    handle to goal3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of goal3
global goal_select
goal_select = 3;

% --- Executes on button press in goal4.
function goal4_Callback(hObject, eventdata, handles)
% hObject    handle to goal4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of goal4
global goal_select
goal_select = 4;

% --- Executes on button press in goal5.
function goal5_Callback(hObject, eventdata, handles)
% hObject    handle to goal5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of goal5
global goal_select
goal_select = 5;

% --- Executes on button press in goal6.
function goal6_Callback(hObject, eventdata, handles)
% hObject    handle to goal6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of goal6
global goal_select
goal_select = 6;

% --- Executes on button press in save.
function save_Callback(hObject, eventdata, handles)
% hObject    handle to save (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% save('trial_data.mat', 'classHistory');
F = getframe(handles.axes);
Image = frame2im(F);
[fileName, filePath]=uiputfile('*.jpg', 'Save as');
if fileName ~= 0
    imwrite(Image, strcat(filePath,fileName));
end


% --- Executes during object creation, after setting all properties.
function goal1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to goal2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes during object creation, after setting all properties.
function goal2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to goal2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on button press in show_hide.
function show_hide_Callback(hObject, eventdata, handles)
% hObject    handle to show_hide (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global pathHistory
global showHistory
plots = handles.plots;
% Hint: get(hObject,'Value') returns toggle state of show_hide
showHistory = get(hObject, 'Value');
if showHistory
    set(hObject, 'String', 'Hide path');
    set(plots(4), 'Visible', 'on');
    if ~isempty(pathHistory)
        set(plots(4), 'xdata', pathHistory(:,1), 'ydata', pathHistory(:,2));
    end
else
    set(hObject, 'String', 'Show path');
    set(plots(4), 'Visible', 'off');
end


% --- Executes on button press in start_stop.
function start_stop_Callback(hObject, eventdata, handles)
% hObject    handle to start_stop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of start_stop
global start
start = get(hObject,'Value');
if start
    set(hObject, 'String', 'Stop');
else
    set(hObject, 'String', 'Start');
end



% --- Executes during object creation, after setting all properties.
function start_stop_CreateFcn(hObject, eventdata, handles)
% hObject    handle to start_stop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
handles.startBtn = hObject;
guidata(hObject, handles);


% --------------------------------------------------------------------
function Untitled_1_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
