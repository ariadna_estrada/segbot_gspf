function varargout = goalFinderGUI(varargin)
% GOALFINDERGUI MATLAB code for goalFinderGUI.fig
%      GOALFINDERGUI, by itself, creates a new GOALFINDERGUI or raises the existing
%      singleton*.
%
%      H = GOALFINDERGUI returns the handle to a new GOALFINDERGUI or the handle to
%      the existing singleton*.
%
%      GOALFINDERGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GOALFINDERGUI.M with the given input arguments.
%
%      GOALFINDERGUI('Property','Value',...) creates a new GOALFINDERGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before goalFinderGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to goalFinderGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help goalFinderGUI

% Last Modified by GUIDE v2.5 08-Mar-2018 13:08:06

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @goalFinderGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @goalFinderGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before goalFinderGUI is made visible.
function goalFinderGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to goalFinderGUI (see VARARGIN)
% Create the data to plot.

handles.model = varargin{1};

ax = gca;
% Set the current data value.
% handles.current_data = handles.peaks;
% surf(handles.current_data)
map = handles.model.map;
show(map);
title('Two rooms map');
hold(ax, 'on');
grid on;
plots(1) = plot(ax, [0], [0], 'b.'); %particles
plots(2) = plot(ax, [-5], [-4], 'rx', 'linewidth', 3); %true position
plots(3) = plot(ax, [0], [0], 'yo',  'linewidth', 3); %estimated position
plots(4) = plot(ax, [-5], [-4], '-m', 'linewidth', 2); %path history
plots(5) = plot(ax, [-5], [-4], '.g'); %Classified productive n>75%
plots(6) = plot(ax, [-5], [-4], '.y'); %Classified med-productive  50% < n < 75%
plots(7) = plot(ax, [-5], [-4], '.r'); %Classified productive n<50%

goals = handles.model.goals;
[n,~] = size(goals);


% Choose default command line output for goalFinderGUI
handles.output = hObject;
handles.plots = plots;
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes goalFinderGUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = goalFinderGUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in initialize_pb.
function initialize_pb_Callback(hObject, eventdata, handles)
% hObject    handle to initialize_pb (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
rosshutdown;
try 
    rosinit('localhost');
catch 
    warning('Unable to connect to ROS master');
    return;
end
holonomic = handles.model.holonomic;
viz_plot = handles.plots;


try
    %teleop node
    handles.teleop_node = robotics.ros.Node('teleop_node', 'localhost');
    handles.twist.Pub = robotics.ros.Publisher(handles.teleop_node,'/cmd_vel','geometry_msgs/Twist');
    handles.twist.Msg = rosmessage(handles.twist.Pub);
    handles.joySubTeleop = robotics.ros.Subscriber(handles.teleop_node, 'joy', {@joyTeleopNodeCallback, handles.twist, holonomic});

catch 
    disp('Colud not create ROS Node');
    return;
end

try
%     handles.particleSub = rossubscriber('particlecloud', {@particleSegbotCallbackFcn, viz_plot});
    handles.amclSub = rossubscriber('amcl_pose', {@amclCallbackFcn, viz_plot});
catch me
    disp('The ROS master is not reachable. Launch ROS simulation in terminal first.'); 
    return;
end
guidata(hObject, handles);


% --- Executes on slider movement.
function slider1_Callback(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
global slider_value
text_slider = handles.text_slider;
slider_value = get(hObject, 'Value');
set(text_slider, 'String', strcat(num2str(ceil(slider_value*100)),'%'));



% --- Executes during object creation, after setting all properties.
function slider1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
global slider_value
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
handles.slider = hObject;
set(hObject, 'Value', 0.75);
handles.slider_value = get(hObject, 'Value');
slider_value = handles.slider_value;
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function axes1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate axes1
handles.axes = hObject;
guidata(hObject,handles);



% --- Executes on button press in clear.
function clear_Callback(hObject, eventdata, handles)
% hObject    handle to clear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global classHistory;
classHistory = [];
plots = handles.plots;
set(plots(5), 'xdata', -5, 'ydata', -4);
set(plots(6), 'xdata', -5, 'ydata', -4);
set(plots(7), 'xdata', -5, 'ydata', -4);


% --- Executes on button press in goal1.
function goal1_Callback(hObject, eventdata, handles)
% hObject    handle to goal1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of goal1
global goal_select
goal_select = 1;

% --- Executes on button press in goal2.
function goal2_Callback(hObject, eventdata, handles)
% hObject    handle to goal2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of goal2
global goal_select
goal_select = 2;

% --- Executes on button press in goal3.
function goal3_Callback(hObject, eventdata, handles)
% hObject    handle to goal3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of goal3
global goal_select
goal_select = 3;

% --- Executes on button press in goal4.
function goal4_Callback(hObject, eventdata, handles)
% hObject    handle to goal4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of goal4
global goal_select
goal_select = 4;

% --- Executes on button press in goal5.
function goal5_Callback(hObject, eventdata, handles)
% hObject    handle to goal5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of goal5
global goal_select
goal_select = 5;

% --- Executes on button press in goal6.
function goal6_Callback(hObject, eventdata, handles)
% hObject    handle to goal6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of goal6
global goal_select
goal_select = 6;


% --- Executes on button press in classType1.
function classType1_Callback(hObject, eventdata, handles)
% hObject    handle to classType1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of classType1
global classType
classType = 'CV';
set(handles.slider, 'Enable', 'off');
set(handles.label_count, 'Visible', 'off');
set(handles.text_count, 'Visible', 'off');
guidata(hObject, handles);


% --- Executes on button press in classType2.
function classType2_Callback(hObject, eventdata, handles)
% hObject    handle to classType2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of classType2
global classType
classType = 'Count';
set(handles.slider, 'Enable', 'on');
set(handles.label_count, 'Visible', 'on');
set(handles.text_count, 'Visible', 'on');
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function text_slider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to text_slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
handles.text_slider = hObject;
set(hObject, 'String', '75%');
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function classType2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to classType2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
global classType
if get(hObject, 'Value')
    classType = 'Count';
    set(handles.slider, 'Enable', 'on');
    set(handles.label_count, 'Visible', 'on');
    set(handles.text_count, 'Visible', 'on');
end


% --- Executes during object creation, after setting all properties.
function text_count_CreateFcn(hObject, eventdata, handles)
% hObject    handle to text_count (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
handles.text_count = hObject;
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function label_count_CreateFcn(hObject, eventdata, handles)
% hObject    handle to label_count (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
handles.label_count = hObject;
guidata(hObject, handles);


% --- Executes on button press in save.
function save_Callback(hObject, eventdata, handles)
% hObject    handle to save (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global classHistory
% save('trial_data.mat', 'classHistory');
uisave('classHistory');


% --- Executes during object creation, after setting all properties.
function classType1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to classType1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
global classType
if get(hObject, 'Value')
    classType = 'CV';
end

% --- Executes during object creation, after setting all properties.
function goal1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to goal2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes during object creation, after setting all properties.
function goal2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to goal2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on button press in classType3.
function classType3_Callback(hObject, eventdata, handles)
% hObject    handle to classType3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of classType3
global classType
classType = 'Multiple';
set(handles.slider, 'Enable', 'off');
set(handles.label_count, 'Visible', 'off');
set(handles.text_count, 'Visible', 'off');
guidata(hObject, handles);


% --- Executes on button press in save_image.
function save_image_Callback(hObject, eventdata, handles)
% hObject    handle to save_image (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
F = getframe(handles.axes);
Image = frame2im(F);
[fileName, filePath]=uiputfile('*.jpg', 'Save as');
if fileName ~= 0
    imwrite(Image, strcat(filePath,fileName));
end


% --- Executes on button press in vf_sampling.
function vf_sampling_Callback(hObject, eventdata, handles)
% hObject    handle to vf_sampling (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of vf_sampling
global classType
classType = 'VFSampling';


% --- Executes on button press in new_goal_btn.
function new_goal_btn_Callback(hObject, eventdata, handles)
% hObject    handle to new_goal_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global rand_goal 

n = length(handles.model.goals);

rand_goal = handles.model.goals(randi(n),:)



% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over new_goal_btn.
function new_goal_btn_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to new_goal_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
