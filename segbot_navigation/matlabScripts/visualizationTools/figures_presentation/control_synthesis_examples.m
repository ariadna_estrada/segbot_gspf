%Load map and value function
load('lab_map_smooth_3d_4_9.5.mat');
load('lab_map.mat');
load('trial_data/control_synthesis/a10/classPoint_6');
g3.xs{1,1} = g.xs{1,1}(:,:,1);
g3.xs{2,1} = g.xs{2,1}(:,:,1);

%% set interest locations
% set location of the goal and two other interesting locations where saddle
% points are expected (Near ridges or minimums). 

% goal = [-2, 0];
% p1 = [1.5, 0, pi]; %On top of saddle
% zoom_region = [0 3 -1.5 1.5];
% p1 = [-2.33, -1.22, pi/2]; %Near goal
% zoom_region = [-3.5 0 -2.5 1];

% p1 = [8.3593, 4.0981, 1.8242]; %When we have consensus
% zoom_region = [-3 3 -3 3];
% p1 = [3.90,9.47,1.19];r
% zoom_region = [3 11 -1 10];
%% sample particle cloud
px = 0.1 .* randn(300,1) + p1(1);
py = 0.1 .* randn(300,1) + p1(2);
pz = 0.1 .* randn(300,1) + p1(3);
ps = [px, py, pz];

%% Show map, particle and goal
mapObj = plot_particles(map, ps, goal);
axis(zoom_region); axis square;
mapObj.AxesHandle.XTickMode = 'auto';
% title('Control synthesis example');
title('Finite wall');

figure
contour(g3.xs{:}, mttr(:,:,45), 100);
axis(zoom_region); axis square;
% colorbar('eastoutside', 'direction', 'reverse');
title('Slice of \psi(\theta = \pi)');
ylabel('y [m]'); xlabel('x [m]');
%% sample gradient at particle locations
[searchDir, gradientSamples] = bestActionForValueFunction3D(ps, g, action,[]);
% ps = particle_set;
% gradientSamples = gs;
vs =  sign(gradientSamples(:,1).*cos(ps(:,3)) + gradientSamples(:,2).*sin(ps(:,3)));
%% bin particle orientations

[pz_sorted, ind] = sort(ps(:,3));
bins = [ind(1:100), ind(101:200), ind(201:300)] ;

%Find representative slices of the value function
t1 = mean(pz_sorted(1:100));
t2 = mean(pz_sorted(101:200));
t3 = mean(pz_sorted(201:300));

s = g.vs{3,1};

[~, slices(1)] = min( abs(t1 - s) );
[~, slices(2)] = min( abs(t2 - s) );
[~, slices(3)] = min( abs(t3 - s) );

%% plot gradients for each bin
% close all
%define some colors to highlight the particles belonging to each bin
colors = [0, 0.45, 0.74; 0.47, 0.67, 0.19; 0.8, 0, 0.6];

nBins = length(slices);
nSamples = length(bins);

%Gradient samples on top of value function
for b = 1:nBins
    bin = bins(:,b);
    slice = slices(b);
    
    plot_particles(map, ps, goal, colors(b,:), bin);
    axis(zoom_region);
    title(''); 
    axis square;
    
    figure; hold on;
    contour(g3.xs{:}, mttr(:,:,slice), 100);
    axis(zoom_region);
    axis square; box on;
    figTitle = sprintf('Slice of value function when \\theta = %4.3f', g.vs{3}(slice));
    title(figTitle);
    xlabel('x [m]'); ylabel('y [m]');
    
    
    poses = ps(bin, :);
%     samples = gs(bin, 3);
    samples = vs(bin);
    
    for i=1:nSamples
        if samples(i) < 0
            mrk1 = 'x';
            mrk2 = 'o';
            clr = 'b';
            plot(poses(i,1), poses(i,2), 'Marker', mrk2, 'MarkerSize', 10, 'Color', clr, 'LineWidth', 2);
        elseif samples(i) > 0
            mrk1 = 'o';
            mrk2 = '.';
            clr = 'r';
            plot(poses(i,1), poses(i,2), 'Marker', mrk2, 'MarkerSize', 5, 'Color', clr, 'LineWidth', 2);
        else
            mrk1 = '.';
            clr = 'k';
        end     
        plot(poses(i,1), poses(i,2), 'Marker', mrk1, 'MarkerSize', 10, 'Color', clr, 'LineWidth', 2);
    end
end
%% Find direction
if searchDir == 0
    gs = gradientSamples;
    ps = particle_set;
    % Remove NaNs from gs and ps
    nanRows = any(isnan(gradientSamples),2);
    gs(nanRows,:) = [];
    ps(nanRows,:) = [];
    
        disp('Classifying saddle point');
    %first check angular velocity
    [wIsMinimum, omega, xc_t] = sp_heading(ps, gs, []);
    fprintf('omega: %4.4f\n', omega);
end
%% Classify stationary point
figure
plot(pz, gradientSamples(:,3), 'k.', 'MarkerSize', 8); hold on;
xlabel('\theta'); ylabel('\omega^{[m]}(t)');
plot(pz, -sign(gradientSamples(:,3)), 'r.', 'MarkerSize', 8);

x_omega = [ pz, -sign(gradientSamples(:,3)) ];
% Classification of stationary point, if the quadratic fit(y = ax^2+bx+c)
% has negative or zero a value, it is a local minimum, if it has a positive
% a value, then it is a local maximum. Note the procedure is opposite from
% the 2d Hessian classfication because for the 2d case, we use the gradient
% space and for 3d case, we use the action space.
a = polyfit(x_omega(:,1),x_omega(:,2),1);

xs = [min(pz), max(pz)];
ys = a(1) * xs + a(2);
plot(xs, ys, 'Color', [0, 0.45, 0.74], 'LineWidth', 2);

figure
plot(vs, -sign(vs), 'k.', 'MarkerSize', 8);
%% Draw arrows
%Click on figure first to make it current figure

radius = 0.75; % Height from top to bottom
centre = [9,1];
arrow_angle = 135; % Desired orientation angle in degrees
angle = 30; % Anglebetween start and end of arrow
direction = -1; % for CW enter 1, for CCW enter -1
colour = 'b'; % Colour of arrow

figHandle = gcf;
circular_arrow(figHandle, 0.75, centre, arrow_angle, 90, direction, colour);


%% store image as pdf
%click on the image you want to save and make sure you change the name and
%directory to be saved in.

fig = gcf;
set(fig,'Units','Inches');
pos = get(fig,'Position');
set(fig,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3)-1.5, pos(4)]);
print(fig, strcat('pdfs/', 'set-3-v'),'-dpdf','-r500')
