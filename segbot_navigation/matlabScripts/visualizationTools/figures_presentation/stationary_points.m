%Load map and value function
load('finite_wall_scenario.mat');
load('finite_wall_3d_90.mat');
g3.xs{1,1} = g.xs{1,1}(:,:,1);
g3.xs{2,1} = g.xs{2,1}(:,:,1);

%% set interest locations
% set location of the goal and two other interesting locations where saddle
% points are expected (Near ridges or minimums). 

goal = [-2, 0];
% p1 = [1.5, 0, pi];
% p1 = [-2.4, 0.77, 7*pi/4];
p1 = [-2.33, -1.22, pi/2];
% p1 = [-1.6, -2, 2.1];
zoom_region = [-3.5 0 -2.5 1];

%% sample particle cloud
px = 0.2 .* randn(300,1) + p1(1);
py = 0.2 .* randn(300,1) + p1(2);
pz = 0.1 .* randn(300,1) + p1(3);
particle_set = [px, py, pz];

%% Show map, particle and goal
mapObj = plot_particles(map, particle_set, goal);
axis(zoom_region);
title('Finite wall');
%% sample gradient at particle locations
[searchDir, gradientSamples] = bestActionForValueFunction3D(particle_set, g, action,[]);

%% bin particle orientations

[pz_sorted, ind] = sort(pz);
bins = [ind(1:100), ind(101:200), ind(201:300)] ;

%Find representative slices of the value function
t1 = mean(pz_sorted(1:100));
t2 = mean(pz_sorted(101:200));
t3 = mean(pz_sorted(201:300));

s = g.vs{3,1};

[~, slices(1)] = min( abs(t1 - s) );
[~, slices(2)] = min( abs(t2 - s) );
[~, slices(3)] = min( abs(t3 - s) );

%% plot gradients for each bin
close all
%define some colors to highlight the particles belonging to each bin
colors = [0, 0.45, 0.74; 0.47, 0.67, 0.19; 0.8, 0, 0.6];

nBins = length(slices);
nSamples = length(bins);

%Gradient samples on top of value function
for b = 1:nBins
    bin = bins(:,b);
    slice = slices(b);
    
    plot_particles(map, particle_set, goal, colors(b,:), bin);
    axis(zoom_region); title(''); axis square;
    
    figure; hold on;
    contour(g3.xs{:}, mttr(:,:,slice), 100);
    axis(zoom_region); axis square;
    figTitle = sprintf('Slice of value function when \\theta = %4.3f', g.vs{3}(slice));
    title(figTitle);
    xlabel('x [m]'); ylabel('y [m]');
    
    
    poses = particle_set(bin, :);
    gs = gradientSamples(bin, 3);
    
    for i=1:nSamples
        if gs(i) < 0
            mrk1 = 'x';
            mrk2 = 'o';
            clr = 'b';
            plot(poses(i,1), poses(i,2), 'Marker', mrk2, 'MarkerSize', 10, 'Color', clr, 'LineWidth', 2);
        else
            mrk1 = 'o';
            mrk2 = '.';
            clr = 'r';
            plot(poses(i,1), poses(i,2), 'Marker', mrk2, 'MarkerSize', 5, 'Color', clr, 'LineWidth', 2);
        end     
        plot(poses(i,1), poses(i,2), 'Marker', mrk1, 'MarkerSize', 10, 'Color', clr, 'LineWidth', 2);
    end
end
%% Find direction
if searchDir == 0
    gs = gradientSamples;
    ps = particle_set;
    % Remove NaNs from gs and ps
    nanRows = any(isnan(gradientSamples),2);
    gs(nanRows,:) = [];
    ps(nanRows,:) = [];
    
        disp('Classifying saddle point');
    %first check angular velocity
    [wIsMinimum, omega, xc_t] = sp_heading(ps, gs, []);
    fprintf('omega: %4.4f\n', omega);
end

%% store image as pdf
%click on the image you want to save and make sure you change the name and
%directory to be saved in.

fig = gcf;
set(fig,'Units','Inches');
pos = get(fig,'Position');
set(fig,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3)-1, pos(4)]);
print(fig, strcat('pdfs/', 'set-1-vf'),'-dpdf','-r0')
