%Load map and value function
load('finite_wall_scenario.mat');
load('finite_wall_3d_90.mat');
g3.xs{1,1} = g.xs{1,1}(:,:,1);
g3.xs{2,1} = g.xs{2,1}(:,:,1);

%% set interest locations
% set location of the goal and two other interesting locations where saddle
% points are expected (Near ridges or minimums). 

goal = [-2, 0];
p1 = [3, 0, pi]; %On top of saddle
zoom_region = [-3 3 -3 3];
% p1 = [-2.33, -1.22, pi/2]; %Near goal
% zoom_region = [-3.5 0 -2.5 1];

% p1 = [1.925, -1, pi]; %When we have consensus
% zoom_region = [-3 3 -3 3];

%% sample particle cloud
px = 0.2 .* randn(300,1) + p1(1);
py = 0.2 .* randn(300,1) + p1(2);
pz = 0.1 .* randn(300,1) + p1(3);
particle_set = [px, py, pz];

%% Show map, particle and goal
mapObj = plot_particles(map, particle_set, goal);
axis(zoom_region);
title('Finite wall');
%% plot countours
figure; hold on;
slice = 45;
[c,h]=contourf(g3.xs{:}, mttr(:,:,45), 45);
axis(zoom_region); box on;
axis square;

title('Slice of value function when \theta = \pi');
xlabel('x [m]'); ylabel('y [m]');
%% sample vf at particles' location
[ps, vf_samples] = valueFunctionSamples3D(particle_set, g, mttr);

%% Translate each particle by the user's input 
u = [1 1];
translated_ps = sample_motion_nonholonomic(u, ps, 1);

%% Sample value function at the new particle location
[~, new_vf_samples] = valueFunctionSamples3D(translated_ps, g, mttr);

going_downhill = compare_vf_samples(vf_samples, new_vf_samples);
                
%%
plot(ps(:,1), ps(:,2), '.');
plot(translated_ps(:,1), translated_ps(:,2), '.')


%% store image as pdf
%click on the image you want to save and make sure you change the name and
%directory to be saved in.

fig = gcf;
set(fig,'Units','Inches');
pos = get(fig,'Position');
set(fig,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3)-3, pos(4)]);
print(fig, strcat('pdfs/', 'vspf-ex2'),'-dpdf','-r500')
