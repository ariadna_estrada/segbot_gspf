
% 
% 
% load('noturn.mat');
% ydiff3 = new_vf_samples - vf_samples;
% 
% load('direct.mat');
% ydiff4 = new_vf_samples - vf_samples;


%% Overall
load('lab_map.mat');
load('classHistory');
close all;
figure
figure(1)
% subplot(1,2,1);
%%
figure
show(map);
hold on;
title(' ');
plot(2, 5, '-p', 'MarkerIndices', [1], 'MarkerFaceColor', 'b', 'MarkerEdgeColor', 'b', 'MarkerSize', 10);
xlabel('x [m]'); ylabel('y [m]');
zoom_region = [0 10 -6 6];
axis(zoom_region);
%% plot pose
p = 1;
pose = [positions(p,1), positions(p,2), theta(p)];
plot( pose(1),pose(2),'o','MarkerSize',10,'MarkerEdgeColor', 'k', 'MarkerFaceColor', 'c', 'LineWidth', 2);
% plot([pose(1), pose(1) + 0.75*cos(pose(3))], [pose(2), pose(2) + 0.75*sin(pose(3))], '*','MarkerSize',2,'Color','k','LineStyle','-');
v = 0.9;
qx = quiver(pose(1), pose(2), v*cos(pose(3)), v*sin(pose(3)), 'Color', 'k', 'LineWidth', 1, 'MaxHeadSize', 1);
%%
fig2 = gcf;
set(fig2,'Units','Inches');
pos = get(fig2,'Position');
set(fig2,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)]);
print(fig2, strcat('pdfs/', 'vspf-1'),'-dpdf','-r0')
%%

ind_high = find(classHistory(:,8)>= 0.7);
ind_low = find(classHistory(:,8) < 0.7);


% plot( classHistory(ind_high,1), classHistory(ind_high,2), '.g', 'MarkerSize', 10);
% plot( classHistory(ind_low,1),  classHistory(ind_low,2), '.r', 'MarkerSize', 10);  


ut = classHistory(:,4:5);
positions = classHistory(:,1:2);
theta = classHistory(:,3);
n = length(classHistory);
rot_ut = zeros(n,2);
for i = 1:n
    rot_ut(i,:) = [cos(theta(i)) -sin(theta(i)); sin(theta(i)) cos(theta(i))] * ut(i,:)'; 
end
sub_low = ind_low(2:2:end);
sub_high = ind_high(2:4:end);
% qg = quiver(positions(ind_high,1), positions(ind_high,2), rot_ut(ind_high,1), rot_ut(ind_high,2), 0, 'g', 'LineWidth', 1, ...
%      'MaxHeadSize', 0.1);
% qr = quiver(positions(ind_low,1), positions(ind_low,2), rot_ut(ind_low,1), rot_ut(ind_low,2), 0, 'r', 'LineWidth', 1, ... 
%      'MaxHeadSize', 0.1);

qg = quiver(positions(sub_high,1), positions(sub_high,2), rot_ut(sub_high,1), rot_ut(sub_high,2), 0, 'g', 'LineWidth', 1, ...
     'MaxHeadSize', 0.2);
qr = quiver(positions(sub_low(1:6),1), positions(sub_low(1:6),2), rot_ut(sub_low(1:6),1), rot_ut(sub_low(1:6),2), 0, 'r', 'LineWidth', 1, ... 
     'MaxHeadSize', 1);
 
 %% Markers
load('away.mat');
theta = estimatedPose(3);
plot(estimatedPose(1), estimatedPose(2), 'ro', 'MarkerFaceColor', 'r' , 'MarkerEdgeColor', 'k', 'MarkerSize', 10)
ydiff1 = new_vf_samples - vf_samples;

%% plot pose
p = 12;
pose = [positions(p,1), positions(p,2), theta(p)];
plot( pose(1),pose(2),'o','MarkerSize',10,'MarkerEdgeColor', 'k', 'MarkerFaceColor', 'c', 'LineWidth', 2);
% plot([pose(1), pose(1) + 0.75*cos(pose(3))], [pose(2), pose(2) + 0.75*sin(pose(3))], '*','MarkerSize',2,'Color','k','LineStyle','-');
v = 0.9;
qx = quiver(pose(1), pose(2), v*cos(pose(3)), v*sin(pose(3)), 'Color', 'k', 'LineWidth', 1, 'MaxHeadSize', 1);
%%
load('good_far.mat');
theta = estimatedPose(3);
plot(estimatedPose(1), estimatedPose(2), 'ro', 'MarkerFaceColor', 'g', 'MarkerEdgeColor', 'k', 'MarkerSize', 10)
ydiff2 = new_vf_samples - vf_samples;


qg = quiver(positions(sub_high(1:13),1), positions(sub_high(1:13),2), rot_ut(sub_high(1:13),1), rot_ut(sub_high(1:13),2), 0, 'g', 'LineWidth', 1, ...
     'MaxHeadSize', 1);
qr = quiver(positions(sub_low(1:14),1), positions(sub_low(1:14),2), rot_ut(sub_low(1:14),1), rot_ut(sub_low(1:14),2), 0, 'r', 'LineWidth', 1, ... 
     'MaxHeadSize', 1);

 
p = 80;
pose = [positions(p,1), positions(p,2), theta(p)];
plot( pose(1),pose(2),'o','MarkerSize',10,'MarkerEdgeColor', 'k', 'MarkerFaceColor', 'c', 'LineWidth', 2);
% plot([pose(1), pose(1) + 0.75*cos(pose(3))], [pose(2), pose(2) + 0.75*sin(pose(3))], '*','MarkerSize',2,'Color','k','LineStyle','-');
v = 0.9;
qx = quiver(pose(1), pose(2), v*cos(pose(3)), v*sin(pose(3)), 'Color', 'k', 'LineWidth', 1, 'MaxHeadSize', 1);

%%

load('obstacle4.mat');
theta = estimatedPose(3);
plot(estimatedPose(1), estimatedPose(2), 'ro', 'MarkerFaceColor', 'r', 'MarkerEdgeColor', 'k', 'MarkerSize', 10)
ydiff3 = new_vf_samples - vf_samples;


qg = quiver(positions(sub_high(1:15),1), positions(sub_high(1:15),2), rot_ut(sub_high(1:15),1), rot_ut(sub_high(1:15),2), 0, 'g', 'LineWidth', 1, ...
     'MaxHeadSize', 1);
qr = quiver(positions(sub_low(1:15),1), positions(sub_low(1:15),2), rot_ut(sub_low(1:15),1), rot_ut(sub_low(1:15),2), 0, 'r', 'LineWidth', 1, ... 
     'MaxHeadSize', 1);

 
p = 92;
pose = [positions(p,1), positions(p,2), theta(p)];
plot( pose(1),pose(2),'o','MarkerSize',10,'MarkerEdgeColor', 'k', 'MarkerFaceColor', 'c', 'LineWidth', 2);
% plot([pose(1), pose(1) + 0.75*cos(pose(3))], [pose(2), pose(2) + 0.75*sin(pose(3))], '*','MarkerSize',2,'Color','k','LineStyle','-');
v = 0.9;
qx = quiver(pose(1), pose(2), v*cos(pose(3)), v*sin(pose(3)), 'Color', 'k', 'LineWidth', 1, 'MaxHeadSize', 1);



%%
load('obstacle.mat');
theta = estimatedPose(3);
plot(estimatedPose(1), estimatedPose(2), 'ro', 'MarkerFaceColor', 'r', 'MarkerEdgeColor', 'k', 'MarkerSize', 10)
ydiff4 = new_vf_samples - vf_samples;

qg = quiver(positions(sub_high(1:16),1), positions(sub_high(1:16),2), rot_ut(sub_high(1:16),1), rot_ut(sub_high(1:16),2), 0, 'g', 'LineWidth', 1, ...
     'MaxHeadSize', 1);
qr = quiver(positions(sub_low(1:20),1), positions(sub_low(1:20),2), rot_ut(sub_low(1:20),1), rot_ut(sub_low(1:20),2), 0, 'r', 'LineWidth', 1, ... 
     'MaxHeadSize', 1);

 
p =103;
pose = [positions(p,1), positions(p,2), theta(p)];
plot( pose(1),pose(2),'o','MarkerSize',10,'MarkerEdgeColor', 'k', 'MarkerFaceColor', 'c', 'LineWidth', 2);
% plot([pose(1), pose(1) + 0.75*cos(pose(3))], [pose(2), pose(2) + 0.75*sin(pose(3))], '*','MarkerSize',2,'Color','k','LineStyle','-');
v = 0.9;
qx = quiver(pose(1), pose(2), v*cos(pose(3)), v*sin(pose(3)), 'Color', 'k', 'LineWidth', 1, 'MaxHeadSize', 1);



%%
% load('obstacle3.mat');
% theta = estimatedPose(3);
% plot(estimatedPose(1), estimatedPose(2), 'ro', 'MarkerFaceColor', 'r', 'MarkerEdgeColor', 'k', 'MarkerSize', 8)
% ydiff3 = new_vf_samples - vf_samples;

load('door2.mat');
theta = estimatedPose(3);
plot(estimatedPose(1), estimatedPose(2), 'ro', 'MarkerFaceColor', 'r', 'MarkerEdgeColor', 'k', 'MarkerSize', 10)
ydiff5 = new_vf_samples - vf_samples;

qg = quiver(positions(sub_high(1:35),1), positions(sub_high(1:35),2), rot_ut(sub_high(1:35),1), rot_ut(sub_high(1:35),2), 0, 'g', 'LineWidth', 1, ...
     'MaxHeadSize', 1);
qr = quiver(positions(sub_low(1:27),1), positions(sub_low(1:27),2), rot_ut(sub_low(1:27),1), rot_ut(sub_low(1:27),2), 0, 'r', 'LineWidth', 1, ... 
     'MaxHeadSize', 1);

 
p =196;
pose = [positions(p,1), positions(p,2), theta(p)];
plot( pose(1),pose(2),'o','MarkerSize',10,'MarkerEdgeColor', 'k', 'MarkerFaceColor', 'c', 'LineWidth', 2);
% plot([pose(1), pose(1) + 0.75*cos(pose(3))], [pose(2), pose(2) + 0.75*sin(pose(3))], '*','MarkerSize',2,'Color','k','LineStyle','-');
v = 0.9;
qx = quiver(pose(1), pose(2), v*cos(pose(3)), v*sin(pose(3)), 'Color', 'k', 'LineWidth', 1, 'MaxHeadSize', 1);



%%


load('good_near.mat');
theta = estimatedPose(3);
plot(estimatedPose(1), estimatedPose(2), 'ro', 'MarkerFaceColor', 'g', 'MarkerEdgeColor', 'k', 'MarkerSize', 10)
ydiff6 = new_vf_samples - vf_samples;


qg = quiver(positions(sub_high(1:51),1), positions(sub_high(1:51),2), rot_ut(sub_high(1:51),1), rot_ut(sub_high(1:51),2), 0, 'g', 'LineWidth', 1, ...
     'MaxHeadSize', 1);
qr = quiver(positions(sub_low(1:28),1), positions(sub_low(1:28),2), rot_ut(sub_low(1:28),1), rot_ut(sub_low(1:28),2), 0, 'r', 'LineWidth', 1, ... 
     'MaxHeadSize', 1);

 
p =258;
pose = [positions(p,1), positions(p,2), theta(p)];
plot( pose(1),pose(2),'o','MarkerSize',10,'MarkerEdgeColor', 'k', 'MarkerFaceColor', 'c', 'LineWidth', 2);
% plot([pose(1), pose(1) + 0.75*cos(pose(3))], [pose(2), pose(2) + 0.75*sin(pose(3))], '*','MarkerSize',2,'Color','k','LineStyle','-');
v = 0.9;
qx = quiver(pose(1), pose(2), v*cos(pose(3)), v*sin(pose(3)), 'Color', 'k', 'LineWidth', 1, 'MaxHeadSize', 1);


%% Second plot
xs1=ones(length(ydiff1),1);
xs2=ones(length(ydiff2),1);
xs3=ones(length(ydiff3),1);
xs4=ones(length(ydiff4),1);
xs5=ones(length(ydiff5),1);
xs6=ones(length(ydiff6),1);

figure
% subplot(1,2,2);
hold on;
xlabel('Section');
ylabel('\Delta \psi(x_t)');
grid on;
h = plot(xs1, ydiff1, 'o');
c1 = get(h, 'Color');

h = plot(2*xs2, ydiff2, 'o');
c2 = get(h, 'Color');

h = plot(3*xs3, ydiff3, 'o');
c3 = get(h, 'Color');

h = plot(4*xs4, ydiff4, 'o');
c4 = get(h, 'Color');

h = plot(5*xs5, ydiff5, 'o');
c5 = get(h, 'Color');

h = plot(6*xs6, ydiff6, 'o');
c6 = get(h, 'Color');

xticks([1 2 3 4 5 6 ])
axis([0.5 6.5 -0.5 1.5])
