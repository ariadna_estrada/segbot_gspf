function [ obj ] = plot_particles_bins( map, particles, goal, color, bin )
%plot_particles shows a map with arrows representing the particle poses and
%a goal location. User can specify a color for the arrows. If color is not
%specified arrows will be purple. If user specifies a subset of particles
%in 'bin' to highlight, the color to be used should be specified.
%
% Inputs:
%   - map: 1x1 BinaryOccupancyGrid of the world. use
%   robotics.BinaryOccupancyGrid to generate.
%   - particles: n x 3 pose array [x, y, theta]
%   - goal: vector size 2 with goal location [x y]
%   - bin: vector size m with indices of particles in the set to be
%   highlited
%   - color: 1 x 3 array with desired color of the particles
% Outputs: 
%   - obj: struct with fields FigureHandle and AxesHandle
if nargin < 4
    color =  [0.4940, 0.1840, 0.5560];
end
if nargin < 5
    bin = [];
end

obj.FigureHandle = figure('Name','Particle cloud');
show(map);
grid on; hold on;

xlabel('x [m]');
ylabel('y [m]');
obj.AxesHandle = gca;
obj.AxesHandle.GridLineStyle = ':';
obj.AxesHandle.XTick = obj.AxesHandle.YTick;


plot(goal(1), goal(2), 'rp', 'MarkerSize', 10, 'MarkerFaceColor', 'r');

px = particles(:,1);
py = particles(:,2);
pz = particles(:,3);
v = 0.2;
vx = v*cos(pz);
vy = v*sin(pz);

if isempty(bin)
    quiver(px, py, vx, vy, 0, 'Color', color);
else   
    %Show all particles in gray color
    gray = [0.8, 0.8, 0.8];
    quiver(px, py, vx, vy, 0, 'Color', gray);
    
    %Highlight particles from subset contained in bin
    quiver(px(bin), py(bin), vx(bin), vy(bin), 0, 'Color', color, 'LineWidth', 1);
end

end

