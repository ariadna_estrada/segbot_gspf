function [ plots ] = saddle_classification_plots( fig1, holonomic )
%Create and return plot handle which can be used for visualization of
%saddle point classification procedure
%
%Input:
%   -fig1: figure to plot particles and linear approximation when
%   classifiying a saddle point
%   -holonomic: boolean. Return empty plots if holonomic is true.
%Output:
%   -plots: A plot handle



colors = get(groot, 'DefaultAxesColorOrder');
blue = colors(1,:);
red = colors(2,:);
yellow = colors(3,:);

if ~holonomic
    ax = get(fig1, 'CurrentAxes');
    if isempty(ax)
        ax = axes('Parent', fig1);
    end
    cla(ax);
    hold(ax, 'on');

    ax1 = subplot(1,2,1);
    hold(ax1, 'on');
    xlabel('\theta');
    ylabel('Optimal \omega');
    axis([-0.2 2*pi+0.2 -1.1 1.1]);

    plots(1) = plot(ax1, [0], [0], '.k', 'MarkerSize', 10); %original theta values
    plots(2) = plot(ax1, [0], [0], 'r.', 'MarkerSize', 10); %Shifted theta values
    plots(3) = plot(ax1, [0], [0],'Color', blue, 'Linewidth', 2); %Fitted line
%     plots(4) = plot(ax1, [0], [0], 'm+'); %x-axis crossing point

    % 
    ax2 = subplot(1,2,2);
    hold(ax2, 'on');
    xlabel('x cos \theta + y sin \theta');
    ylabel('Optimal v');
    axis([-10 10 -1.1 1.1]);
    
    plots(7) = plot(ax2, [0], [0], '.k', 'MarkerSize', 10); %black dots
    plots(5) = plot(ax2, [0], [0], 'r.', 'MarkerSize', 10); % values
    plots(6) = plot(ax2, [0], [0], 'Color', blue, 'Linewidth', 2); %Fitted line
    
else
    close(fig1);
    plots = [];
end

end


