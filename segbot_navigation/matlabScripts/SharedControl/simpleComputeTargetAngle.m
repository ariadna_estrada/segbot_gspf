function [ targetAngleRobot ] = simpleComputeTargetAngle( g, p )
    % Determine angle to get to goal directly
    targetPoint = g(1:2) - p(1:2);
    targetAngleGlobal = atan2(targetPoint(2),targetPoint(1));
    targetAngleRobot = targetAngleGlobal - p(3);
    targetAngleRobot = robotics.internal.wrapToPi(targetAngleRobot);
   
end

