function segbotSCTimer(mTimer, event, handles)
    %segbotSCTimer Timer update function for the shared control framework
    %   segbotSCTimer(mTimer, ~, handles) updates the state of a segbot
    %   using ROS publishers and subscribers passed into the function
    %   through the handles struct. The controller produces motion commands
    %   according to different blending policies. The user can select a
    %   goal point or a random goal can be generated. A figure displays the
    %   map showing the position of the robot over time. In the same
    %   figure, the user can select between the available shared control
    %   policies. Check the "different goals" checkbox, if the user and the
    %   autonomous planner will have different goals.
    %
    %   Code largely based on exampleHelperTurtleBotObstacleTimer and
    %   TurtleBotObstacleAvoidanceExample
    %
    
    % Declare persistent variables
    persistent goal;
    persistent plotobj;
    persistent mode;
    persistent popup;
    persistent box;
    
    
    %Create object to visualize data and select controller options.

    if isempty(plotobj)
        goal = [];
    end
    

    if isempty(goal)
        try
            if isempty(plotobj)
                plotobj = segbotVisualizer([-10,10,-10,10], handles.map);
                popup = uicontrol('Style', 'popup', 'String', handles.modes,...
                    'Position', [20 20 150 20], 'Callback', @setMode, ...
                    'Parent', plotobj.FigureHandle);
                box = uicontrol('style','checkbox','units','pixels', ...
                    'position',[200,20,200,15],'string','different goals',...
                    'Parent', plotobj.FigureHandle, 'Callback', {@setDiffGoal, handles});
            end
        catch me
            fprintf(2, 'There was an error creating the plot object! The message was:\n%s\n', me.message);
            plotobj = [];
            stop(mTimer);
        end
        
        % Obtain goal point from the user or sample a semi-random one
       
        %goal = returnGoal(handles);
        goal = selectRandomGoal(handles, box.Value);
   
    end
        
    
    % Get latest laser and pose sensor data
    [laserData, poseData] = getSensorReadings(handles.sublaser, handles.subamcl);
    
    %Get joystick data
    joyData = getJoystickData(handles.subjoy);
    
    
    % Plot the robot position and orientation every loop
    try
        plotData(plotobj,poseData,[]);
    catch me
%         fprintf(2, 'There was an error plotting the data! The message was:\n%s\n', me.message);
        plotobj = [];
        stop(mTimer);
    end
    %Make sure the planner has a working status.
    checkStatus(handles, box.Value);

    % Execute the main control loop
    try 
        if (joyData.X ~= 0 || joyData.Z ~= 0) 
            u_segbot = segbotController(handles, joyData, laserData, poseData, goal, mode);
        else
            u_segbot = [0,0];
        end

        if norm(goal-poseData(1:2),2) < 0.75
            u_segbot = [0,0];
            disp('Goal Reached');
            goal = [];
            sound(handles.goal_sound);
        end

        handles.pubmsg.Linear.X  = u_segbot(1);
        handles.pubmsg.Angular.Z = u_segbot(2);
        send(handles.pub,handles.pubmsg);
        
    catch me
         fprintf(2, 'There was an error in the main control loop! The message was:\n%s\n', me.message);
         plotobj = [];
         goal = [];
         stop(mTimer)
    end

    %% Callback functions and Nested Functions  
    function [laserData, poseData] = getSensorReadings(sublaser, subamcl)
        %getSensorReadings - Wait for next laser reading and get most
        %current amcl reading
        
        laserMsg = receive(sublaser);
        poseMsg = subamcl.LatestMessage;
        
        laserData = processLaserMessage(laserMsg);
        poseData = processAMCLMessage(poseMsg);       
    end
    
    function laserData = processLaserMessage(message)
        %processLaserMessage - Process ROS message with laser scan data
        
        laserData.cartesian = readCartesian(message) * [0 1; -1 0];
        [laserData.Ranges, laserData.Angles] = ExtractRangeDataMod(message);
        [laserData.mindist, minIndex] = min(laserData.Ranges(~isnan(laserData.Ranges)));
        laserData.minAngle = laserData.Angles(minIndex);
    end
    
    function poseData = processAMCLMessage(message)
        %processAMCLMessage - Process ROS message with localization data
        
        if isempty(message)
            poseData = [0 0 0];
            return;
        end
        
        pos = message.Pose.Pose;
        xpos = pos.Position.X;
        ypos = pos.Position.Y;
        quat = pos.Orientation;
        angles = quat2eul([quat.W quat.X quat.Y quat.Z]);
        theta = angles(1);
        poseData = [xpos, ypos, theta];
    end

    function joyData = getJoystickData(subjoy)
        % getJoystickData - Retreive user's desired steering direction from
        % joystick message. joyDir is the deflection from the forward
        % motion.
        
        joyMsg = subjoy.LatestMessage;
        joyData.X = double(joyMsg.Axes(2)); %2
        joyData.Z = double(joyMsg.Axes(1)); %1
        joyData.Dir = atan2(joyData.Z, joyData.X);    
    end

    function setMode(source, event)
        %setMode - Changes the blending approach according to the popup
        %menu selection
        
        val = source.Value;
        mode = source.String{val}
    end

    function setDiffGoal(source, ~, handles)
        disp('checkbox clicked')
        diffGoal = source.Value;
   
        if diffGoal
            disp('sending different goals');
        else
            disp('sending the same goal');
        end
        goal = selectRandomGoal(handles, diffGoal);
    end
        
end

function g = returnGoal(handles)
    %returnGoal -  Obtains the desired goal position from user input and
    %sends it to the planner.
    
    prompt = 'Click a goal position "[x,y]": ';
    disp(prompt);
    fig = findall(0,'Type', 'Figure', 'Name', 'Robot Position');
    g = ginput(1);
    
    %send goal to ros planner
    handles.msgGoal.Goal.TargetPose.Pose.Position.X = g(1);
    handles.msgGoal.Goal.TargetPose.Pose.Position.Y = g(2);
    handles.msgGoal.Goal.TargetPose.Pose.Orientation.W = 1;
    send(handles.pubGoal, handles.msgGoal);
end
function gu = selectRandomGoal(handles, send_diff_goals)
    %selectRandomGoal - Selects a random goal from an array of goal points
    %and sends it to the planner. If the user has checked the "different
    %goals" box, a randomly selected goal will be sent to the planner. Such
    %goal might be the same as the user's goal. 
    
    world_goals = handles.world_goals;

    nGoals = length(world_goals);
    gu = world_goals(randi(nGoals),:); %goal for user
    
    if (send_diff_goals)
        gp = world_goals(randi(nGoals),:); %sample another goal for planner
    else
        gp = gu;
    end
    gu
    gp
    
    handles.msgGoal.Goal.TargetPose.Pose.Position.X = gp(1);
    handles.msgGoal.Goal.TargetPose.Pose.Position.Y = gp(2);
    handles.msgGoal.Goal.TargetPose.Pose.Orientation.W = 1;
    send(handles.pubGoal, handles.msgGoal);
end

function checkStatus(handles, diffGoals)
%Check whether the current goal has been aborted. If it has, re-send it to
%the planner. Re-send goal in case planner goal has been reached but is
%different from the user's goal. 

planner_status = handles.substatus.LatestMessage.StatusList.Status;
if (planner_status == 4)
    disp('resending goal to planner')
    send(handles.pubGoal, handles.msgGoal);
elseif (diffGoals && planner_status == 3)
    disp('reached planner goal; resending goal to planner')
    send(handles.pubGoal, handles.msgGoal);
end 

end


function [u_shared] = segbotController(handles, joy, laser, poseData, goal, mode)
    %TURTLEBOTCONTROLLER Returns velocity commands to publish to the robot
    persistent prevState;
    persistent prevPrompt; 
    persistent prevGoal;
    persistent dataobj;
    persistent prevW;
    
%     Initialize state 
    if isempty(mode)
        mode = 'Teleop';
        prevState = mode;
    end
    
    if isempty(dataobj)
        dataobj = segbotData();
        prevGoal = goal;
    end
    
    if isempty(prevW)
        prevW = 0;
    end
    
    vfh = handles.vfh;
    subeband = handles.subeband;
    map_inflated = handles.map_inflated;
    
    % Gains and parameters
    maxW = 0.5;
    maxLinV = 0.5;
    
    u_joy = [joy.X*maxLinV, joy.Z*maxW];
    
   
    if ~strcmp(mode,prevState)
        mode
        reset(vfh);
    end
    
    if (goal ~= prevGoal)
        dataobj.clearData;
    end
    

    switch mode
        case 'EfficiencyBlend'
            %Blending based on approach described in "An adaptive scheme
            %for Wheelchair Navigation Collaborative Control" by Urdiales

            u_eband = processEbandMessage(subeband);
            %Compute planner efficiencies
            u_eband_ang = atan2(u_eband(2), u_eband(1));
            r_smooth = exp(-1*abs(u_eband_ang)); 
            r_direct = 1;
            r_safety = 1 - exp(-1 * abs (angdiff(laser.minAngle, u_eband_ang)));
            r_efficiency = (r_smooth + r_direct + r_safety) / 3;
            
            fprintf('r_sm: %.2f\t r_di: %.2f\t r_sa: %.2f\t r_eff: %.2f\n', r_smooth, r_direct, r_safety, r_efficiency);
            %Compute user efficiencies
            h_smooth = exp(-1 * abs(joy.Dir) );
            h_direct = exp(-1 * abs( angdiff ( u_eband_ang , joy.Dir)));
            h_safety = 1 - exp(-1 * abs (angdiff(laser.minAngle, joy.Dir)));
            h_efficiency = (h_smooth + h_direct + h_safety) / 3;
            fprintf('h_sm: %.2f\t h_di: %.2f\t h_sa: %.2f\t h_eff: %.2f\n', h_smooth, h_direct, h_safety, h_efficiency);
            
            %blend if human_efficiency falls below threshold
            if h_efficiency < 0.4    
                alpha = [h_efficiency, r_efficiency];
                alpha = alpha ./ sum(alpha) ; 
                u_blend = u_joy * alpha(1) + u_eband * alpha(2) ; 
                u_shared = u_blend;
            else
                u_shared = u_joy;
            end
            
            next_pose = sample_motion_nonholonomic(u_shared, poseData, 2);
            prob_collision = getOccupancy(handles.global_costmap, next_pose(1:2));
            if prob_collision > 0.4
                disp('avoiding collision');
                u_shared = computeVFHvelocities(vfh, u_shared, laser);
            end
            
        case 'DynamicSC'
            u_eband = processEbandMessage(subeband);
            if(laser.mindist < 0.3 && u_joy(1) > 0)
                %reduce v
                v = u_eband(1);
            else
                v = u_joy(1);
            end
            prev_best = 0;
            best_k1 = 1;
            best_k2 = 0;
            for k1 = 1:-0.1:0
               k2 = 1-k1;
               w = k1*u_joy(2) + k2*u_eband(2);
               u_ang = atan2(w, v);
               
               next_pose = sample_motion_nonholonomic([v,w], poseData, 2);
               prob_collision = getOccupancy(handles.global_costmap, next_pose(1:2));
               
               safety = 1-prob_collision; 
               if (safety < 0.6)
                   safety = 0; 
               end 
               comfort = exp(-3 * abs(w - prevW));
               obedience = exp(-2 * abs( angdiff ( u_ang , joy.Dir)));
               fprintf('K1: %.1f \t s: %.2f\t c: %.2f \t o: %.2f\n', k1, safety, comfort, obedience);
               
               current_min = min([safety, comfort, obedience]);
               if current_min > prev_best
                   prev_best = current_min;
                   best_k1 = k1; best_k2 = k2;  
               end               
            end
            fprintf('k1: %.1f \t k2: %.1f\n', best_k1, best_k2);   
            
            w_blend  = best_k1 * u_joy(2) + best_k2 * u_eband(2);

            u_blend = [v, w_blend];
            next_pose = sample_motion_nonholonomic(u_blend, poseData, 1);
            prob_collision = getOccupancy(handles.global_costmap, next_pose(1:2));
            
            if prob_collision > 0.4
                u_shared = computeVFHvelocities(vfh, u_joy, laser);
            else
                u_shared = u_blend;
            end
            prevW = u_shared(2);
        case 'Teleop'
           u_shared = u_joy;
            
        case 'CollisionAvoidance'           
            next_pose = sample_motion_nonholonomic(u_joy, poseData, 1);
            prob_collision = getOccupancy(handles.global_costmap, next_pose(1:2));
            
            if prob_collision > 0.4
                u_shared = computeVFHvelocities(vfh, u_joy, laser);
            else
                u_shared = u_joy;
            end
        case 'StopCollision'
            next_pose = sample_motion_nonholonomic(u_joy, poseData, 1);
            prob_collision = getOccupancy(handles.global_costmap, next_pose(1:2));
            
            if prob_collision > 0.4
                u_shared = [0,0];
                vfhAngle = step(vfh, laser.Ranges, laser.Angles, joy.Dir);
                if vfhAngle < 0
                    prompt = 'Try turning right';
                else
                    prompt = 'Try turning left';
                end
                if ~strcmp(prompt, prevPrompt)
                    disp(prompt)
                end
                prevPrompt = prompt;
            else
                prevPrompt = [];
                u_shared = u_joy;      
            end
        case 'NearGoalAutoSwitch'
            [ebandPlan.Waypoints, ebandPlan.Dist] = extractWaypoints(handles.subpath.LatestMessage);
            [nearGoal, ~] = checkNearGoal(goal, poseData, laser, ebandPlan);
            
            if nearGoal
                u_shared = processEbandMessage(subeband);                 
            else
                next_pose = sample_motion_nonholonomic(u_joy, poseData,1);
                prob_collision = getOccupancy(handles.global_costmap, next_pose(1:2));
            
                if prob_collision > 0.4
                    u_shared = computeVFHvelocities(vfh, u_joy, laser);
                else
                    u_shared = u_joy;
                end
            end
                       
        case 'HighLevelBlend'
            %Blending with High Level Goal from Erdogan paper
            %Steps away control from the user based on euclidean distance
            %to the goal. Perhaps should be plan distance. 
            dc = 5; %At what distance the control is at 50/50
            tau = 0.75;  %How fast the control steps away from the user, higher values produce steeper changes.
            
            d = norm(goal-poseData(1:2),2);
            alpha = 1 / (1 + exp(-tau*(d - dc))); %user weight
            [u_eband] = processEbandMessage(subeband);
            u_blend =  (alpha * u_joy + (1 - alpha) * u_eband );
            
            next_pose = sample_motion_nonholonomic(u_blend, poseData, 1);
            prob_collision = getOccupancy(handles.global_costmap, next_pose(1:2));
            
            if prob_collision > 0.4
                disp('avoiding collision');
                u_shared = computeVFHvelocities(vfh, u_blend, laser);
            else
                u_shared = u_blend; 
            end
            
        case 'AwayFromGoalBlend'
            %Blending similar to HighLevelBlend but inverse values for
            %alpha. When the user is away from the goal, the planner will
            %have a higher weight. As the user gets closer to the goal it
            %gets more control. 
            dc = 10; %At what distance the control is at 50/50
            tau = 0.75;  %How fast the control steps away from the planner, higher values produce steeper changes.
            
            d = norm(goal-poseData(1:2),2);
            alpha = 1 / (1 + exp(-tau*(d - dc))); %planner weight
            u_eband = processEbandMessage(subeband);
            u_blend =  ((1-alpha) * u_joy + alpha * u_eband );
            
            next_pose = sample_motion_nonholonomic(u_blend, poseData,1);
            prob_collision = getOccupancy(handles.global_costmap, next_pose(1:2));
            
            if prob_collision > 0.4
                u_shared = computeVFHvelocities(vfh, u_blend, laser);
            else
                u_shared = u_blend;
            end
            
        case 'TestStopCollision'
            [u_eband] = processEbandMessage(subeband);
            next_pose = sample_motion_nonholonomic(u_eband, poseData, 0.1);
            prob_collision = getOccupancy(handles.global_costmap, next_pose(1:2));

             if (prob_collision > 0.78)
                u_shared = computeVFHvelocities(vfh, u_eband, laser);
                disp('avoiding collision');
             else        
                u_shared = u_eband;
             end
            
        case 'DisagreementBlend'
            %Blend according to a disagreement metric between the user and
            %the planner inputs.
            u_eband = processEbandMessage(subeband);
            ebandAngle = atan2(u_eband(2), u_eband(1));
            
            cDif = abs(angdiff(joy.Dir,ebandAngle));
%             fprintf('cDif = %.2f \t aDif = %.2f \t stdD = %.2f\n',cDif, dataobj.averageValue, dataobj.stdDevValue);          
            if (length(dataobj.DataHistory) < 30 || dataobj.averageValue >= pi/2)
                disp('user in control');
                %follow user command if disagreement is high
                next_pose = sample_motion_nonholonomic(u_joy, poseData, 1);
                prob_collision = getOccupancy(handles.global_costmap, next_pose(1:2));
                
                if prob_collision > 0.4
                    disp('avoiding collision');
                    u_shared = computeVFHvelocities(vfh, u_joy, laser);
                else
                    u_shared = u_joy;
                end
            else
                if(cDif > (pi/2))
                    fprintf('cDif = %.2f\t aVal = %.2f\t stdD= %.2f\n', cDif, dataobj.averageValue, dataobj.stdDevValue);
                    disp('user back in control');
                    dataobj.clearData();
                    u_shared = u_joy;
                else
                    alpha = 0.48*dataobj.averageValue;
                    fprintf('blending with alpha = %.2f\n', alpha);
                    u_blend =  (alpha * u_joy + (1 - alpha) * u_eband );
                    
                    next_pose = sample_motion_nonholonomic(u_blend, poseData, 1);
                    prob_collision = getOccupancy(handles.global_costmap, next_pose(1:2));
                    
                    if prob_collision > 0.4
                        disp('avoiding collision');
                        u_shared = computeVFHvelocities(vfh, u_blend, laser);
                    else
                        u_shared = u_blend;
                    end
                end
            end
            dataobj.addAngle( cDif );
            
    end
   prevState = mode;
   prevGoal = goal;
end
function [u_free] = computeVFHvelocities(vfh, u, laserData)
%[u_free] = computeVFHvelocities(vfh, u, laserData)
% vfh: Vector field histogram object
% u: control input vector [v w]
% laserData: struct with fields Ranges and Angles
persistent angPID

%Initialize PID Controller
if isempty(angPID)
    angGains = struct('pgain',0.9,'dgain',0.1,'igain',0,'maxwindup',0','setpoint',0);
    angPID = ExampleHelperPIDControl(angGains);
end

%Parameters
maxWController = 0.7; %The controller cap is high to react quickly to collisions.

uAngle = atan2(u(2),u(1));
vfhAngle = step(vfh, laserData.Ranges, laserData.Angles, uAngle);

if isnan(vfhAngle) 
    u_free = [0, u(2)];
else
    angleDiff = abs( angdiff(uAngle, vfhAngle) );
    u_free(1) = u(1)*exp(-angleDiff);
    
    u_free(2) = update(angPID, -vfhAngle);
    if abs(u_free(2)) > maxWController
        u_free(2) = maxWController*sign(u_free(2));
    end
end
end 

function [nearGoal, targetAngle] = checkNearGoal(goal, pose, laser,ebandPlan)
%[nearGoal, targetAngle] = checkNearGoal(goal, pose, laser)
%Checks whether the robot is in the vicinity of the goal (5 meter radius)
%and also verifies that the shortest distance between the robot and the
%goal is clear of obstacles. 

%Parameters
nearGoalThresh = 5;     

d1 =  norm(goal-ebandPlan.Waypoints(end,:),2);
distToGoal = ebandPlan.Dist + d1;

% distToGoal = norm(goal-pose(1:2),2);
targetAngle = simpleComputeTargetAngle(goal, pose);

if (distToGoal < nearGoalThresh)        
    if(targetAngle < pi/2 && targetAngle > -pi/2)
        indices = find((laser.Angles > targetAngle - pi/12)  & (laser.Angles < targetAngle + pi/12));
        mindistTargetRegion = min(laser.Ranges(indices));  %#ok<FNDSB>
        if mindistTargetRegion > distToGoal 
            nearGoal = 1;
            return
        end
    end
end
nearGoal = 0;

end

function [u_eband] = processEbandMessage(subeband)
ebandMsg = subeband.LatestMessage;
u_eband(1) = ebandMsg.Linear.X;
u_eband(2) = ebandMsg.Angular.Z;
end

