classdef segbotData < handle
    %segbotData - Class for storing temporal variables used in the shared
    %control algorithms used with the segbot. 
    %   OBJ = segbotData() The object keeps track of disagreement
    %   metrics between the autonomous planner and the user inputs.
    %
    %   segbotData methods:
    %      clearData                - Clears stored data. Use for new goal.
    %      addAngle                 - Adds new angle difference. 
    %
    %   segbotData properties:
    %      DataHistory              - History if disagreement metric
    %
    % Ariadna Estrada 04/20/2018
    
    properties
        DataHistory = [];      % Handle to the object figure
        averageValue = [];     % Average of angle differences
        stdDevValue = [];      % Standard deviation of current samples
    end
    
    properties (Constant, Access = private)      
        %MaxDataHistorySize The maximum number of values that should be stored in DataHistory
        %   DataHistory is a FIFO and if it reaches MaxDataHistorySize, the
        %   oldest element will be deleted.
        MaxDataHistorySize = 50     
    end
    
    methods (Access = public)
        function obj = segbotData()
            %segbotData - Constructor 
        end
        
       
        function addAngle(obj, angle)
            %angleDifference - Keeps track of the latest angle differences 
                                 
            % Keep a history of previous robot positions (fixed-size FIFO)
            angleHistoryHandle = angle;
            addNewAngleHandle(obj, angleHistoryHandle);
            
            %Compute average of current set of angle differences
            obj.averageValue = sum(obj.DataHistory)/length(obj.DataHistory);
            obj.stdDevValue = std(obj.DataHistory);
        end
        
        function clearData(obj)
            obj.DataHistory = [];
            obj.averageValue = [];
            obj.stdDevValue = [];
        end
        
    end
    
    methods (Access = protected)
        function addNewAngleHandle(obj, angleHandle)
            %addNewAngleHandle Store a new angle handle
            
            obj.DataHistory = [obj.DataHistory; angleHandle];
            if length(obj.DataHistory) > obj.MaxDataHistorySize
                % If we reached the maximum size of the array, delete the
                % oldest angle handle.
                obj.DataHistory(1) = [];
            end
        end
        
    end
    
end

