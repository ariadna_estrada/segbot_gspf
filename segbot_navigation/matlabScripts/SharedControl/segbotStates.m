classdef segbotStates
    %segbotStates - Enumerated class with no methods. Contains
    %   enumerated list of Segbot states
    
    %   Modified by Ariadna 15-March-2018
    
    enumeration
        Teleop, TeleopCollisionAvoidance, NearGoalAutonomySwitch, NearGoalSteering, HighLevelBlend, SteeringCorrection
    end
    
end