function random_goal = returnRandomGoal(handles)
    %returnRandomGoal uses costmap to generate a random goal 
    persistent costmap
    persistent fs_row
    persistent fs_col
    persistent n
    
    if isempty(costmap)
        costmap = handles.costmap;
        m_cost = ones(size(costmap.cost))*100;
        m_cost(41:costmap.width-40, 41:costmap.height-40) = costmap.cost(41:costmap.width-40, 41:costmap.height-40);
        [fs_row, fs_col] = find( m_cost == 0);
        n = length(fs_row);
    end
    
    prompt = 'Finding random goal location';
    disp(prompt);
    
    rnd_id = randi(n,1);
    random_goal = [ fs_row(rnd_id),fs_col(rnd_id)]*costmap.resolution + costmap.origin;

    handles.msgGoal.Goal.TargetPose.Pose.Position.X = random_goal(1);
    handles.msgGoal.Goal.TargetPose.Pose.Position.Y = random_goal(2);
    handles.msgGoal.Goal.TargetPose.Pose.Orientation.W = 1;
    send(handles.pubGoal, handles.msgGoal);
end