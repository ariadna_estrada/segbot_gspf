%Script to sample p(xt | ut, xt-1) for a holonomic robot described in
%Probabilistic Robotics chapter 5, exercise 7. 
%Last modified 10/20/2016

clear all; clc;

%Set state vector at t-1
xt_1 = [0, 0, 0]; %Start with location at 0,0 and theta = 0.
plot(xt_1(1), xt_1(2), 'g*'); hold on;

%Set input vector ut 
ut = [1; 1; 0]; %Vx = 1, Vy = 1, w = 0.

for i = 1:400
    x = sample_motion_holonomic(ut, xt_1);
    plot(x(1), x(2), '.r');
end

hold off;
xlabel('x location');
ylabel('y location');
axis([-5 10 -5 10]);