function [ isProductive ] = classifyInput(particle_set, value_fcn, joyInput, saddle_plot, class_plot )
%classifyInput(particle_set, value_fcn)
%   Function to test whether the user's input falls within the span of the
%   particles.
%Inputs:
%   particle_set
%   value_fcn
%Outputs: 
%   isProductive: Boolean. Indicates if the current user input is
%   productive towards the current goal location. 
global classHistory

[searchDir, gradientSamples] = classBestActionForValueFunction3D(particle_set, value_fcn.g, value_fcn.action, joyInput);

% gsamp is an mx3 real matrix of the gradients at each particle, with NaNs removed.
gsamp = gradientSamples3D(particle_set, value_fcn.g, value_fcn.action;
gradient_samples = gsamp;

% Remove NaNs from gsamp.
nanRows = any(isnan(gsamp),2);
gsamp(nanRows,:) = [];
particle_set(nanRows,:) = [];


gsamp = [gsamp ];





classHistory = [classHistory; isProductive];
npoints = length(classHistory);
set(class_plot(1), 'xdata', linspace(0, npoints, npoints+1), 'ydata', classHistory);
end


