function [ps, vf_samples] = valueFunctionSamples3D(particle_set, grid_info, mttr, XS, VS, remove_nans)
% Return interpolated samples of the value function at each particle.
% 
% Input:
% 	- particle_set: mx3 real matrix where each row is a particle (sample).
%	- grid_info: 1x1 struct. grid_info.xs is an nxn grid where the value
%       function was computed. See ttr.m for more details.
%	- mttr: grid with value function values.
%
% Output:
%   - vf_samples: column vector of size m.  Interpolated value function
%       samples at the particle locations. Row k is the value function at
%       the k-th particle in particle_set. May contain NaN.
%
% Neil Traft, 2015/08/18
% Modified by Ariadna Estrada, 2017/12/06

% Interpolate on the particle set. We need to extend the grid one more cell in
% the theta dimension to fully cover the [0,2pi] range. We replicate the values
% from the first cell into the last cell for a periodic boundary.
if nargin < 5
    X1 = cat(3, grid_info.xs{1}, grid_info.xs{1}(:,:,1));
    X2 = cat(3, grid_info.xs{2}, grid_info.xs{2}(:,:,1));
    X3 = cat(3, grid_info.xs{3}, ones(grid_info.shape(1:2))*2*pi);
    V1 = cat(3, mttr, mttr(:,:,1));

else
    X1 = XS.X1;   
    X2 = XS.X2; 
    X3 = XS.X3;
    V1 = VS.V4;
end

Q1 = particle_set(:,1);
Q2 = particle_set(:,2);
Q3 = particle_set(:,3);

vf_samples = interpn(X1, X2, X3, V1, Q1, Q2, Q3);

if remove_nans
    valids = ~any(isnan(vf_samples),2);
    ps = particle_set(valids,:);
    vf_samples = vf_samples(valids,:);
else
    ps = particle_set;
end
    
end
