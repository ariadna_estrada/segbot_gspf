function [ Xp ] = sample_motion_holonomic( ut, xt_1 )
%sample_motion_holonomic Function to sample p(xt | ut, xt_1) for a
%holonomic robot described in Probabilistic Robotics Chapter 5 question 7
%Input Arguments:
%   xt: Matrix size n x 3. Contains the particle cloud of the robot at time t. 
%       Each row represents a state hypothesis of the form xt = (x y theta)'
%   ut: Vector of size 2. Contains the controls for the robot at
%   time t. ut = (Vx Vy)
%Output Arguments: 
%   Xp:  Matrix of size n x 3. Sampled values for the poses
%   at time t given the controls at time t and the pose at time t-1.

%Modification Log:
%10/20/2016 Ariadna Estrada Initial creation
%12/04/2017 Ariadna Estrada renamed sample to sample_normal, vectorized
%code to work with a set of particles instead of single sample. Removed
%theta motion.



dt = sample_normal(0.0230448, 0.155986 ); % hardcoded values from gaussian fit
a1 = 0.005;

vx = ut(1);
vy = ut(2);


x = xt_1(:,1);
y = xt_1(:,2);
% theta = xt_1(:,3);

vx_hat = vx + sample_normal(sqrt(a1*vx^2 + a1*vy^2));
vy_hat = vy + sample_normal(sqrt(a1*vx^2 + a1*vy^2));

xp = x + vx_hat.*dt;
yp = y + vy_hat.*dt;

Xp = [xp, yp];
end

