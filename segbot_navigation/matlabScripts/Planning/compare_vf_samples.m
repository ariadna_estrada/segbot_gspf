function [ downhill ] = compare_vf_samples( vf_samples, vf_samples_translated )
%COMPARE_VF_SAMPLES Function to compare two sets of value function samples
%and evaluate the fraction of the particles that are going downhill. 
% Input:
% 	- vf_samples: Column vector of size n where each row is a sample of the
% 	              value function. See valueFunctionSamples2D.m.
%   - vf_samples_translated: Column vector of same size as vf_samples.
%                            Might content NaN values. Each row is a sample
%                            of the value function for a translated
%                            particle location.
%
% Output:
%   - downhill: scalar. Value between 0 - 1 to indicate fraction of rows
%     where vf_samples_translated < vf_samples. 
%
% Ariadna Estrada, 2017/12/05

nsamples1 = length(vf_samples);
nsamples2 = length(vf_samples_translated);
if nsamples1 ~= nsamples2
    warning('Value function samples have different lengths');
    return;
end

%Remove NaNs from vf_samples_translated
valids = ~any(isnan(vf_samples_translated),2);
vf_samples_translated = vf_samples_translated(valids,:);
vf_samples = vf_samples(valids,:);

downhill = sum(vf_samples_translated < vf_samples) / nsamples1;


end

