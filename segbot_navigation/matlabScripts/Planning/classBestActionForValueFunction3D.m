function [searchDir, gsamp, actionSamples, particle_set] = classBestActionForValueFunction3D(particle_set, grid_info, grad, XS, VS)
% Return the consensus search direction found by gradient sampling, and the
% samples of the gradient at each particle.
% 
% Input:
% 	- particle_set: mx3 real matrix where each row is a particle (sample).
%	- grid_info: 1x1 struct. grid_info.xs is an nxn grid where the value
%       function was computed. See ttr.m for more details.
%	- grad: 3x1 cell array. grad{1}, grad{2}, grad{3} contain the gradient
%       w.r.t. x, y, theta respectively (each nxn grid is flattened into a
%       vector).
%   - plots: A 2-vector of scatter plot handles: the first element is for
%       plotting gradient samples, the second handle is for plotting the search 
%       direction (see gradient_plots.m).
%
% Output:
%   - searchDir: 2x1 unit vector giving linear and angular velocity [v; w].
%       Result is clamped s.t. |v| <= 1, |w| <= 1.
%   - gsamp: mx3 interpolated gradient samples at the particle
%       locations. Row k is the gradient at the k-th particle in particle_set.
%       Nan values are removed.
%   - actionSamples: mx2 giving optimal aciton in v and w for each particle. Value is
%   clamped to -1 or 1. 
%   - particle_set: mx3 particle set wit NaNs removed.
%
% Neil Traft, 2015/03/20
% Modified by Ariadna 2017/10/17
%

% Constants to determine whether the direction vector is "zero".
relative_zero = 0.01;
% Picked this absolute constant because the value function should be
% producing gradients that are close to magnitude 1.
absolute_zero = 0.01;

% gsamp is an mx3 real matrix of the gradients at each particle, with NaNs removed.
gsamp = gradientSamples3D(particle_set, grid_info, grad, XS, VS);
gradient_samples = gsamp;

% Remove NaNs from gsamp.
nanRows = any(isnan(gsamp),2);
gsamp(nanRows,:) = [];
particle_set(nanRows,:) = [];


% In case of error.
if any(~isfinite(gsamp(:))) || ~isreal(gsamp)
    warning('In bestActionForValueFunction3D: gradient is either nan or complex and will be included in SR1 update.');
end

if isempty(gsamp)
    warning('In bestActionForValueFunction3D: all gradients are NaN.');
    searchDir = [NaN;NaN];
    actionSamples = [NaN;NaN];
	return;
end

% Get search direction from the gradient samples.
[searchDir, actionSamples] = classBestActionForGradientSamples3D(particle_set, gsamp);
% a = -searchDir;
% a2 = -sd2;

end
