function [ Xp ] = sample_motion_nonholonomic( ut, xt_1, dt)
%sample_motion_nonholonomic Function to sample poses xt = (x' y' theta')'
%from a pose xt_1 = (x y theta)' and a velocity control ut = (v w)' for a
%robot with non-holonomic dynamics. See Table 5.3 from Probabilistic
%Robotics.
%
%Input Arguments:
%   xt_1: Matrix size n x 3. Contains the particle cloud of the robot at time t. 
%       Each row represents a state hypothesis of the form xt_1 = (x y theta)'
%   ut: Vector of size 2. Contains the velocity controls for the robot at
%   time t. ut = (v w)'
%Output Arguments: 
%   Xp:  Matrix of size n x 3. Sampled values for the poses
%   at time t given the controls at time t and the pose at time t-1.

%Modification Log:
%2017/12/07 Ariadna Estrada Initial creation
%2018/04/19 Ariadna Estrada Added validation for zeroed controls and
%optional dt parameter.

if (ut == 0)
    Xp = xt_1;
    return
end

% dt = sample_normal(0.275858, 0.745281); % hardcoded values from gaussian fit (simulation)
% dt = sample_normal(0.0840457, 0.35); %data from segbot in lab
if(nargin < 3)
    dt = 2;
end

%The values a1 - a6 are the parameters of the motion noise. 
a1 = 0.02;
a2 = 0.002;
a3 = 0.001;
a4 = 0.005;
a5 = 0.001;
a6 = 0.001;

v = ut(1);
w = ut(2);


x = xt_1(:,1);
y = xt_1(:,2);
theta = xt_1(:,3);

v_hat = v + sample_normal( sqrt(a1 * v^2 + a2 * w^2) );
w_hat = w + sample_normal( sqrt(a3 * v^2 + a4 * w^2) );
t_hat = sample_normal( sqrt(a5 * v^2 + a6 * w^2) );

vw = (v_hat / w_hat);
xp = x - vw .* sin(theta) + vw * sin(theta + w_hat * dt); 
yp = y + vw .* cos(theta) - vw * cos(theta + w_hat * dt);
thp = theta + w_hat * dt + t_hat * dt;

Xp = [xp, yp, thp];
end

