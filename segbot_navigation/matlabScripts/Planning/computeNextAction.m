function [ action ] = computeNextAction( particle_set, value_fcn, toggleBtn, amclSvc, plot)
%computeNextAction get next set of velocities
%Input Arguments: 
%   -particle_set: mx2 or mx3 depending on type of robot.
%   -value_fcn: 1x1 struct containing action/grid-specs/goal/mttr/obstacle
%   fields. See ttr.m. 
%   -toggleBtn: 1x1 UIControl for start/stop button.
%   -amclSvc: 1x1 struct contains client and request fields for amcl
%   service. Used to call no motion updates.
%   -plot: 1x7 Line handles to visualize saddle point classification. 
%Output Arguments:
%   -action: 1x2 velocities in x/y for the holonomic case and
%   linear/angular for the non-holonomic.

global gradientSamples
global actionSamples
global start 
global estimatedPose
global saddleCount

vmax = 0.3;
wmax = 0.2;

ndims = length(value_fcn.action);

if ndims == 2, holonomic = true; else holonomic = false; end;

if holonomic %holonomic case
    
   [searchDir, gradientSamples] = bestGradientForValueFunction2D(particle_set, value_fcn.g, value_fcn.action, []);      

    if searchDir == 0 
        disp('At saddle point');
        [searchDir, xc] = directionAtSaddle(gradientSamples, particle_set);
        if isnan(searchDir)
            shifted_ps = particle_set - xc'; 
            searchDir =  bestGradientForGradientSamples2D(shifted_ps, []);
            if searchDir == 0 
                disp('xc is in the span of the particles');
                disp('goal reached');
                set(toggleBtn, 'String', 'Start', 'Value', 0);
                start = 0;
            else
                disp('xc is not in the span of the particles');
                attemptLocalization(amclSvc);
                searchDir = [0 0];
            end
        end
    elseif isnan(searchDir) 
        disp('received NaN searchDir');
        attemptLocalization(amclSvc);
    end
    
    
    action = searchDir;
else %non holonomic case
    [searchDir, gradientSamples] = bestActionForValueFunction3D(particle_set, value_fcn.g, value_fcn.action, [], value_fcn.XS, value_fcn.VS);

    searchDir = [searchDir(1)*vmax; searchDir(2)*wmax];

    if searchDir == 0 
        gs = gradientSamples;
        ps = particle_set;
        % Remove NaNs from gs and ps
        nanRows = any(isnan(gradientSamples),2);
        gs(nanRows,:) = [];
        ps(nanRows,:) = [];
        
        
        disp('Classifying saddle point');
        %first check angular velocity
        [wIsMinimum, omega, xc_t] = sp_heading(ps, gs, plot);
        omega = omega*wmax;
%         if wIsMinimum
            %Check linear velocity only if omega is at minimum
            [~, actionSamples] = bestActionForGradientSamples3D(ps, gs);
            [vIsMinimum, v, xc_v] = sp_velocity_rev(ps, actionSamples, gs, plot);
            v=v*vmax;
%         else
%              v = 0;
%         end
     
        %Check whether v and w are still zero after classification
        if wIsMinimum && vIsMinimum %w and v appear to be at minimum 
            if xc_t == 0 && xc_v == 0 %Both lines are horizontal --> all particles agree
                disp('goal reached');
                set(toggleBtn, 'String', 'Start', 'Value', 0);
                start = 0;
            else
                attemptLocalization(amclSvc);
            end
        end
        searchDir = [v, omega];
        
    end
    action = searchDir;
end


end
function attemptLocalization(amclService)
disp('Calling nomotion_update');
amclResp = call(amclService.client,amclService.req,'Timeout',1); 
amclResp = call(amclService.client,amclService.req,'Timeout',1);
end
