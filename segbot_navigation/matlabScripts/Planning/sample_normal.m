function [ random_sample ] = sample_normal( sigma, mu)
%sample_normal Function to sample from a normal distribution with mean mu and
%standard deviation sigma.
%Input Arguments:
%   sigma: Scalar. Variance of the normal distribution.
%   mu: Scalar. Mean of the normal distribution, if not provided mean will
%   be 0. 
% Output Arguments:
%  random_sample:  Scalar. Value of a random sample from a normal
%  distribution with mean mu and std deviation sigma. 

if nargin < 2
    mu = 0;
end

random_sample = sigma * randn + mu;


end

