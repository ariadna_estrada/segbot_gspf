function [ps, vf_samples] = valueFunctionSamples2D(particle_set, grid_info, mttr, remove_nans)
% Return samples of valid gradient and its corresponding particle.
% 
% Input:
% 	- particle_set: mx2 real matrix where each row is a particle [x y]. 
%	- grid_info: 1x1 struct. grid_info.xs is an nxn grid where the value
%       function was computed. See ttr_2d.m for more details.
%	- grad: 2x1 cell array. grad{1} and grad{2} contain the gradient w.r.t. x
%       and y respectively (each nxn grid is flattened into a vector).
%   -remove_nans: boolean. Indicator to remove NaN values. 
%
% Output:
%   - vf_samples: mx2 interpolated gradient samples at the particle
%       locations with invalid gradients eliminated. Row k is the gradient
%       at the k-th particle in particle_set.
%   - ps : mx2 particle set with corresponding invalid gradient samples
%       eliminated (or not).
%
% Ariadna Estrada, 2017/12/04
% 

% Interpolate on the particle set.
vf_samples = interpn(grid_info.xs{:}, mttr, particle_set(:,1), particle_set(:,2));

if remove_nans
    valids = ~any(isnan(vf_samples),2);
    ps = particle_set(valids,:);
    vf_samples = vf_samples(valids,:);
else
    ps = particle_set;
end
end