function [searchDir] = bestGradientForGradientSamples2D(gradientSamples, plots)
% Return the consensus search direction among the given gradients.
% 
% Input:
%   - gradientSamples: mx2 gradient samples at the particle locations.
%   - plots: A 2-vector of scatter plot handles: the first element is for
%       plotting gradient samples, the second handle is for plotting the search 
%       direction (see gradient_plots.m).
%
% Output:
%   - searchDir: 2x1 unit vector giving the direction to travel.
%
% Neil Traft, 2015/04/04
%

% Constants to determine whether the direction vector is "zero".
relative_zero = 0.01;
% Picked this absolute constant because the value function's should be
% producing gradients that are close to magnitude 1.
absolute_zero = 0.01;

% Remove NaNs from gradientSamples.
gradientSamples(isnan(gradientSamples(:,1)),:) = [];
gradientSamples(isnan(gradientSamples(:,2)),:) = [];

% In case of error.
if any(~isfinite(gradientSamples(:))) || ~isreal(gradientSamples)
    warning('In bestGradientForGradientSamples2D: gradient is either nan or complex and will be included in SR1 update.');
end

if isempty(gradientSamples)
    warning('In bestGradientForGradientSamples2D: gradientSamples is empty.');
    searchDir = [NaN;NaN];
	return;
end

% Get search direction from the gradient samples.
% [~,g] = consensusGradient(gradientSamples');
%Use gurobi instead of matlab's quadprog
[~,g] = gurobi_cssgrad(gradientSamples');

% Determine if g is close enough to a zero gradient or not.
% One method: if norm(g) < relative_zero*mean_magnitude + absolute_zero.
% Some planners may give very small magnitude gradients, which are only meant
% to indicate direction, not velocity. To deal with this, we can instead make
% sure we are smallest than the minimum gradient:
% if norm(g) < absolute_zero && norm(g) < min_magnitude
mags = sqrt(gradientSamples(:,1).^2 + gradientSamples(:,2).^2);
mean_magnitude = mean(mags);

if norm(g) < relative_zero*mean_magnitude + absolute_zero
    searchDir = zeros(size(g));
%     if any(mags <= absolute_zero)
%         disp('There is a 0 gradient in the samples.');
%     end
else 
    searchDir = g/norm(g);
end

% Plot the gradients on a scatter plot.
if nargin > 1 && ~isempty(plots)
    C = unique(gradientSamples, 'rows');
    x = -C(:,1);
    y = -C(:,2);
    if (size(x,1) > 2)
        try
            K = convhull(x,y);
        catch ME
            %warning(ME.message);
            K = [];
        end
    else
        K = [];
    end
    set(plots(1), 'xdata', x, 'ydata', y);
    set(plots(2), 'xdata', x(K), 'ydata', y(K));
    set(plots(3), 'xdata', g(1), 'ydata', g(2));
    
    origins = zeros(size(x,1),1);
    set(plots(4), 'xdata', origins, 'ydata', origins);
    set(plots(4), 'udata', x, 'vdata', y);
    set(plots(5), 'xdata', x(K), 'ydata', y(K));
    set(plots(6), 'position', [0,0,g(1), g(2)]);
%     set(plots(7), 'position', [0,0, 0, 0]);
    set(plots(8), 'position', [0,0, 0, 0]);
end

end