function [ r ] = oneNormFit( x, y )
% ONENORMFIT Find the best fit for Ax = y with norm 1 error matrix
%   Convert the problem into a linear program to solve
%
% INPUTS: 
%    - x: nxn matrix, A
%    - y: nx1 vector, y
% OUTPUTS:
%    - r: nx1 vector, x
n = size(x,1);
% Initialize the b's
b = zeros(2*n,1);
beq = y(:);

% Set up the matrix
Z = zeros(n,n);
I = eye(n);
A = [Z,I,-I;Z,-I,-I];
Aeq = [x, I, zeros(n,n)];

% the constant vector for the objective function
c = [zeros(2*n,1);ones(n,1)];

[r,~] = linprog(c,A,b,Aeq,beq);
end

