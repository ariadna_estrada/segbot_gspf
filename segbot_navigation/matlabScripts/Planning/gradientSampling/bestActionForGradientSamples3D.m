function [a, actionSamples] = bestActionForGradientSamples3D(x, g)
% bestActionForGradientSamples3D: Computes the search direction by
% (separately) minimizing the convex combination of robot actions over linear
% velocity v and angular velocity omega. A robot's action maps to its velocity
% in the state space as:
%
%   \dot x      = v \cos \theta
%   \dot y      = v \sin \theta
%   \dot \theta = \omega
%
% And we seek to minimize $p^T\dot x$ over the inputs $v,\omega$, for each
% particle. This becomes
% 
%   a^{(k)} = \min_v v(p^{(k)}_1\cos\theta + p^{(k)}_2\sin\theta) + min_{\omega} \omega p^{(k)}_3,
%   s.t. |v| \leq 1, |\omega| \leq 1
% 
% where $p^{(k)}$ is the gradient at the particle location. We then find the
% consensus action for all particles,
% 
%   \min_{c} || \conv \{ a^{(1)}, \dots, a^{(m)} \} ||
% 
% where $c$ are the coefficients of the convex combination. The result of these
% two minimizations is the output of this function.
%
% INPUT:
%   x: mx3 matrix, the state [x y theta] of each sample.
%   g: mx3 matrix, each row is a gradient sample.
%
% OUTPUT:
%   a: 2x1, consensus action [v; omega].
%   actionSamples: mx2 matrix, each row is an action sample.
%

% For each particle, find the optimal action.
theta = x(:,3);
v = -sign(g(:,1).*cos(theta) + g(:,2).*sin(theta));
w = -sign(g(:,3));
actionSamples = [v, w];

% Now derive a consensus action for all particles.
a = [minimize1D(v), minimize1D(w)];

end

function [sol] = minimize1D(x)

xmin = min(x);
xmax = max(x);

if xmin<=0 && xmax>=0
    sol = 0;
elseif xmax<=0
    sol = xmax;
else
    sol = xmin;
end

end