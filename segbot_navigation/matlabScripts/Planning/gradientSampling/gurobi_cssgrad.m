function [ c, d ] = gurobi_cssgrad( g )
%GUROBI_CSSGRAD Use Gurobi optimization toolbox to find the consensus
%direction
%   Look at the function consensusGradient to find the quadratic optimization detail 
%
% INPUT: 
%    - g: 2xm matrix, each column is a gradient sample
%
% OUTPUT:
%    - c: mx1, solution to qp
%    - d: 2x1, approximated descent direction
%

clear model;
[~,n] = size(g);

model.Q = sparse(g'*g);
model.A = sparse(ones(1,n));
model.obj = zeros(1,n);
model.rhs = 1;
model.sense = '=';
params.outputflag = 0;
results = gurobi(model,params);
c = results.x;
d = -g*c;

end
