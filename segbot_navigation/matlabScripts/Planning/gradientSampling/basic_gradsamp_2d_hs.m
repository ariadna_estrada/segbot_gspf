function [searchDir, gradientSamples] = basic_gradsamp_2d_hs(particle_set, grid_info, grad)
% Return the search direction found by gradient sampling half-space method, and the
% samples of the gradient at each particle.
% Find possible search direction by solving for direction closest to productive
% half-spaces
% Input:
% 	- particle_set: mx2 real matrix where each row is a particle [x y]. 
%	- grid_info: 1x1 struct. grid_info.xs is an nxn grid where the value
%     function was computed. See ttr_2d.m for more details.
%	- grad: 2x1 cell array. grad{1} and grad{2} contain the gradient w.r.t. x
%     and y respectively (each nxn grid is flattened into a vector).
%
% Output:
%   - searchDir: 2x1 unit vector giving the direction to travel. Returns
%   NaN if no travel direction can be found, which may occur if no valid
%   gradient samples can be computed or if a local minimum is encountered.
%   - gradientSamples: mx2 interpolated gradient samples at the particle
%   locations. Row k is the gradient at the k-th particle in particle_set.
%

% gsamp is an mx2 real matrix of the gradients at each particle, with NaNs removed.
[~, gradientSamples] = gradientSamples2D(particle_set, grid_info, grad); 

% In case of error.
if any(~isfinite(gradientSamples(:))) || ~isreal(gradientSamples)
    warning('In bestGradientForGradientSamples2D: gradient is either nan or complex and will be included in SR1 update.');
end

if isempty(gradientSamples)
    warning('In bestGradientForGradientSamples2D: gradientSamples is empty.');
    searchDir = [NaN;NaN];
	return;
end

% Get search direction from the gradient samples.
searchDir = bestAction2D(gradientSamples);

end
