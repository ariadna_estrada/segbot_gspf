function [a] = bestActionForDirection(p, x, vmax, wmax)
% bestActionForDirection: Computes the search direction by voting on which 
% input would best match the given descent direction (p), for all particles. A 
% robot's action maps to its velocity in the state space as:
%
%   \dot x      = v \cos \theta
%   \dot y      = v \sin \theta
%   \dot \theta = \omega
%
% And we seek to minimize $p^T\dot x$ over the inputs $v,\omega$. This becomes
% 
%   \min_v v(p_1\cos\theta + p_2\sin\theta) + min_{\omega} \omega p_3
% 
% The result of these two minimizations is the output of this function.
%
% INPUT:
%   p: 3x1, approximated descent direction, normalized.
%   x: mx3 matrix, the state [x y theta] of each sample.
%   vmax: scalar, the maximum linear velocity (v) of the robot.
%   wmax: scalar, the maximum angular velocity (omega) of the robot.
%
% OUTPUT:
%   a: 2x1, consensus action [v; omega].
%

theta = x(:,3);
%vdir = sign(sum([cos(theta), sin(theta)] * p(1:2)));
%wdir = sign(p(3));
%a = [vmax*vdir; wmax*wdir];

% Let's try not clamping to bang-bang controls...
v = mean([cos(theta), sin(theta)] * p(1:2));
w = p(3);
a = [v; w];

end


function [sol] = minimize1D(x)

xmin = min(x);
xmax = max(x);

if xmin<=0 && xmax>=0
    sol = 0;
elseif xmax<=0
    sol = xmax;
else
    sol = xmin;
end

end