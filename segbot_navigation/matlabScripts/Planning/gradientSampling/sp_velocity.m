function [ n, v, check ] = sp_velocity( ps, actionSamples )
% SP_VELOCITY Classify the stationary points according to linear velocity
%   Classify the stationary point according to the sign of a - coefficient 
%   of non linear best fit.
%
% INPUTS:
%     - ps: mx3 matrix, each row is a particle [x,y,heading]
%     - actionSamples: mx2 matrix, each row is a action sample [v, omega]
% OUTPUTS:
%     - n: integer indicates the type of stationary point
%          1 - local minimum, 2 - local maximum
%     - v: consensus linear velocity
%     - check: check the reason of large number of iterations if check is
%     1, (randomization error or algorithm is not good)
%
% keyboard
m = size(ps,1);
% find the mean of particle locations
mean_x = mean(ps,1);
mean_x = mean_x(1:2);
new_xh = mean_x;
xh = 0;
relative_zero = 0.03;
mean_nbf = NaN;
% we need to count the number of iterations
k = 0;
check = 0;
while true  
  xh = xh + new_xh;
  s = [ (ps(:,1)-xh(1)).*cos(ps(:,3))+(ps(:,2)-xh(2)).*sin(ps(:,3)), ones(m,1) ];
  % solve the non linear best fit on the function of action sample(with an 
  % opposite sign of the real action) on each particle.
  nbf = s \ -actionSamples(:,1);
  if isnan(mean_nbf)
      % On the first iteration where xh is the mean of the particle cloud,
      % store the nbf in case we need it later.
      mean_nbf = nbf;
  end
  if abs(nbf(1)) <= relative_zero
      % Slope / Hessian is zero, so we cannot adjust the center any
      % further.  However, zero Hessian means we are not at a maximum so we
      % can just stop iterative fitting.
      % plot(s(:,1),actionSamples(:,1),'*')
      % figure
      % quiver(ps(:,1), ps(:,2), cos(ps(:,3)), sin(ps(:,3)))
      check = 2;
      
      break;  
   end
  
  % compute the new approximation of xh.
  A = [cos(ps(:,3)),sin(ps(:,3))];
  new_xh = A \ ((-nbf(2)/nbf(1))*ones(m,1));
  new_xh = new_xh';
      
  k = k + 1;
  % repeat until convergence
  if abs(new_xh) < 1e-4
      break;
  end
  
  if k > 50
      check = 1;
      break;
  end
end

a = nbf(1);
% if a > 0, local minimum, set linear velocity be 0
if a > 0 || abs(a) <= relative_zero
    v = 0;
    n = 1;
% if a < 0, local maximum, use voting procedure to find v.
else if a < 0
      v = voteVel(-actionSamples);
      n = 2;
    end
end


end





