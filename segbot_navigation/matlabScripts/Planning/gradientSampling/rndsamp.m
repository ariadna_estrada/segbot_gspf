function [ xsamp ] = rndsamp( x , rad)
% rndsamp: Randomly sample a point in a ball with radius rad, centered around x.
%
% INPUT:
%   x: vector, point in which to sample around.
%   rad: Number larger than 0, radius of the circular sampling area.
%
% OUTPUT:
%   xsamp: vector, sampled point, same dimensions as x.
%
sz = size(x);
while 1
    % Pick random vector with components that are maximum 'rad' away from x.
    xtemp = x + rad*(2*rand(sz) - 1);
    if norm(xtemp-x)<=rad % check if x is within the sampling circle with radius 'rad'
        xsamp = xtemp; % if so, return xsamp. else resample.
        break;
    end
end

end

