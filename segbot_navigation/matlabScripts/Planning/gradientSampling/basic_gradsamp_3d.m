function [css_action, gradientSamples,actionSamples, ps] = basic_gradsamp_3d(particle_set, grid_info, grad)
% Return the search direction and the samples of gradient at each particle
% INPUTS:
%    - particle_set: mx3 real matrix where each row is a particle(sample).
%    - grid_info: 1x1 struct. grid_info.xs is an nxn grid where the value
%      function was computed. See ttr.m for more details.
%	 - grad: 3x1 cell array. grad{1}, grad{2}, grad{3} contain the gradient
%      w.r.t. x, y, theta respectively (each nxn grid is flattened into a
%      vector).
%
% OUTPUTS:
%    - css_action: 2x1 unit vector [v;omega] giving the direction to travel. 
% 
%    - gradientSamples: nx3 interpolated gradient samples at the particle
%
%    - actionSamples: nx2 matrix, each row is an action sample.
%
%    - ps: nx3 matrix, the origin particle set, but eliminate the rows with NaN gradients. 

% Find the samples of gradient at each particle
[ps, gradientSamples] = gradientSamples3D(particle_set, grid_info, grad);

% In case of error.
% if any(~isfinite(gradientSamples(:))) || ~isreal(gradientSamples)
%     warning('In bestGradientForGradientSamples3D: gradient is either nan or complex and will be included in SR1 update.');
% end
% 
if isempty(gradientSamples)
    warning('In bestActionForGradientSamples3D: gradientSamples is empty.');
    css_action = [NaN;NaN;NaN];
    actionSamples = NaN;
	return;
end
% Get consensus action (both linear velocity and angular velocity) from the gradient samples.
[css_action,actionSamples] = bestActionForGradientSamples3D(ps, gradientSamples);

end
