function [ particle_set ] = sample_set(x, m)
%SAMPLE_SET Random samples in a crescent shape
%   Returns m uniformly random samples from the fake crescent shaped
%   distribution.
%   The given point is always included in the set.
%
% INPUT:
%	x: 1x2 vector, the current iterate x_k.
%	m: scalar, the number of samples to return
%
% OUTPUT:
%   particle_set: mx2 matrix, uniform sample points around x, sampled from the
%   given distribution. Each column is a sample point.
%

particle_set = zeros(m,2);
particle_set(1,:) = x;
for i = 2:m
    particle_set(i,:) = fakeDistribution(x,0.002,pi/150);
end

end
