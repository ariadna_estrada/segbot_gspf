function [A, B, x_c, iters] = quadApprox2D(particle_set, gradient_samples)
% Quadratic approximation to the value function
% 
% Returns a quadratic approximation to the value function in the neighborhood
% of the given set of particles. Equivalently, it fits a linear function to the
% gradient of the value function:
% 
% g = 2A(x - x_c) + B
%
% Input:
% 	- particle_set: nx2 real matrix where each row is a particle [x y].
%       Represented by x in the equation above.
%   - gradient_samples: mx2 interpolated gradient samples at the particle
%       locations. Row k is the gradient at the k-th particle in particle_set.
%       Represented by g in the equation above. Should not contain NaNs.
%
% Output:
%	- A: 2x2 matrix as in the equation above.
%	- B: bias as in the equation above. This should be very close to 0.
%	- x_c: 2x1 vector, the center of curvature of the quadratic.
%   - iters: number of iterations needed to achieve convergence.
%
% Neil Traft, 2015/03/09
%

psize = size(particle_set);
gsize = size(gradient_samples);
if ~isequal(psize, gsize)
    error('quadraticApprox: Particle set (%d, %d) differs in size from gradient set (%d, %d).',...
            psize, gsize);
end
if psize(2) ~= 2
    error('quadApprox2D: Only 2D datasets are supported.');
end

abs_thresh = 1e-8;
max_iters = 10;

m = psize(1);
g = gradient_samples';
g = g(:);

L = nan(2*m, 5);
col0 = zeros(m,1);
col1 = ones(m,1);
B = ones(2,1);
iters = 0;

% B should be close to 0 when we converge.
while iters < max_iters && all(abs(B) > abs_thresh)
    iters = iters+1;
    if iters==1
        % Initialize center to the mean of particles.
        x_c = mean(particle_set)';
    else
        x_c = x_c - 0.5*inv(A)*B; %#ok<MINV>
    end
    
    % Form L: alternate every other row.
    % Even rows are the equations for gradients in the x direction.
    % Odd rows are the equations for gradients in the y direction.
    y = bsxfun(@minus, particle_set, x_c');
    L(1:2:end,:) = [2*y col0 col1 col0];
    L(2:2:end,:) = [col0 2*y col0 col1];
    
    % Uncomment if want to perform least squares.
%     r = [a11 a12 a22 b1 b2]
    r = L \ g;
    % Perform the norm 1 best fit
%      r = oneNormFit(L'*L,L'*g);
    
    % Extract the answer.
    A = zeros(2,2);
    A(1,:) = r(1:2);
    A(2,:) = r(2:3);
    B = r(4:5);
end

end