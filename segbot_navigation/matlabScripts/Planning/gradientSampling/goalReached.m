function [ reached ] = goalReached(particle_set, x_c, lambda)
%GOALREACHED Return 0 or 1 according to whether the robot is at the goal.
%
% When navigating on an implicit value function using only gradients, we
% create a quadratic approximation to the value function and wonder whether we
% have reached the minimum. This function answers that question.
%   
% INPUT:
%   particle_set: nx2 real matrix where each row is a particle [x y].
%   x_c: Vector, the center of curvature of the quadratic.
%   lambda: Vector, the eigenvalues of the Hessian of the quadratic.
%
% OUTPUT:
%   reached: 1 if the goal has been reached, 0 otherwise.
%
% Neil Traft, 2016/08/22

% If we have a negative eigenvalue, then we're definitely not finished yet.
reached = all(lambda >= 0);
if reached
    % Check to see if the center of the quadratic is inside our particle cloud.
    center = mean(particle_set);
    center_dist = norm(center' - x_c);
    particle_dists = bsxfun(@minus, particle_set, center);
    particle_cloud_radius = max(sqrt(sum(particle_dists.^2,2)));
    reached = (center_dist < particle_cloud_radius);
end

end
