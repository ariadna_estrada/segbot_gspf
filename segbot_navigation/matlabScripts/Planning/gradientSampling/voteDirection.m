function [ omega ] = voteDirection(x_omega)
% VOTEDIRECTION When reaches local maximum point, vote a consensus angular
% velocity
%    
% INPUT:
%    - x_omega: mx2 matrix [heading,sign of gradient samples]
%
% OUTPUT:
%    - omega: 1 or -1, new angular velocity
% 
% sumL = nnz(x_omega(:,2)==1);
% sumR = nnz(x_omega(:,2)==-1);
% if sumL>sumR
%     omega = 1;
% else omega = -1;

    
sumL = sum(x_omega(:,2)== 1);
sumR = sum(x_omega(:,2)==-1);
sumZ = sum(x_omega(:,2)== 0);

[~, c] = max([sumL sumR sumZ]);
switch c
    case 1
        omega = 1;
    case 2
        omega = -1;
    case 3
        omega = 0;
        disp('consensus for omega is 0');
end
    
    
end

