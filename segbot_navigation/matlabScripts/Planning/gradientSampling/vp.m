function [ direction ] = vp( matrix, gradient_samps, evectors, evalues)
%VOTING_PROCEDURE Pick descent direction when no consensus is available.
%
% If there is only one negative eigenvalue(which must be the case of saddle
% point in 2D), goes toward the direction which the majority of the
% particles prefer. If there are more than one negative eigenvalues, use
% the above voting procedure on the eigenvector associated with the most
% negative eigenvector.
%
%INPUTS:
%    matrix: 2x2 Hessian matrix of value function, if the eigenvectors of
%    matrix are not all positive, use voting procedure to find the
%    direction to travel.
%
%    gradient_samps: mx2 matrix, each column is a gradient sample.
%
%    evectors: 2x2 matrix containing the eigenvectors of the Hessian.
%    Optional.
%
%    evalues: 2x1 vector containing the eigenvalues of the
%    Hessian.  Optional.  It is assumed that one of the eigenvalues is
%    negative.
%
%OUTPUTS:
%    direction: a 1x2 vector gives the direction to travel

    if(nargin < 4)
      [evectors, evalues] = eig(matrix);
      evalues = diag(evalues);
    end

    % If there are two negative eigenvalues (a local maximum) for now we will
    % just vote on the most negative.
    [ ~, evalue_index ] = min(evalues);

    descent_evector = evectors(:, evalue_index);
    
    inner_product = gradient_samps * -descent_evector;
    votes = sign(inner_product);
    direction_multiplier = sum(votes);
    consensus_dir = sign(direction_multiplier);
    if consensus_dir == 0
        direction = descent_evector;
    else direction = sign(direction_multiplier) * descent_evector;
    end

end


