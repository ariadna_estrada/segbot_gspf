function [ v ] = voteVel( actionSamples )
% VOTEVEL compute the consensus linear velocity by voting procedure
%   Detailed explanation goes here
% INPUTS:
%    - actionSamples: mx2 matrix, each row is an action sample
% OUTPUT:
%    - v : linear velocity

% goForward = nnz(sign(actionSamples(:,1)) == 1);
% goBackward = nnz(sign(actionSamples(:,1)) == -1);
% if goForward > goBackward
%     v = 1;
% else v = -1;
% end


goForward = sum(sign(actionSamples(:,1)) == 1);
goBackward = sum(sign(actionSamples(:,1)) == -1);
stay = sum(sign(actionSamples(:,1)) == 0);

[~,c] = max([goForward goBackward stay]);
switch c
    case 1
        v = 1;
    case 2
        v = -1;
    case 3
        v = 0;
        disp('consensus v is 0');
%         if goF > goB, disp('otw goForward'); end;
end

end

