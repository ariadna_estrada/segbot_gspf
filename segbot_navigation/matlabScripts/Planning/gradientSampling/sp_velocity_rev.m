function [ n, v, x_cut ] = sp_velocity_rev( ps, actionSamples, gs,  plot )
% SP_VELOCITY_REV Classify the stationary points according to linear
% velocity
%   Classify the stationary point according to the sign of a - coefficient 
%   of non linear best fit.
%
% INPUTS:
%     - ps: mx3 matrix, each row is a particle [x,y,heading]
%     - actionSamples: mx2 matrix, each row is a action sample [v, omega]
% OUTPUTS:
%     - n: integer indicates the type of stationary point
%          1 - local minimum, 0 - local maximum
%     - v: consensus linear velocity
%     - x_cut: value where line corresponding to non linear best fit slope
%     crosses the x axis. 

m = size(ps,1);
% find the mean of particle locations
mean_x = mean(ps(1:2),1);
xh = mean_x;
relative_zero = 0.04;

% s = [ (ps(:,1)-xh(1)).*cos(ps(:,3))+(ps(:,2)-xh(2)).*sin(ps(:,3)), ones(m,1) ];
s = [ (ps(:,1)).*cos(ps(:,3))+(ps(:,2)).*sin(ps(:,3)), ones(m,1) ];
% solve the non linear best fit on the function of action sample(with an 
% opposite sign of the real action) on each particle.

nbf = s \ actionSamples(:,1);

a = nbf(1);
if a == 0 %Slope is 0 - horizontal line, no xcut
    x_cut = 0;
else
    x_cut = -nbf(2) / nbf(1); % 0 = mx + b --> x = -b/m 
end


if a >= 0 || abs(a) < relative_zero
    disp('voting velocity');
    v = voteVel(actionSamples);
    n = 0;
    if v == 0 %consensus was to stay in place
        n = 1;
    end
else
    v = 0;
    n = 1;
    disp('found v minimum');
end


if nargin > 2 && ~isempty(plot)
    ygs = gs(:,1).*cos(ps(:,3)) + gs(:,2).*sin(ps(:,3));
    set(plot(7), 'xdata', s(:,1), 'ydata', -ygs);
    set(plot(5), 'xdata', s(:,1), 'ydata', actionSamples(:,1));
   
    xs = [-10, 10];
    ys = nbf(1) * xs + nbf(2);
    set(plot(6), 'xdata', xs, 'ydata', ys);
    ax = get(plot(5), 'Parent');
    set(ax, 'XLim', [min(s(:,1))-0.01, max(s(:,1))+0.01]);
end


end





