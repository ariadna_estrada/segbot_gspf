function [ particle_set ] = sample_set_circle(x,m,radius )
%   SAMPLE_SET_CIRCLE Generate a circular particle cloud to find the
%   particle position
% 
% INPUT:
%    - x: 1x3 vector, initial position of the particle
%    - m: scalar, the number of samples to return
%    - radius: the radius within which to sample
%
% OUTPUT:
%    - particle_set: mx3 matrix, uniform sample points around x, sampled around a
%    sphere. Each row is a sample point.
%
particle_set = zeros(m,3);
particle_set(1,:) = x;
for i = 2:m
    particle_set(i,:) = rndsamp(x,radius);
    % Boundary check
    if particle_set(i,3) < 0
            particle_set(i,3) = particle_set(i,3) + 2*pi;
        else if particle_set(i,3) > 2*pi
                particle_set(i,3) = particle_set(i,3) - 2*pi;
            end
    end
end
    
end

