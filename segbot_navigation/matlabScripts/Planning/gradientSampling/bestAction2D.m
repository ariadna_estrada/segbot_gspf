function [ searchDir ] = bestAction2D( gradientSamples )
%BESTACTION2D GENERATE ALL THE POSSIBLE ACTION FOR ALL THE PARTICLES
%   We assume the polygon is a circle with unit radius. 
% INPUT:
%    - gradientSamples: mx2 interpolated gradient samples at the particel
%    locations
% OUTPUT:
%    - searchDir: a possible search direction

% find the angles according to each gradient samples
m = size(gradientSamples, 1);
i = 1:m;
[grad(i),~] = cart2pol(gradientSamples(i,1),gradientSamples(i,2));
if grad(i) < 0
    grad(i) = grad(i) + 2*pi;
end
grad = grad';
% Set up linear program problem
f = [0, ones(1,2*m)];
% Construct A
c1 = [ones(m,1);-ones(m,1);zeros(m,1)];
c2 = [zeros(m,m);zeros(m,m);-eye(m)];
c3 = [-eye(m);-eye(m);-eye(m)];
A = [c1,c2,c3];
b = [grad;-grad;-pi/2*ones(m,1)];
lb = zeros(1,2*m+1);
% we measure the line segment by the angle between this line segment and the
% positive x_axis, so the value range of it is from 0 to a number close to
% 2*pi. We want to avoid the big jump so the upper bound is the largest
% number we can represent that is smaller than 2*pi.
ub = [(1-eps)*2*pi,inf*ones(1,2*m)];
result = linprog(f,A,b,[],[],lb,ub);
angle = result(1);
%find the search direction
%represent it in polar coordinate and convert back to cartesian
[x,y] = pol2cart(angle,1);
searchDir = [-x;-y];
end

