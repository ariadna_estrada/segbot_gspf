function [ n, omega, x_cut ] = sp_heading( ps,gs, plot )
%STATIONARY_POINT_HEADING Classify and handle the path around the
%stationary point.
%   
%INPUT:
%   - ps: mx3 matrix, the particle cloud 
%   - gs: gradient samples correspond to the particle cloud
%
%OUTPUTS:
%   - n: integer, 1 represents a local minimum point and 0 represents a
%   local maximum point.
%   - omega: angular velocity, if the point is a local minimum, keep omega
%   to be 0(do not change travel direction), if the point is a local
%   maximum, use voting procedure to find the direction of omega, and
%   travel along that direction in the following step.

% Remove NaNs from gs and ps
nanRows = any(isnan(gs),2);
gs(nanRows,:) = [];
ps(nanRows,:) = [];

% generate a mx2 matrix, the first column is the heading of the particle
% and the second column is the gradient samples of the particle.
x = [ ps(:,3), gs(:,3) ];
% sort x in ascending heading order
x_sort = sortrows(x,1);
m = size(x_sort,1);
%check whether the particle cloud contains the cut(boundary of 0 and 2pi)
theta = x_sort(:,1);
dtheta = diff(theta);
dtheta = [dtheta; x_sort(1,1)+2*pi-x_sort(m,1)];
[ dtheta_gap, dtheta_loc ] = max(dtheta);
if dtheta_gap ~= dtheta(m)
    % if the particle cloud contains the cut, rotate it in anticlockwise
    % direction to make sure it does not contain the cut.  That amount is
    % determined by the heading of the particle on the larger side of the
    % largest gap.
    delta = 2*pi - theta(dtheta_loc+1);
    theta_shifted = mod(theta + delta, 2*pi);
    
    x_sort = [theta_shifted, x_sort(:,2)];
 end

x_omega = [ x_sort(:,1), -sign(x_sort(:,2)) ];
% Classification of stationary point, if the quadratic fit(y = ax^2+bx+c)
% has negative or zero a value, it is a local minimum, if it has a positive
% a value, then it is a local maximum. Note the procedure is opposite from
% the 2d Hessian classfication because for the 2d case, we use the gradient
% space and for 3d case, we use the action space.
a = polyfit(x_omega(:,1),x_omega(:,2),1);

if a(1) < 0 
    omega = 0;
    n = 1;
else
%     disp('voting omega');
    omega = voteDirection(x_omega);
    n = 0;
    if omega == 0
        disp('omega consensus was 0');
        n = 1;
    end
end

if a(1) == 0 %Slope is 0 - horizontal line, no xcut
    x_cut = 0;
else
    x_cut = -a(2) / a(1); % 0 = mx + b --> x = -b/m 
end

if nargin > 2 && ~isempty(plot)
    set(plot(2), 'xdata', x_omega(:,1), 'ydata', x_omega(:,2));
    set(plot(1), 'xdata', x(:,1), 'ydata', -x(:,2));
    xs = [-0.1,2.1] * pi;
    ys = a(1) * xs + a(2);
    set(plot(3), 'xdata', xs, 'ydata', ys);
    ax = get(plot(1), 'Parent');
    set(ax, 'XLim', [min(x_omega(:,1))-0.01, max(x_omega(:,1))+0.01]);
end
end