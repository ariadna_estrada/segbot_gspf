function [ reached ] = atGoal( gradient_samples, particle_set )
%atGoal Return 0 or 1 according to whether the robot is at the goal.
%
% When navigating on an implicit value function using only gradients, we
% create a quadratic approximation to the value function and wonder whether we
% have reached the minimum. This function answers that question.
%   
% INPUT:
%   particle_set: nx2 real matrix where each row is a particle [x y].
%   gradient_samples:
%
% OUTPUT:
%   reached: 1 if the goal has been reached, 0 otherwise.
%
% Neil Traft, 2016/08/22
% Modified by Ariadna Estrada, 2017/07/11

[~, psize] = size(particle_set);
if psize == 2; holonomic = 1; else holonomic = 0; end;

valids = ~any(isnan(gradient_samples),2);
valid_particles = particle_set(valids,:);
valid_gradients = gradient_samples(valids,:);

if (holonomic) 
    [A, B, x_c, iters] = quadApprox2D(valid_particles, valid_gradients);
else
    [A, B, x_c, iters] = quadApprox3D(valid_particles, valid_gradients);
end

[V, D] = eig(A);
lambda = diag(D);
    
% If we have a negative eigenvalue, then we're definitely not finished yet.
reached = all(lambda >= 0);
if reached
    % Check to see if the center of the quadratic is inside our particle cloud.
    center = mean(valid_particles);
    center_dist = norm(center' - x_c);
    particle_dists = bsxfun(@minus, valid_particles, center);
    particle_cloud_radius = max(sqrt(sum(particle_dists.^2,2)));
    reached = (center_dist < particle_cloud_radius);
end
end

