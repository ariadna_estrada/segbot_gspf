function [ c, d ] = consensusGradient(g, tol)
% consensusGradient: Computes the search direction by solving a 
% quadratic program that minimizes the convex combination 
% of gradients, c*g, where sum(c) = 1 and c >=0.
%
% INPUT:
%	g: dxm matrix, each column is a gradient sample.
%	tol: Tolerance for qp termination.
%
% OUTPUT:
%	c: mx1, solution to qp (weighting of convex combination).
%	d: dx1, approximated descent direction, unnormalized.
%

if nargin == 0
    error('Gradient samples must be provided.')
elseif nargin == 1
    tol = 1e-12;
end

[~,n] = size(g);
H = g'*g; 

% Instantiate variables
z = zeros(1,n);
Aeq = ones(1,n);
c0 = ones(n,1)/n; % starting point
% TODO: enable tolerance for quadprog?
opts = optimset('Algorithm','interior-point-convex','Display','off',...
    'TolX',tol,'TolFun',tol);
c = quadprog(H, z, [], [], Aeq, 1, z, [], c0, opts); % solve quadratic problem
d = -g*c;

end
