function [searchDir, gradient_samples] = bestActionForValueFunction3D(particle_set, grid_info, grad, XS, VS, plots)
% Return the consensus search direction found by gradient sampling, and the
% samples of the gradient at each particle.
% 
% Input:
% 	- particle_set: mx3 real matrix where each row is a particle (sample).
%	- grid_info: 1x1 struct. grid_info.xs is an nxn grid where the value
%       function was computed. See ttr.m for more details.
%	- grad: 3x1 cell array. grad{1}, grad{2}, grad{3} contain the gradient
%       w.r.t. x, y, theta respectively (each nxn grid is flattened into a
%       vector).
%   - plots: A 2-vector of scatter plot handles: the first element is for
%       plotting gradient samples, the second handle is for plotting the search 
%       direction (see gradient_plots.m).
%
% Output:
%   - searchDir: 2x1 unit vector giving linear and angular velocity [v; w].
%       Result is clamped s.t. |v| <= 1, |w| <= 1.
%   - gradient_samples: mx3 interpolated gradient samples at the particle
%       locations. Row k is the gradient at the k-th particle in particle_set.
%       May contain NaN.
%
% Neil Traft, 2015/03/20
%

% Constants to determine whether the direction vector is "zero".
relative_zero = 0.01;
% Picked this absolute constant because the value function should be
% producing gradients that are close to magnitude 1.
absolute_zero = 0.01;

% gsamp is an mx3 real matrix of the gradients at each particle, with NaNs removed.
if nargin < 5
    gsamp = gradientSamples3D(particle_set, grid_info, grad);
else
    gsamp = gradientSamples3D(particle_set, grid_info, grad, XS, VS);
end

gradient_samples = gsamp;

% Remove NaNs from gsamp.
nanRows = any(isnan(gsamp),2);
gsamp(nanRows,:) = [];
particle_set(nanRows,:) = [];

% In case of error.
if any(~isfinite(gsamp(:))) || ~isreal(gsamp)
    warning('In bestActionForValueFunction3D: gradient is either nan or complex and will be included in SR1 update.');
end

if isempty(gsamp)
    warning('In bestActionForValueFunction3D: all gradients are NaN.');
    searchDir = [NaN;NaN];
	return;
end

% Get search direction from the gradient samples.
[searchDir, actionSamples] = bestActionForGradientSamples3D(particle_set, gsamp);
a = -searchDir;

% Plot the gradients on a scatter plot.
if nargin > 1 && ~isempty(plots)
    C = unique(actionSamples, 'rows');
    x = -C(:,1);
    y = -C(:,2);
    if (size(x,1) > 2)
        try
            K = convhull(x,y);
        catch ME
            %warning(ME.message);
            K = [];
        end
    else
        K = [];
    end
    set(plots(1), 'xdata', x, 'ydata', y);
    set(plots(2), 'xdata', x(K), 'ydata', y(K));
    set(plots(3), 'xdata', a(1), 'ydata', a(2));
    
    origins = zeros(size(x,1),1);
    set(plots(4), 'xdata', origins, 'ydata', origins);
    set(plots(4), 'udata', x, 'vdata', y);
    set(plots(5), 'xdata', x(K), 'ydata', y(K));
    set(plots(6), 'position', [0,0, a(1), a(2)]);
end

end
