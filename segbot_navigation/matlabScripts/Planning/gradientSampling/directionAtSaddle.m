function [ eigenDir, x_c ] = directionAtSaddle(gradient_samples, particle_set)
%directionAtSaddle Find the search direction when at a saddle point
%   Detailed explanation goes here

vmax = 0.3;
wmax = 0.3;

[~, psize] = size(particle_set);
if psize == 2, holonomic = 1; else, holonomic = 0; end;

valids = ~any(isnan(gradient_samples),2);
valid_particles = particle_set(valids,:);
valid_gradients = gradient_samples(valids,:);

if (holonomic) 
    [A, B, x_c, iters] = quadApprox2D(valid_particles, valid_gradients);
else
    [A, B, x_c, iters] = quadApprox3D(valid_particles, valid_gradients);
end

[V, D] = eig(A);
lambda = diag(D);


all_neg_eigenvectors = V(:,lambda<=0);
if isempty(all_neg_eigenvectors)
    disp('No negative eigenvectors')
    eigenDir = [nan nan];
else
    v = all_neg_eigenvectors(:,1);
    if (holonomic)
    %Gradients vote on which direction along v we should travel.
        consensus = sign(sum(sign(valid_gradients*v)));
    % If it's a tie, then choose; otherwise, go in the direction of the NEGATIVE
    % gradient which won the majority vote.
        if consensus==0, eigenDir = v; else, eigenDir = -consensus*v; end;
            eigenDir = eigenDir / norm(eigenDir);
    else 
    %For now, just use the first descent vector.
        v = v/norm(v);
        eigenDir = bestActionForDirection(v, valid_particles, vmax, wmax);
    end
end


