function [searchDir, gradientSamples] = bestGradientForValueFunction2D(particle_set, grid_info, grad, plots)
% Return the consensus search direction found by gradient sampling, and the
% samples of the gradient at each particle.
% 
% Input:
% 	- particle_set: mx2 real matrix where each row is a particle [x y]. 
%	- grid_info: 1x1 struct. grid_info.xs is an nxn grid where the value
%       function was computed. See ttr_2d.m for more details.
%	- grad: 2x1 cell array. grad{1} and grad{2} contain the gradient w.r.t. x
%       and y respectively (each nxn grid is flattened into a vector).
%   - plots: A 2-vector of scatter plot handles: the first element is for
%       plotting gradient samples, the second handle is for plotting the search 
%       direction (see gradient_plots.m).
%
% Output:
%   - searchDir: 2x1 unit vector giving the direction to travel.
%   - gradientSamples: mx2 interpolated gradient samples at the particle
%       locations. Row k is the gradient at the k-th particle in particle_set.
%
% Carolyn Shen, 2014/08/20
% Neil Traft, 2015/08/27
%

% gsamp is an mx2 real matrix of the gradients at each particle, with NaNs removed.
gradientSamples = gradientSamples2D(particle_set, grid_info, grad);

searchDir = bestGradientForGradientSamples2D(gradientSamples, plots);

end
