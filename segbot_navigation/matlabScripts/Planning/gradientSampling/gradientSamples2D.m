function [ps, gradient_samples] = gradientSamples2D(particle_set, grid_info, grad)
% Return samples of valid gradient and its corresponding particle.
% 
% Input:
% 	- particle_set: mx2 real matrix where each row is a particle [x y]. 
%	- grid_info: 1x1 struct. grid_info.xs is an nxn grid where the value
%       function was computed. See ttr_2d.m for more details.
%	- grad: 2x1 cell array. grad{1} and grad{2} contain the gradient w.r.t. x
%       and y respectively (each nxn grid is flattened into a vector).
%
% Output:
%   - gradient_samples: mx2 interpolated gradient samples at the particle
%       locations with invalid gradients eliminated. Row k is the gradient
%       at the k-th particle in particle_set.
%   - ps : mx2 particle set with corresponding invalid gradient samples
%       eliminated.
%
% Neil Traft, 2015/08/18
% 

% Interpolate on the particle set.
gx = interpn(grid_info.xs{:}, grad{1,1}, particle_set(:,1), particle_set(:,2));
gy = interpn(grid_info.xs{:}, grad{2,1}, particle_set(:,1), particle_set(:,2));

gradient_samples = [gx gy];

valids = ~any(isnan(gradient_samples),2);
ps = particle_set(valids,:);
gradient_samples = gradient_samples(valids,:);
end

