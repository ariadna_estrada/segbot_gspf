function [ next_pose ] = nextPose(css_action,y0)
%NEXTPOSE Find the pose of the robot with the consensus action
%INPUTS:
%   - css_action: 2x1 unit vector [v;omega]
%   - y0: 1x3 vector, the initial pose of robot [x,y,h]
%OUTPUTS:
%   - next_pose:  1x3 vector [x,y,h]
%

v = css_action(1);
omega = css_action(2);
tspan = [0 0.15];
[~, next_pose] = ode23(@(t,x) odefcn(t,x,v,omega),tspan, y0);
m = size(next_pose,1);
next_pose = next_pose(m,:);
% Boundary check
if next_pose(3) < 0
            next_pose(3) = next_pose(3) + 2*pi;
        else if next_pose(3) > 2*pi
                next_pose(3) = next_pose(3) - 2*pi;
            end
end
end

function dxdt = odefcn(~,x,v,omega)
%ODEFCN Define the dynamics of robot movement
% x1'= vcos(x3)
% x2'= vsin(x3)
% x3'= omega
%INPUTS:
%   - x: 1x3 vector [x pos, y pos, heading]
%   - v: linear velocity
%   - omega: angular velocity

dxdt = [v*cos(x(3));v*sin(x(3));omega];

% dxdt(1)= v*cos(x(3));
% dxdt(2)= v*sin(x(3));
% dxdt(3)= omega;

end