function [searchDir, gradientSamples] = basic_gradsamp_2d(particle_set, grid_info, grad)
% Return the search direction found by gradient sampling, and the
% samples of the gradient at each particle.
% 
%     Return the consensus search direction found by gradient sampling if
%     it is not near any statinary points. Otherwise, identify the nature of
%     stationary point by checking the eigenvalues of Hessian matrix. Set
%     the searchDir to be NaN if reach the local minimum, otherwise using
%     the voting procedure to determine the searchDir near saddle point.
%     You can either choose use Matlab or Gurobi optimizer to find the
%     consensus action by changing 'which_optimizer'
% Input:
% 	- particle_set: mx2 real matrix where each row is a particle [x y]. 
%	- grid_info: 1x1 struct. grid_info.xs is an nxn grid where the value
%     function was computed. See ttr_2d.m for more details.
%	- grad: 2x1 cell array. grad{1} and grad{2} contain the gradient w.r.t. x
%     and y respectively (each nxn grid is flattened into a vector).
%
% Output:
%   - searchDir: 2x1 unit vector giving the direction to travel. Returns
%   NaN if no travel direction can be found, which may occur if no valid
%   gradient samples can be computed or if a local minimum is encountered.
%   - gradientSamples: mx2 interpolated gradient samples at the particle
%   locations. Row k is the gradient at the k-th particle in particle_set.
%
% Carolyn Shen, 2014/08/20
% Neil Traft, 2015/09/27
%

% Which optimizer should be used when solving for minimum norm in the convex hull?
% Ignored for other problems.  Current options are 'matlab' or 'gurobi'.
which_optimizer = 'gurobi';

% gsamp is an mx2 real matrix of the gradients at each particle, with NaNs removed.
[ps, gradientSamples] = gradientSamples2D(particle_set, grid_info, grad); 

% In case of error.
if any(~isfinite(gradientSamples(:))) || ~isreal(gradientSamples)
    warning('In bestGradientForGradientSamples2D: gradient is either nan or complex and will be included in SR1 update.');
end

if isempty(gradientSamples)
    warning('In bestGradientForGradientSamples2D: gradientSamples is empty.');
    searchDir = [NaN;NaN];
	return;
end

% Get search direction from the gradient samples.
switch(which_optimizer)
    case 'matlab'
        [~, searchDir] = consensusGradient(gradientSamples');
    case 'gurobi'
        [~, searchDir] = gurobi_cssgrad(gradientSamples');
    otherwise
        error('Unknown optimizer: %s', which_optimizer);
end
   
% Under what conditions is the descent direction vector considered to be
% zero.
too_small = 1e-3 * sqrt(median(sum(gradientSamples.^2,2)));
if norm(searchDir,2) <= too_small
    % If the descent direction is zero there is no consensus descent
    % direction -- we are near a stationary point.  In that case, test
    % the eigenvalues of the Hessian to see whether we are at a minimum
    % (the goal) or not.
    [A,~,~,~] = quadApprox2D(ps,-gradientSamples);
    [v,d] = eig(A);
    d = diag(d);
    if (min(d)>=0)
        % we are at a local minimum, so signal that no descent direction
        % exists. Reach the target.
        searchDir = NaN;
    else
        % If not at a minimum, choose a descent direction by voting
        % procedure.
        searchDir = vp(A,gradientSamples,v,d);
    end

end

end
