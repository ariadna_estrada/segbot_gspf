function [gradient_samples] = gradientSamples3D(particle_set, grid_info, grad, XS, VS)
% Return samples of the gradient at each particle.
% 
% Input:
% 	- particle_set: mx3 real matrix where each row is a particle (sample).
%	- grid_info: 1x1 struct. grid_info.xs is an nxn grid where the value
%       function was computed. See ttr.m for more details.
%	- grad: 3x1 cell array. grad{1}, grad{2}, grad{3} contain the gradient
%       w.r.t. x, y, theta respectively (each nxn grid is flattened into a
%       vector).
%
% Output:
%   - gradient_samples: mx3 interpolated gradient samples at the particle
%       locations. Row k is the gradient at the k-th particle in particle_set.
%       May contain NaN.
%
% Neil Traft, 2015/08/18
%

% Interpolate on the particle set. We need to extend the grid one more cell in
% the theta dimension to fully cover the [0,2pi] range. We replicate the values
% from the first cell into the last cell for a periodic boundary.
if nargin < 5
    X1 = cat(3, grid_info.xs{1}, grid_info.xs{1}(:,:,1));
    X2 = cat(3, grid_info.xs{2}, grid_info.xs{2}(:,:,1));
    X3 = cat(3, grid_info.xs{3}, ones(grid_info.shape(1:2))*2*pi);
    V1 = cat(3, grad{1}, grad{1}(:,:,1));
    V2 = cat(3, grad{2}, grad{2}(:,:,1));
    V3 = cat(3, grad{3}, grad{3}(:,:,1));
else
    X1 = XS.X1;   
    X2 = XS.X2; 
    X3 = XS.X3;
    V1 = VS.V1;
    V2 = VS.V2;
    V3 = VS.V3;
end

Q1 = particle_set(:,1);
Q2 = particle_set(:,2);
Q3 = particle_set(:,3);
gx = interpn(X1, X2, X3, V1, Q1, Q2, Q3);
gy = interpn(X1, X2, X3, V2, Q1, Q2, Q3);
ga = interpn(X1, X2, X3, V3, Q1, Q2, Q3);

gradient_samples = [gx gy ga];

end
