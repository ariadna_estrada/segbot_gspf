fig = figure('Name', 'Gradient Samples');
fig2 = figure('Name', 'Search Direction');
plots = gradient_plots(fig, fig2);


%Compute samples of the gradient at each particle.
gradientSamples = gradientSamples2D(particles, g, action);

%Subtract current joystick input
gradientSamples = gradientSamples - [joy_posx joy_posy];
gradientSamples = double(gradientSamples);

searchDir = bestGradientForGradientSamples2D(gradientSamples, plots);


% x = rand(20,1);
% y = rand(20,1);
% plot(x,y, '.');
% k = convhull(x,y)
% hold on, plot(x(k), y(k), '-r'), hold off