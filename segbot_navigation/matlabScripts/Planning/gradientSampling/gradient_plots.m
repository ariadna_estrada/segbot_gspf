function plots = gradient_plots( fig, fig2 )
% Create and return plots which can be used to display gradient samples.
% 
% Input:
%   - fig: The figure to plot a scatter plot.
%   - fig2: The figure to plot a quiver plot.
% Output:
%   - plots: A vector of plot handles.
% 

colors = get(groot, 'DefaultAxesColorOrder');
blue = colors(1,:);
red = colors(2,:);
yellow = colors(3,:);

ax = get(fig, 'CurrentAxes');
if isempty(ax)
    ax = axes('Parent', fig);
end
cla(ax);
hold(ax, 'on');
plot(ax, [-2,2], [0,0], 'k');
plot(ax, [0,0], [-2,2], 'k');
plots(1) = plot(ax, [0], [0], 'o', 'Color', yellow);
plots(2) = plot(ax, [0], [0], 'Color', blue);
plots(3) = plot(ax, [0], [0], 'p', 'MarkerEdgeColor', red, 'MarkerFaceColor', red, 'MarkerSize', 10);
axis(ax, [-1.5 1.5 -1.5 1.5]);

ax = get(fig2, 'CurrentAxes');
if isempty(ax)
    ax = axes('Parent', fig2);
end
cla(ax);
hold(ax, 'on');
plot(ax, [-2,2], [0,0], 'k');
plot(ax, [0,0], [-2,2], 'k');
plots(4) = quiver(ax, [], [], [], [], 0, 'Color', yellow);
plots(5) = plot(ax, [0], [0], 'Color', blue);
plots(6) = annotation(fig2, 'arrow');
set(plots(6), 'parent', ax, 'HeadLength', 5, 'HeadWidth', 3, 'HeadStyle', 'cback1', 'Color', red, 'LineWidth', 2);
plots(7) = annotation(fig2, 'arrow');
set(plots(7), 'parent', ax, 'HeadLength', 5, 'HeadWidth', 3, 'HeadStyle', 'cback1', 'Color', blue, 'LineWidth', 2);
plots(8) = annotation(fig2, 'textarrow');
set(plots(8), 'parent', ax, 'HeadLength', 5, 'HeadWidth', 3, 'HeadStyle', 'cback1', 'Color', 'g', 'LineWidth', 2);

axis(ax, [-1.5 1.5 -1.5 1.5]);

end