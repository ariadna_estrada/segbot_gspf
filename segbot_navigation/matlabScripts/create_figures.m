
fig2 = figure('Name', 'Chosing optimal omega');


ax = get(fig2, 'CurrentAxes');
if isempty(ax)
    ax = axes('Parent', fig2);
end
cla(ax);
hold(ax, 'on');

ax1 = ax;
hold(ax1, 'on');
xlabel('\theta^{[m]}',  'FontWeight','bold');
ylabel('p_{\theta}^{[m]}', 'FontWeight','bold');
axis([-0.2 2*pi+0.2 -1.1 1.1]);

plots(1) = plot(ax1, [0], [0], '.', 'MarkerSize', 10); %original theta values
plots(2) = plot(ax1, [0], [0], '.', 'MarkerSize', 10); %Shifted theta values
plots(3) = plot(ax1, [0], [0], 'Linewidth', 2); %Fitted line

sp_heading(ps, gs, plots);
title('Finding consensus in angular velocity');
% legend('p_\theta^{[m]}', '\omega^*', 'Location', 'southeast')
%%
fig2 = gcf;
set(fig2,'Units','Inches');
pos = get(fig2,'Position');
set(fig2,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3)-2, pos(4)]);
print(fig2, strcat('presentation/', 'ttt'),'-dpdf','-r0')

%%
colors = get(groot, 'DefaultAxesColorOrder');
blue = colors(1,:);
red = colors(2,:);
yellow = colors(3,:);
purple = colors(4,:);

fig1 = figure;
ax = axes('Parent', fig1);
% ax = show(map);
hold(ax, 'on');
% set(ax, 'XLim', [-pi, pi], 'YLim', [-pi, pi]);
plot(ax, [-pi,pi], [0,0], 'k');
plot(ax, [0,0], [-pi,pi], 'k');
set(ax, 'YAxisLocation', 'right');

theta = ps(:,3);
grad_theta = gs(:,3);
u = grad_theta .* cos(theta);
% u = ones(length(theta),1);
v = grad_theta .* sin(theta);
origin = zeros(length(u), 1);
% origin_x = ps(:,1);
% origin_y = ps(:,2);
ab = gca;
a = quiver(ax, origin, origin, u, v, 0, 'Color', yellow);
b = quiver(ax, origin, origin, actionSamples(:,2), actionSamples(:,2), 0)

set(a, 'MaxHeadSize', 0.05);
% K = convhull(u,v);
% plot(ax, u(K), v(K), 'Color', blue , 'LineWidth', 1)
%%
%% Load data
load('finite_wall_-2.0_0.0.mat');

zeros = find(abs(action{1})<1e-1 & abs(action{2})<1e-1);

%% Mesh Plot
clf; hold on;
mesh(g.xs{:}, mttr);
% sc = scatter3(g.xs{1}(zeros), g.xs{2}(zeros), mttr(zeros), 1, 'k', 'filled');
sc = scatter3([-2; 1.1], [0; 0], [0.1; 7.4], 500, 'k', '+');
scatter3([-2; 1.1], [0; 0], [0.1; 7.4], 50, 'k', 'filled');

%% Contour Plot
clf; hold on;

% contour(g.xs{:}, mttr, 50);
g3.xs{1,1} = g.xs{1,1}(:,:,1);
g3.xs{2,1} = g.xs{2,1}(:,:,1);
% [C1,H1] = contour3(g3.xs{:}, mttr(:,:,1), 100);
% [C2,H2] = contour3(g3.xs{:}, mttr(:,:,17), 100);
% [C3,H3] = contour3(g3.xs{:}, mttr(:,:,33), 100);

[C1] = surf(g3.xs{:}, mttr(:,:,1)); C1.LineStyle = 'none'
[C2] = surf(g3.xs{:}, mttr(:,:,14)); C2.LineStyle = 'none'
[C3] = surf(g3.xs{:}, mttr(:,:,26)); C3.LineStyle = 'none'
legend('\psi(\theta = 0)', '\psi(\theta = \pi/2)', '\psi(\theta = \pi)')
% [C] = surf(g3.xs{:}, mttr(:,:,33))
% contour(g2.xs{:}, m, 150);
% axis image
ax = gca;
gray = [0.9412 0.9412 0.9414];
ax.XColor = gray;
ax.YColor = gray;
ax.ZColor = gray;
% title({'cost'})
title({'Slice of the value function'; 'top view'})
%%
px = 0.2 .* randn(250,1) + 2.5;
py = 0.2 .* randn(250,1);
pz = 0.2 .* randn(250,1) + pi;
plot(px, py, '.', 'Color', purple);
%%
particle_set = [px, py, pz];

[searchDir, gradientSamples] = bestActionForValueFunction3D(particle_set, g, action,[]);

  dx = px;
  dy = py;
%   theta = gradientSamples(:,3);
  theta = pz;
%   v = gradientSamples(:,1).*cos(theta) + gradientSamples(:,2).*sin(theta);
  v = gradientSamples(:,3);
  vx = -v.*cos(theta);
  vy = -v.*sin(theta);k
  
   quiver(dx, dy, vx, vy, 'Color', purple);
   
   
  slice = 50;
  dx = -action{1}(:,:,slice);
  dy = -action{2}(:,:,slice);
  theta = g.vs{3}(slice);
  v = dx.*cos(theta) + dy.*sin(theta);
  vx = v.*cos(theta);
  vy = v.*sin(theta);
  
  %% finite wall occupancy grid
%   load('finite_wall_scenario.mat')
  show(map); hold on;
  title('Finite wall');
  grid on
  ax = gca
  ax.GridLineStyle = ':';
  ax.XTick = ax.YTick;
  xlabel('x [m]');
  ylabel('y [m]');
  plot(-2, 0, 'p', 'Color', 'r', 'MarkerFaceColor', 'r', 'MarkerSize', 10);
%   plot(px, py, '.', 'Color', purple);
  
  