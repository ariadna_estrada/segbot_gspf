function [ vx, vy ] = getVelocity( joy, bestGradient, mode, plots )
%Compute velocity in x and y for holonomic according to intervention mode
%Input:
%   -joy. Struct 4x1. Joystick inputs in x and y and normalized inputs
%   -bestGradient. 2x1 unit vector. Use bestGradientForGradientSamples2D.
%   -mode. Scalar from 1 to 6. Button input.
%   -plots. struct. contains gradient plots, to be updated with joystick
%
%Output:
%   -vx. Scalar, velocity in x.
%   -vy. Scalar, velocity in y. 


normalizedJoystick = [joy.normX, joy.normY];

if (bestGradient(1) == 0 && bestGradient(2) == 0) || (mode == 4)
    %If inside convex hull, send full speed in jostick direction
    vx = joy.X*1;
    vy = joy.Y*1;
else
%Choose what to do when joystick is outside convex hull
    switch(mode)
        case (1) 
        % Don't move if outside convex hull
        %Send a small velocity for testing purposes. To be changed to 0.
            vx = joy.X*0.1;
            vy = joy.Y*0.1;
        
        case (2) 
        %Send a very small velocity prop to the negative exponential of
        %the absolute angle between searchDir and joystick input
            if (joy.mag ~= 0)
                angle = abs(acos(dot(bestGradient, normalizedJoystick)));
                scale = exp(-0.5*angle);
                vx = scale * joy.X;
                vy = scale * joy.Y;
            else
                vx = 0;
                vy = 0;
            end
        case (3) 
        %Add resulting vector to joystick then normalize
            if (joy.mag~= 0)
                JoyGS = [joy.X, joy.Y] + bestGradient;
                JoyGS = JoyGS / norm(JoyGS);
                vx = JoyGS(1);
                vy = JoyGS(2);              
            else
                vx = 0;
                vy = 0;
            end
        case (5) 
            vx = bestGradient(1);
            vy = bestGradient(2); 
        case (6)
            vx = bestGradient(1);
            vy = bestGradient(2);     
    end
end

if nargin > 3 && ~isempty(plots)
     set(plots(8), 'position', [0,0, vx, vy]);
end

end

