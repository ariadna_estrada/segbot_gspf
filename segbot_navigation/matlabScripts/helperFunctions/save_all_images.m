function save_all_images( fig1, file_name1, fig2, file_name2 )
%save_all_images saves the given figures in formats .fig, .pdf, and .png
%under the name given file_name

saveas(fig1, strcat('../../final_plots/figures/', file_name1), 'fig');
saveas(fig2, strcat('../../final_plots/figures/', file_name2), 'fig');

saveas(fig1, strcat('../../final_plots/pngs/', file_name1), 'png');
saveas(fig2, strcat('../../final_plots/pngs/', file_name2), 'png');

set(fig1,'Units','Inches');
pos = get(fig1,'Position');
set(fig1,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3)-2, pos(4)]);
print(fig1, strcat('../../final_plots/pdfs/', file_name1),'-dpdf','-r0')


set(fig2,'Units','Inches');
pos = get(fig2,'Position');
set(fig2,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)]);
print(fig2, strcat('../../final_plots/pdfs/', file_name2),'-dpdf','-r0')

disp('saved images');


end

