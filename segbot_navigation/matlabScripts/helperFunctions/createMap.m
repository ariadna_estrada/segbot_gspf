image = imread(fullfile('../maps','building.pgm'));
figure
imshow(image)
imageBW = image < 100;
figure
imshow(imageBW)
map1 = robotics.BinaryOccupancyGrid(imageBW,20);
figure
show(map1)

%--------------------------------------
res = 20; %20 px per meter
[n,d] = size(image);
rem_left = 1.5;
rem_right = 1.5;
rem_top = 10;
rem_bottom = 9;

crop_im = image(rem_top*res+1:n-rem_bottom*res, rem_left*res+1:d-rem_right*res);
% crop_im = image(50+1:416-45,:)
imshow(crop_im)
imageBW = crop_im < 100;
imshow(imageBW)
map = robotics.BinaryOccupancyGrid(imageBW,res);
figure
show(map)

save('building.mat', 'map');
imwrite(crop_im, 'building.pgm');

