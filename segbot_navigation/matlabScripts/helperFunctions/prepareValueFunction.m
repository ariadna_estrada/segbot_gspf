function [g, mttr, action, obstacle_lsf] = prepareValueFunction(goal, ndim, t_max, obstacle, visualize)

    goal = goal(:);
    if nargin < 2
        ndim = 2;
    end
    if nargin < 3
        obstacle = 'none';
    end
    if nargin < 4
        t_max = 30;
    end
    if nargin < 5
        visualize = false;
    end
    
    % Compute value function (time to reach) for each grid point.
    [mttr, g, obs, obstacle_lsf] = ttr(visualize, obstacle, goal, ndim, t_max);
    % Approximate gradient of the value function at each point
    interpG = upwindFirstValueFcnGradient(mttr, g.dx);
    % Clamp the gradient to +/-1
    action = toAction(interpG);
end
