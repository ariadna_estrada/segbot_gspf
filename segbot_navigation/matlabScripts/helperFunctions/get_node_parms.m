function [ show_plots, plan_time, goalx, goaly, file_name, holonomic] = get_node_parms( ptree )
%get_node_parms( ptree ) 
%Input: 
%   -ptree: 1x1 parameter tree
%
%Output: 
%   -show_plots: boolean. Indicator to show plots or not
%   -plan_time:  double. Use if value function has not been pre-computed.
%   -goalx:      double. x-coordinate of goal
%   -goaly:      double. y-coordinate of goal

show_plots = get(ptree, '/navigator/show_plots');
plan_time = get(ptree, '/navigator/plan_time');
goalx = get(ptree, '/navigator/goalx');
goaly = get(ptree, '/navigator/goaly');
file_name = get(ptree,'/navigator/file_name');
holonomic = get(ptree, '/navigator/holonomic');
end

