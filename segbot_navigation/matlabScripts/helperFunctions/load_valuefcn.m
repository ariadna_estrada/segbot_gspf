function [ goals_vf ] = load_valuefcn( goals , world)
%Load_valuefcn loads value functions for specified goals if they exist
%Input:
%   -goals: nx2 vector contains different goal locations
%   -world: 1xm char. Name of environment (office or narrow_corridor)
%Output:
%   -goals_vf: nx1 cell. Each cell contains struct with 5 fields.


[n,~] = size(goals);
goals_vf = cell(n,2);


for i = 1:n
   file_name = strcat('saved_value_functions/', string(world), '_', string( goals(i,1) ),'_', string( goals(i,2) ), '.mat' );
   file_name = char(file_name);
   
   try
       goals_vf{i,1} = load(file_name);
   catch me
       fprintf('2D Value function for goal [%.2f, %.2f] does not exist\n', goals(i,1), goals(i,2));
   end
   
   file_name_3d = strcat('saved_value_functions/', string(world), '_3d_', string( goals(i,1) ),'_', string( goals(i,2) ), '.mat' );
   file_name_3d = char(file_name_3d);
   
   try
        vf = load(file_name_3d);
        grid_info = vf.g;
        grad = vf.action;
        mttr = vf.mttr;
        obstacle = vf.obstacle;
        vf.XS.X1 = cat(3, grid_info.xs{1}, grid_info.xs{1}(:,:,1));
        vf.XS.X2 = cat(3, grid_info.xs{2}, grid_info.xs{2}(:,:,1));
        vf.XS.X3 = cat(3, grid_info.xs{3}, ones(grid_info.shape(1:2))*2*pi);
        vf.VS.V1 = cat(3, grad{1}, grad{1}(:,:,1));
        vf.VS.V2 = cat(3, grad{2}, grad{2}(:,:,1));
        vf.VS.V3 = cat(3, grad{3}, grad{3}(:,:,1));
        vf.VS.V4 = cat(3, mttr, mttr(:,:,1));
        
        %Store 2D grid for obstacle
        g.dim = 2;
        g.bdry = { @addGhostExtrapolate; @addGhostExtrapolate };
        g.min = obstacle.origin(:);
        g.max = obstacle.origin(:);
        g.max(1) = g.max(1) + (obstacle.width-1)*obstacle.resolution;
        g.max(2) = g.max(2) + (obstacle.height-1)*obstacle.resolution;
        g.N = [obstacle.width; obstacle.height];
        vf.g2 = processGrid(g);
        
        goals_vf{i,2} = vf;
        
   catch me
       fprintf('3D Value function for goal [%.2f, %.2f] does not exist\n', goals(i,1), goals(i,2));
   end
end
end

