%Script to retreive costmap from ROS and compute the value function for a
%given goal and plan time. 

clear;clc;

%Get costmap from ROS. Skip section if loading a saved costmap.
rosshutdown;
rosinit('localhost');
costmapSub = rossubscriber('/costmap/static_costmap/costmap');
costmapMSG = costmapSub.LatestMessage;
obstacle = getCostmap(costmapMSG);
imagesc(obstacle.cost);

%Save costmap 
world = 'building';
file_name = strcat('obstacles/',world, '_obstacle.mat');
save(file_name, 'costmapMSG', 'obstacle');

%% Compute value function
%Load previously computed costmap if needed.
world = 'building';
file_name = strcat('obstacles/', world, '_obstacle.mat');
load(file_name);

%Set parameters
for i=1:10
goal = building_goals(i,:)
ndims = 3;
plan_time = 100;

[g, mttr, action, obstacle_lsf] = prepareValueFunction(goal, ndims, plan_time, obstacle, true);

%Save value function file
file_name = strcat('saved_value_functions/', world, '_3d_', num2str(goal(1)), '_', num2str(goal(2)), '.mat')
% save(file_name, 'goal', 'obstacle', 'g', 'mttr', 'action', 'obstacle_lsf');
end