bag = rosbag('goal1to6.bag');
msgList = bag.MessageList;

durationBag = select(bag, 'Topic', 'durationStamp');
durationMsgs = readMessages(durationBag);
particleBag = select(bag, 'Topic', 'particlecloud');
particleMsgs = readMessages(particleBag);
n = length(durationMsgs);
durSecs = zeros(n,1);
for i = 1:n
    durSecs(i) = seconds(durationMsgs{i,1}.Data);
end
durSecs = durSecs(4:end);
% histfit(durSecs)

timestamps = msgList(:,1:2);
% 
% timestamps = sortrows(timestamps,'Topic','descend');
particle_times = timestamps(1:148,1);
cmd_times = timestamps(318:700,1);
joy_times = timestamps(701:end,1);
class_times = timestamps(201:317,1);
% cmd2_times = timestamps(6490:9313,1);
% 
particle_times = table2array(particle_times);
cmd_times = table2array(cmd_times);
joy_times = table2array(joy_times);
class_times = table2array(class_times);
% 
vel_time_diff = diff(cmd_times);
particles_time_diff = diff(particle_times);
joy_time_diff = diff(joy_times);
class_time_diff = diff(joy_times);
% 
subplot(2,2,1), histfit(vel_time_diff);
title('/cmd vel');
xlabel('time [s]');


subplot(2,2,2), histfit(particles_time_diff);
title('/particlecloud');
xlabel('time [s]');


subplot(2,2,3), histfit(joy_time_diff);
title('/joy');
xlabel('time [s]');

subplot(2,2,4), histfit(class_time_diff);
title('/classification');
xlabel('time [s]');

figure 
histfit(abs(durSecs));
title('particlecloud and joystick time difference');
xlabel('duration [s]');
pdduration = fitdist(abs(durSecs), 'Normal');

figure 
histfit(durSecs);
title('particlecloud and joystick time difference');
xlabel('duration [s]');
pdduration2 = fitdist(durSecs, 'Normal');


% wb_values = fitdist(vel_time_diff(11:140), 'weibull');
% xs = linspace(0.05,0.3);
% Y = normpdf(xs, pd.mu, pd.sigma);
% histogram(particles_time_diff);
% hold on;
% plot(xs, Y, 'r-', 'linewidth', 2);
% xlabel('time [s]');
% title('/particlecloud time between messages');