%Script to retreive costmap from ROS and store it as an occupancy grid

clear;clc;

%Get costmap from ROS. Skip section if loading a saved costmap.
rosshutdown;
rosinit('localhost');
costmapSub = rossubscriber('/move_base/global_costmap/costmap');
costmapMSG = costmapSub.LatestMessage;
obstacle = getCostmap(costmapMSG);
imagesc(obstacle.cost);

norm_cost = double(obstacle.cost) ./100;
map_cost = imrotate(norm_cost, 90);
map = robotics.OccupancyGrid(map_cost, 20); %resolution is 20 cells / meter

%update gridLocationInWorld manually

global_costmap = map;
save('building_global_costmap.mat', 'global_costmap');